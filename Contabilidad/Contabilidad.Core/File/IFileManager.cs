﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.File
{
    public interface IFileManager
    {
        Task<OperationResult<Database.Models.FileInBytes>> GsetByFormFileAsync(IFormFile file);

        Task<byte[]> GetByteArrayFromFormFile(IFormFile file);
    }
}
