﻿using Contabilidad.Database.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Contabilidad.Core.File
{
    public class FileManager : IFileManager
    {
        public async Task<OperationResult<FileInBytes>> GsetByFormFileAsync(IFormFile file)
        {
            if (file != null)
            {
                return new OperationResult<FileInBytes>(new FileInBytes
                {
                    FileinBytes = await this.GetByteArrayFromFormFile(file),
                    Extension = Path.GetExtension(file.FileName),
                    LengthInBytes = file.Length,
                    OriginalName = file.FileName,
                    Name = file.FileName,
                });

            }

            return new OperationResult<FileInBytes>(new[] { "NULL" }, null);
        }

        public async Task<byte[]> GetByteArrayFromFormFile(IFormFile file)
        {
            if (file != null)
            {
                using (var target = new MemoryStream())
                {
                    await file.CopyToAsync(target);
                    return target.ToArray();
                }

            }

            return null;
        }
    }
}
