﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Client
{
    public class ClientManager : IClientManager
    {
        private readonly IRepository<Database.Models.Accounting.Client> clientRepository;

        public ClientManager(IRepository<Database.Models.Accounting.Client> clientRepository)
        {
            this.clientRepository = clientRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.Client client)
        {
            if (client == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(client)] = new string[] { "El objeto CLIENTE no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(client);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.Client client)
        {
            this.clientRepository.Create(client);
            await this.clientRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idClient)
        {
            if (idClient == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idClient)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var client = await this.clientRepository.All().FirstOrDefaultAsync(b => b.Id == idClient.Value);

            if (client == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(client)] = new string[] { $"No se encontró una CLIENTE con el id {idClient.Value}." },
                    });
            }

            this.clientRepository.Delete(client);
            await this.clientRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.Client client)
        {
            this.clientRepository.Update(client);
            await this.clientRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.Client>> GetAllAsync()
        {
            var list = await this.clientRepository.All()
                .Include(p => p.People)
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.Client> GetByIdAsync(int? idClient)
        {
            if (idClient == null)
            {
                return null;
            }

            return await this.clientRepository.All()
                .Include(p => p.People)
                .FirstOrDefaultAsync(p => p.Id == idClient.Value);
        }
    }
}
