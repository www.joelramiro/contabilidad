﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Client
{
    public interface IClientManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.Client client);

        Task<OperationResult> EditAsync(Database.Models.Accounting.Client client);

        Task<OperationResult> DeleteAsync(int? idClient);

        Task<IEnumerable<Database.Models.Accounting.Client>> GetAllAsync();

        Task<Database.Models.Accounting.Client> GetByIdAsync(int? idClient);
    }
}
