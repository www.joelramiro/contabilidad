﻿// <copyright file="OperationResult{TResult}.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Core
{
    using System.Collections.Generic;

    public class OperationResult<TResult> : OperationResult
    {
        public OperationResult()
            : base()
        {
        }

        public OperationResult(TResult result)
            : this()
        {
            this.Result = result;
        }

        public OperationResult(bool succeeded, TResult result)
            : this(result)
        {
            this.Succeeded = succeeded;
        }

        public OperationResult(IEnumerable<string> errors, TResult result)
            : base(errors)
        {
            this.Result = result;
        }

        public OperationResult(IDictionary<string, IEnumerable<string>> validationMessages, TResult result)
            : base(validationMessages)
        {
            this.Result = result;
        }

        public TResult Result { get; protected set; }
    }
}