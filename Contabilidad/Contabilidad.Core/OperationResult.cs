﻿// <copyright file="OperationResult.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>
namespace Contabilidad.Core
{
    using System.Collections.Generic;
    using System.Linq;

    public class OperationResult
    {
        public OperationResult()
        {
            this.ValidationMessages = new Dictionary<string, IEnumerable<string>>();
            this.Succeeded = false;
        }

        public OperationResult(bool succeeded)
            : this()
        {
            this.Succeeded = succeeded;
        }

        public OperationResult(IEnumerable<string> errors)
            : this()
        {
            this.ValidationMessages = new Dictionary<string, IEnumerable<string>>
            {
                [string.Empty] = errors,
            };
        }

        public OperationResult(IDictionary<string, IEnumerable<string>> validationMessages)
            : this()
        {
            this.ValidationMessages = new Dictionary<string, IEnumerable<string>>(validationMessages);
        }

        public IEnumerable<string> Errors
        {
            get
            {
                return this.ValidationMessages.SelectMany(s => s.Value);
            }
        }

        public IReadOnlyDictionary<string, IEnumerable<string>> ValidationMessages { get; protected set; }

        public bool Succeeded { get; protected set; }
    }
}