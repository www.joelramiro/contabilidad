﻿using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.AssignPurchaseToWarehouse
{
    public class AssignPurchaseToWarehouseManager : IAssignPurchaseToWarehouseManager
    {
        private readonly IRepository<Database.Models.Accounting.AssignPurchaseToWarehouse> assignPurchaseToWarehouseRepository;
        private readonly IRepository<Database.Models.Accounting.PurchaseInvoice> purchaseInvoiceRepository;
        private readonly IRepository<Database.Models.Accounting.Warehouse> warehouseRepository;

        public AssignPurchaseToWarehouseManager(
            IRepository<Database.Models.Accounting.AssignPurchaseToWarehouse> assignPurchaseToWarehouseRepository,
            IRepository<Database.Models.Accounting.PurchaseInvoice> purchaseInvoiceRepository,
            IRepository<Database.Models.Accounting.Warehouse> warehouseRepository)
        {
            this.assignPurchaseToWarehouseRepository = assignPurchaseToWarehouseRepository;
            this.purchaseInvoiceRepository = purchaseInvoiceRepository;
            this.warehouseRepository = warehouseRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.AssignPurchaseToWarehouse assignPurchaseToWarehouse)
        {
            if (assignPurchaseToWarehouse == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(assignPurchaseToWarehouse)] = new string[] { "El objeto registro no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(assignPurchaseToWarehouse);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.AssignPurchaseToWarehouse assignPurchaseToWarehouse)
        {
            var invoice = await this.purchaseInvoiceRepository
                .All()
                .Include(i => i.Details)
                .ThenInclude(d => d.Product)
                .FirstOrDefaultAsync(i => i.Id == assignPurchaseToWarehouse.IdPurchaseInvoice);

            if (invoice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(invoice)] = new string[] { "No se encontro la factura." },
                    });
            }

            if (invoice.IsAssignedToWarehouse)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(invoice)] = new string[] { "La factura ya esta asignada a un (unos) álmacen (es)." },
                    });
            }

            var productsWithQuantityInvoice = invoice.Details
                .GroupBy(d => d.Product)
                .Select(d => new
                {
                    Product = d.Key,
                    Quantity = d.Sum(de => de.Quantity),
                });

            var preAssigned = await this.assignPurchaseToWarehouseRepository.All()
                .Include(p => p.Detail)
                .ThenInclude(d => d.Product)
                .Where(p => p.IdPurchaseInvoice == assignPurchaseToWarehouse.IdPurchaseInvoice)
                .SelectMany(p => p.Detail, (p, d) => new { d.Product, d.Quantity })
                .ToListAsync();
            
            var preAssignedGroup = preAssigned
                .GroupBy(d => d.Product)
                .Select(d => new { Product = d.Key, Quantity = d.Sum(dd => dd.Quantity) });

            var finalResult = productsWithQuantityInvoice.GroupJoin(
                preAssignedGroup, p => p.Product, d => d.Product, (i, d) => new { i, d })
                .SelectMany(d => d.d.DefaultIfEmpty(), (i, d) => new { Product = i.i.Product, Quantity = d != null ? i.i.Quantity - d.Quantity : i.i.Quantity });

            var productsWithQuantityAssigned = assignPurchaseToWarehouse.Detail
                .GroupBy(d => d.IdProduct)
                .Select(d => new
                {
                    idProduct = d.Key,
                    Quantity = d.Sum(de => de.Quantity)
                });

            var IAQuantity = finalResult
                .GroupJoin(productsWithQuantityAssigned, d => d.Product.Id, d => d.idProduct, (fResult, pwqA) => new { fResult, pwqA })
                .SelectMany(d => d.pwqA.DefaultIfEmpty(), (fresult, pwqA) => new
                {
                    fresult.fResult.Product,
                    AQuantity = fresult.fResult.Quantity,
                    IQuantity = pwqA == null ? 0 : pwqA.Quantity,
                })
                .GroupBy(d => new { d.Product, d.AQuantity })
                .Select(d => new
                {
                    Product = d.Key.Product,
                    AQuantity = d.Key.AQuantity,
                    IQuantity = d.Sum(dd => dd.IQuantity),
                }).ToList();

            var test = finalResult
                .GroupJoin(productsWithQuantityAssigned, d => d.Product.Id, d => d.idProduct, (fResult, pwqA) => new { fResult, pwqA })
                .SelectMany(d => d.pwqA.DefaultIfEmpty(), (fresult, pwqA) => new
                {
                    fresult.fResult.Product,
                    AQuantity = fresult.fResult.Quantity,
                    IQuantity = pwqA == null ? 0 : pwqA.Quantity,
                }).ToList();

            var messages = new StringBuilder();

            var allTrue = true;
            foreach (var item in IAQuantity)
            {
                if (item.AQuantity < item.IQuantity)
                {
                    messages.Append($"Las cantidades del producto {item.Product.Name} sobrepasa a la cantidad disponible de la factura.");
                }
                else
                {
                    if (item.AQuantity != item.IQuantity)
                    {
                        allTrue = false;
                    }
                }
            }

            if (messages.Length != 0)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(invoice)] = new string[] { messages.ToString() },
                    });
            }

            if (allTrue)
            {
                invoice.IsAssignedToWarehouse = true;
                this.purchaseInvoiceRepository.Update(invoice);
                //await this.purchaseInvoiceRepository.SaveChangesAsync();
            }

            var temp = assignPurchaseToWarehouse.Detail.GroupBy(w => w.IdWarehouse);
            var warehouses = this.warehouseRepository.All()
                .Include(w => w.Details);

            var warehouseJoin = temp.Join
                (warehouses, w => w.Key, w => w.Id, (a,w) => new { warehouse = w, assignation = a});

            foreach (var item in warehouseJoin)
            {
                var productGroup = item.assignation.GroupBy(d => d.IdProduct);

                foreach (var product in productGroup)
                {
                    var productLine = item.warehouse.Details.FirstOrDefault(w => w.IdProduct == product.Key);
                    if (productLine == null)
                    {
                        item.warehouse.Details.Add(new WarehouseDetail 
                        {
                            IdProduct = product.Key,
                            Quantity = product.Sum(p => p.Quantity),
                        });
                    }
                    else
                    {
                        productLine.Quantity += product.Sum(p => p.Quantity);
                    }
                }
                this.warehouseRepository.Update(item.warehouse);
            }
            this.assignPurchaseToWarehouseRepository.Create(assignPurchaseToWarehouse);
            await this.assignPurchaseToWarehouseRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.AssignPurchaseToWarehouse>> GetAllAsync()
        {
            var list = await this.assignPurchaseToWarehouseRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.AssignPurchaseToWarehouse> GetByIdAsync(int? idAssignPurchaseToWarehouse)
        {
            if (idAssignPurchaseToWarehouse == null)
            {
                return null;
            }

            return await this.assignPurchaseToWarehouseRepository.All()
                .FirstOrDefaultAsync(p => p.Id == idAssignPurchaseToWarehouse.Value);
        }

        public async Task<IEnumerable<Database.Models.Accounting.AssignPurchaseToWarehouse>> GetByDateRangeAsync(DateTime startDate, DateTime endDate)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;

            var list = await this.assignPurchaseToWarehouseRepository.All()
                .Where(a => a.CreatedDate >= startDate && a.CreatedDate <= endDate)
                .ToListAsync();

            return list;
        }

        public async Task<OperationResult> AssignPurchaseInvoiceToWarehouse(Database.Models.Accounting.PurchaseInvoice purchase, int idWarehouse)
        {
            var warehouse = await this.warehouseRepository
                .All()
                .Include(w => w.Details)
                .FirstOrDefaultAsync(w => w.Id == idWarehouse);

            if (warehouse == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idWarehouse)] = new string[] { $"No existe un almacén con el id {idWarehouse}." },
                    });
            }
            var assigned = new Database.Models.Accounting.AssignPurchaseToWarehouse
            {
                CreatedDate = DateTime.Now,
                IdPurchaseInvoice = purchase.Id,
                //IdUsser = purchase.IdUsser.Value, CORE HERE   CORE HERE   CORE HERE
                Detail = new List<AssignPurchaseToWarehouseDetail>(),
            };

            foreach (var detail in purchase.Details)
            {
                assigned.Detail.Add(new AssignPurchaseToWarehouseDetail
                {
                    IdWarehouse = idWarehouse,
                    IdProduct = detail.IdProduct,
                    PurchaseUnitPrice = detail.UnitPrice,
                    Quantity = detail.Quantity,
                });

                var wDetail = warehouse.Details.FirstOrDefault(w => w.IdProduct == detail.IdProduct);

                if (wDetail == null)
                {
                    var d = new WarehouseDetail
                    {
                        IdProduct = detail.IdProduct,
                        Quantity = detail.Quantity,
                    };

                    warehouse.Details.Add(d);
                }
                else
                {
                    wDetail.Quantity += detail.Quantity;
                }
            }

            this.assignPurchaseToWarehouseRepository.Create(assigned);
            purchase.IsAssignedToWarehouse = true;
            this.purchaseInvoiceRepository.Update(purchase);
            this.warehouseRepository.Update(warehouse);
            /*await this.assignPurchaseToWarehouseRepository.SaveChangesAsync();
            await this.purchaseInvoiceRepository.SaveChangesAsync();*/
            await this.warehouseRepository.SaveChangesAsync();

            return new OperationResult(true);
        }
    }
}