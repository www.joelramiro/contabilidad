﻿using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.AssignPurchaseToWarehouse
{
    public interface IAssignPurchaseToWarehouseManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.AssignPurchaseToWarehouse assignPurchaseToWarehouse);

        Task<IEnumerable<Database.Models.Accounting.AssignPurchaseToWarehouse>> GetAllAsync();

        Task<Database.Models.Accounting.AssignPurchaseToWarehouse> GetByIdAsync(int? idAssignPurchaseToWarehouse);

        Task<IEnumerable<Database.Models.Accounting.AssignPurchaseToWarehouse>> GetByDateRangeAsync(DateTime startDate, DateTime endDate);

        Task<OperationResult> AssignPurchaseInvoiceToWarehouse(Database.Models.Accounting.PurchaseInvoice purchase, int idWarehouse);
    }
}
