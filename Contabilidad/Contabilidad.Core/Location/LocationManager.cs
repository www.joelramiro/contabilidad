﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Contabilidad.Core.Location
{
    public class LocationManager : ILocationManager
    {
        public LocationManager()
        {
        }
        public async Task<Database.Models.Accounting.Location> GetLocationAsync()
        {
            //GeoCoordinateWatcher watcher = new GeoCoordinateWatcher();
            //CODE HERE CODE HERE   CODE HERE   CODE HERE   CODE HERE   CODE HERE  CODE HERE   

            return new Database.Models.Accounting.Location 
            {
                Accuracy = 0,
                Altitude = 0,
                AltitudeAccuracy = 0,
                Heading = 0,
                Latitude = 0,
                Longitude = 0,
            };
        }
    }
}
