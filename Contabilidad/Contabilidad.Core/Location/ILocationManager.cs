﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Location
{
    public interface ILocationManager
    {
        Task<Database.Models.Accounting.Location> GetLocationAsync();
    }
}
