﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Brand
{
    public interface IBrandManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.Brand brand );

        Task<OperationResult> EditAsync(Database.Models.Accounting.Brand brand);

        Task<OperationResult> DeleteAsync(int? idBrand);

        Task<IEnumerable<Database.Models.Accounting.Brand >> GetAllAsync();

        Task<Database.Models.Accounting.Brand > GetByIdAsync(int? idBrand);
    }
}
