﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Brand
{
    public class BrandManager : IBrandManager
    {
        private readonly IRepository<Database.Models.Accounting.Brand> brandRepository;

        public BrandManager(IRepository<Database.Models.Accounting.Brand> brandRepository)
        {
            this.brandRepository = brandRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.Brand brand)
        {
            if (brand == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(brand)] = new string[] { "El objeto MARCA no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(brand);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.Brand brand)
        {
            var exist = await this.brandRepository.All().AnyAsync(p => p.Name == brand.Name);
            if (exist)
            {
                return new OperationResult(
                       new Dictionary<string, IEnumerable<string>>()
                       {
                           [nameof(brand.Name)] = new string[] { $"Ya existe una MARCA con el nombre {brand.Name}." },
                       });
            }

            this.brandRepository.Create(brand);
            await this.brandRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idBrand)
        {
            if (idBrand == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idBrand)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var brand = await this.brandRepository.All().Include(b => b.Products).FirstOrDefaultAsync(b => b.Id == idBrand.Value);

            if (brand == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(brand)] = new string[] { $"No se encontró una MARCA con el id {idBrand.Value}." },
                    });
            }

            if (brand.Products.Any())
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(brand)] = new string[] { $"No se puede eliminar la MARCA debido a que esta siendo utilizada por uno(varios) PRODUCTOS" },
                    });
            }

            this.brandRepository.Delete(brand);
            await this.brandRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.Brand brand)
        {
            var exist = await this.brandRepository.All().AnyAsync(p => p.Name == brand.Name && p.Id != brand.Id);

            if (exist)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(brand.Name)] = new string[] { $"Ya existe una MARCA con el nombre {brand.Name}." },
                    });
            }

            this.brandRepository.Update(brand);
            await this.brandRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.Brand>> GetAllAsync()
        {
            var list = await this.brandRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.Brand> GetByIdAsync(int? idBrand)
        {
            if (idBrand == null)
            {
                return null;
            }

            return await this.brandRepository.All()
                .FirstOrDefaultAsync(p => p.Id == idBrand.Value);
        }
    }
}
