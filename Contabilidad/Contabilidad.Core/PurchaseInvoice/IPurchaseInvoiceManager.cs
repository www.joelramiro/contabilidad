﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.PurchaseInvoice
{
    public interface IPurchaseInvoiceManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.PurchaseInvoice purchaseInvoice);

        Task<OperationResult> EditAsync(Database.Models.Accounting.PurchaseInvoice purchaseInvoice);

        Task<OperationResult> DeleteAsync(int? idPurchaseInvoice);

        Task<IEnumerable<Database.Models.Accounting.PurchaseInvoice>> GetAllAsync();

        Task<Database.Models.Accounting.PurchaseInvoice> GetByIdAsync(int? idPurchaseInvoice);
    }
}
