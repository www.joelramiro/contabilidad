﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.PurchaseInvoice
{
    public class PurchaseInvoiceManager : IPurchaseInvoiceManager
    {
        private readonly IRepository<Database.Models.Accounting.PurchaseInvoice> purchaseInvoiceRepository;
        private readonly IRepository<Database.Models.Accounting.AssignPurchaseToWarehouse> assignPurchaseToWarehouseRepository;

        public PurchaseInvoiceManager(
            IRepository<Database.Models.Accounting.PurchaseInvoice> purchaseInvoiceRepository,
            IRepository<Database.Models.Accounting.AssignPurchaseToWarehouse> assignPurchaseToWarehouseRepository)
        {
            this.purchaseInvoiceRepository = purchaseInvoiceRepository;
            this.assignPurchaseToWarehouseRepository = assignPurchaseToWarehouseRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.PurchaseInvoice purchaseInvoice)
        {
            if (purchaseInvoice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(purchaseInvoice)] = new string[] { "El objeto FACTURA DE COMPRA no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(purchaseInvoice);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.PurchaseInvoice purchaseInvoice)
        {
            purchaseInvoice.CreatedDate = DateTime.Now;
            this.purchaseInvoiceRepository.Create(purchaseInvoice);
            await this.purchaseInvoiceRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idPurchaseInvoice)
        {
            if (idPurchaseInvoice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idPurchaseInvoice)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var purchaseInvoice = await this.purchaseInvoiceRepository.All().FirstOrDefaultAsync(b => b.Id == idPurchaseInvoice.Value);

            if (purchaseInvoice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(purchaseInvoice)] = new string[] { $"No se encontró una FACTURA DE COMPRA con el id {idPurchaseInvoice.Value}." },
                    });
            }

            if (purchaseInvoice.IsAssignedToWarehouse)
            {
                return new OperationResult(new[] { $"No se puede eliminar debido a que la factura ya ha sido asignada a un almacén" });
            }

            var anyAssigned = await this.assignPurchaseToWarehouseRepository.All().AnyAsync(w => w.IdPurchaseInvoice == purchaseInvoice.Id);

            if (anyAssigned)
            {
                return new OperationResult(new[] { $"No se puede eliminar debido a que la factura ya ha sido asignada parcialmente a un almacén" });
            }

            this.purchaseInvoiceRepository.Delete(purchaseInvoice);
            await this.purchaseInvoiceRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.PurchaseInvoice purchaseInvoice)
        {
            var purchaseAsigned = await this.purchaseInvoiceRepository.FirstOrDefaultAsync(p => p.Id == purchaseInvoice.Id);

            if (purchaseAsigned == null)
            {
                return new OperationResult(new[] { $"No se encontro una factura con el id [{purchaseInvoice.Id}]" });
            }

            if (purchaseInvoice.IsAssignedToWarehouse)
            {
                return new OperationResult(new[] { $"No se puede editar debido a que la factura ya ha sido asignada a un almacén" });
            }

            var anyAssigned = await this.assignPurchaseToWarehouseRepository.All().AnyAsync(w => w.IdPurchaseInvoice == purchaseInvoice.Id);

            if (anyAssigned)
            {
                return new OperationResult(new[] { $"No se puede editar debido a que la factura ya ha sido asignada parcialmente a un almacén" });
            }

            this.purchaseInvoiceRepository.Update(purchaseInvoice);
            await this.purchaseInvoiceRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.PurchaseInvoice>> GetAllAsync()
        {
            var list = await this.purchaseInvoiceRepository.All()
                .Include(p => p.Provider)
                .ThenInclude(pr => pr.People)
                .Include(p => p.Currency)
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.PurchaseInvoice> GetByIdAsync(int? idPurchaseInvoice)
        {
            if (idPurchaseInvoice == null)
            {
                return null;
            }

            return await this.purchaseInvoiceRepository.All()
                .Include(d => d.Details)
                .ThenInclude(d => d.Product)
                .Include(p => p.Provider)
                .ThenInclude(pr => pr.People)
                .Include(p => p.Currency)
                .FirstOrDefaultAsync(p => p.Id == idPurchaseInvoice.Value);
        }
    }
}
