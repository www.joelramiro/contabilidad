﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.ProductPrice
{
    public class ProductPriceManager : IProductPriceManager
    {
        private readonly IRepository<Database.Models.Accounting.ProductPrice> productPriceRepository;

        public ProductPriceManager(IRepository<Database.Models.Accounting.ProductPrice> productPriceRepository)
        {
            this.productPriceRepository = productPriceRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.ProductPrice productPrice)
        {
            if (productPrice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productPrice)] = new string[] { "El objeto PRRECIO DE PRODUCTO no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(productPrice);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.ProductPrice productPrice)
        {
            this.productPriceRepository.Create(productPrice);
            await this.productPriceRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idProductPrice)
        {
            if (idProductPrice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idProductPrice)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var productPrice = await this.productPriceRepository.All().FirstOrDefaultAsync(b => b.Id == idProductPrice.Value);

            if (productPrice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productPrice)] = new string[] { $"No se encontró un PRRECIO DE PRODUCTO con el id {idProductPrice.Value}." },
                    });
            }

            var productsPrice = await this.productPriceRepository.Filter(d => d.IdProduct == productPrice.IdProduct && d.IdCurrency == productPrice.IdCurrency).ToListAsync();

            foreach (var item in productsPrice)
            {
                this.productPriceRepository.Delete(item);
            }

            await this.productPriceRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.ProductPrice productPrice)
        {
            return await this.CreateAsync(productPrice);
        }

        public async Task<IEnumerable<Database.Models.Accounting.ProductPrice>> GetAllAsync()
        {
            var list = await this.productPriceRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.ProductPrice> GetByIdAsync(int? idProductPrice)
        {
            if (idProductPrice == null)
            {
                return null;
            }

            return await this.productPriceRepository.All()
                .Include(d => d.Product)
                .Include(d => d.Currency)
                .FirstOrDefaultAsync(p => p.Id == idProductPrice.Value);
        }

        public async Task<IEnumerable<Database.Models.Accounting.ProductPrice>> GetAllWithProductAndCurrencyMoreRecentAsync()
        {
            var list = await this.productPriceRepository.All()
                .Include(d => d.Product)
                .Include(d => d.Currency).ToListAsync();


            var groupByProduct = list.GroupBy(l => new { l.IdProduct, l.IdCurrency });

            var result = new List<Database.Models.Accounting.ProductPrice>();

            foreach (var item in groupByProduct)
            {
                var moreNew = item.OrderByDescending(d => d.CreatedDate).First();
                result.Add(moreNew);
            }

            return result;
        }

        public async Task<Database.Models.Accounting.ProductPrice> GetMoreRecentByProductIdAsync(int? idProduct)
        {
            var result = await this.productPriceRepository.Filter(p => p.IdProduct == idProduct).ToListAsync();

            return result.OrderByDescending(d => d.CreatedDate).FirstOrDefault();
        }
    }
}
