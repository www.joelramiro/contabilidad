﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.ProductPrice
{
    public interface IProductPriceManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.ProductPrice productPrice);

        Task<OperationResult> EditAsync(Database.Models.Accounting.ProductPrice productPrice);

        Task<OperationResult> DeleteAsync(int? idProductPrice);

        Task<IEnumerable<Database.Models.Accounting.ProductPrice>> GetAllAsync();

        Task<IEnumerable<Database.Models.Accounting.ProductPrice>> GetAllWithProductAndCurrencyMoreRecentAsync();

        Task<Database.Models.Accounting.ProductPrice> GetByIdAsync(int? idProductPrice);

        Task<Database.Models.Accounting.ProductPrice> GetMoreRecentByProductIdAsync(int? idProduct);
    }
}
