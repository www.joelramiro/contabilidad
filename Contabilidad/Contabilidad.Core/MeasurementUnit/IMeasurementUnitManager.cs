﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.MeasurementUnit
{
    public interface IMeasurementUnitManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.MeasurementUnit measurementUnit);

        Task<OperationResult> EditAsync(Database.Models.Accounting.MeasurementUnit measurementUnit);

        Task<OperationResult> DeleteAsync(int? idMeasurementUnit);

        Task<IEnumerable<Database.Models.Accounting.MeasurementUnit>> GetAllAsync();

        Task<Database.Models.Accounting.MeasurementUnit> GetByIdAsync(int? idMeasurementUnit);
    }
}
