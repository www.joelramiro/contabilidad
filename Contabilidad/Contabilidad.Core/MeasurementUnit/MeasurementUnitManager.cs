﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.MeasurementUnit
{
    public class MeasurementUnitManager : IMeasurementUnitManager
    {
        private readonly IRepository<Database.Models.Accounting.MeasurementUnit> measurementUnitRepository;

        public MeasurementUnitManager(IRepository<Database.Models.Accounting.MeasurementUnit> measurementUnitRepository)
        {
            this.measurementUnitRepository = measurementUnitRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.MeasurementUnit measurementUnit)
        {
            if (measurementUnit == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(measurementUnit)] = new string[] { "El objeto UNIDAD DE MEDIDA no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(measurementUnit);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.MeasurementUnit measurementUnit)
        {
            var exist = await this.measurementUnitRepository.All().AnyAsync(p => p.MeasurementName == measurementUnit.MeasurementName);
            if (exist)
            {
                return new OperationResult(
                       new Dictionary<string, IEnumerable<string>>()
                       {
                           [nameof(measurementUnit.MeasurementName)] = new string[] { $"Ya existe una UNIDAD DE MEDIDA con el nombre {measurementUnit.MeasurementName}." },
                       });
            }

            this.measurementUnitRepository.Create(measurementUnit);
            await this.measurementUnitRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idMeasurementUnit)
        {
            if (idMeasurementUnit == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idMeasurementUnit)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var measurementUnit = await this.measurementUnitRepository.All().Include(b => b.Products).FirstOrDefaultAsync(b => b.Id == idMeasurementUnit.Value);

            if (measurementUnit == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(measurementUnit)] = new string[] { $"No se encontró una UNIDAD DE MEDIDA con el id {idMeasurementUnit.Value}." },
                    });
            }

            if (measurementUnit.Products.Any())
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(measurementUnit)] = new string[] { $"No se puede eliminar la UNIDAD DE MEDIDA debido a que esta siendo utilizada por uno(varios) PRODUCTOS" },
                    });
            }

            this.measurementUnitRepository.Delete(measurementUnit);
            await this.measurementUnitRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.MeasurementUnit measurementUnit)
        {
            var exist = await this.measurementUnitRepository.All().AnyAsync(p => p.MeasurementName == measurementUnit.MeasurementName && p.Id != measurementUnit.Id);

            if (exist)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(measurementUnit.MeasurementName)] = new string[] { $"Ya existe una UNIDAD DE MEDIDA con el nombre {measurementUnit.MeasurementName}." },
                    });
            }

            this.measurementUnitRepository.Update(measurementUnit);
            await this.measurementUnitRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.MeasurementUnit>> GetAllAsync()
        {
            var list = await this.measurementUnitRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.MeasurementUnit> GetByIdAsync(int? idMeasurementUnit)
        {
            if (idMeasurementUnit == null)
            {
                return null;
            }

            return await this.measurementUnitRepository.All()
                .FirstOrDefaultAsync(p => p.Id == idMeasurementUnit.Value);
        }
    }
}
