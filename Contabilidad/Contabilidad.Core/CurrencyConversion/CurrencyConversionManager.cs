﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.CurrencyConversion
{
    public class CurrencyConversionManager : ICurrencyConversionManager
    {
        private readonly IRepository<Database.Models.Accounting.CurrencyConversion> currencyConversionRepository;

        public CurrencyConversionManager(IRepository<Database.Models.Accounting.CurrencyConversion> currencyConversionRepository)
        {
            this.currencyConversionRepository = currencyConversionRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.CurrencyConversion currencyConversion, bool reverseConversion)
        {
            if (currencyConversion == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(currencyConversion)] = new string[] { "El objeto CONVERSION DE MONEDA no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(currencyConversion, reverseConversion);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.CurrencyConversion currencyConversion, bool reverseConversion)
        {
            if (currencyConversion.IdCurrencyOrigin == currencyConversion.IdCurrencyDestination)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(currencyConversion)] = new string[] { "No se puede realizar conversión entre la misma moneda." },
                    });
            }

            this.currencyConversionRepository.Create(currencyConversion);

            if (reverseConversion)
            {
                this.currencyConversionRepository.Create(new Database.Models.Accounting.CurrencyConversion
                {
                    CreatedDate = currencyConversion.CreatedDate,
                    IdCurrencyOrigin = currencyConversion.IdCurrencyDestination,
                    IdCurrencyDestination = currencyConversion.IdCurrencyOrigin,
                    ValueConversion = 1/currencyConversion.ValueConversion,
                });
            }

            await this.currencyConversionRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idCurrencyConversion)
        {
            if (idCurrencyConversion == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idCurrencyConversion)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var currencyConversion = await this.currencyConversionRepository.All().FirstOrDefaultAsync(b => b.Id == idCurrencyConversion.Value);

            if (currencyConversion == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(currencyConversion)] = new string[] { $"No se encontró un CONVERSION DE MONEDA con el id {idCurrencyConversion.Value}." },
                    });
            }

            var productsPrice = await this.currencyConversionRepository.Filter(d => d.IdCurrencyOrigin == currencyConversion.IdCurrencyOrigin && d.IdCurrencyDestination == currencyConversion.IdCurrencyDestination).ToListAsync();

            foreach (var item in productsPrice)
            {
                this.currencyConversionRepository.Delete(item);
            }

            await this.currencyConversionRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.CurrencyConversion currencyConversion, bool reverseConversion)
        {
            return await this.CreateAsync(currencyConversion, reverseConversion);
        }

        public async Task<IEnumerable<Database.Models.Accounting.CurrencyConversion>> GetAllAsync()
        {
            var list = await this.currencyConversionRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.CurrencyConversion> GetByIdAsync(int? idCurrencyConversion)
        {
            if (idCurrencyConversion == null)
            {
                return null;
            }

            return await this.currencyConversionRepository.All()
                .Include(d => d.CurrencyOrigin)
                .Include(d => d.CurrencyDestination)
                .FirstOrDefaultAsync(p => p.Id == idCurrencyConversion.Value);
        }

        public async Task<IEnumerable<Database.Models.Accounting.CurrencyConversion>> GetAllWithCurrenciesMoreRecentAsync()
        {
            var list = await this.currencyConversionRepository.All()
                .Include(d => d.CurrencyOrigin)
                .Include(d => d.CurrencyDestination).ToListAsync();


            var groupByProduct = list.GroupBy(l => new { l.CurrencyOrigin, l.CurrencyDestination });

            var result = new List<Database.Models.Accounting.CurrencyConversion>();

            foreach (var item in groupByProduct)
            {
                var moreNew = item.OrderByDescending(d => d.CreatedDate).First();
                result.Add(moreNew);
            }

            return result;
        }

        public async Task<Database.Models.Accounting.CurrencyConversion> GetMoreRecentByIdAsync(int? idCurrencyConversion)
        {
            var result = await this.currencyConversionRepository
                .All()
                .Include(d => d.CurrencyOrigin)
                .Include(d => d.CurrencyDestination)
                .Where(p => p.Id == idCurrencyConversion).ToListAsync();

            return result.OrderByDescending(d => d.CreatedDate).FirstOrDefault();
        }
    }
}
