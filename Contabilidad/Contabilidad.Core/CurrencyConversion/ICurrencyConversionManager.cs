﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.CurrencyConversion
{
    public interface ICurrencyConversionManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.CurrencyConversion currencyConversion, bool reverseConversion);
        Task<OperationResult> EditAsync(Database.Models.Accounting.CurrencyConversion currencyConversion, bool reverseConversion);

        Task<OperationResult> DeleteAsync(int? idCurrencyConversion);

        Task<IEnumerable<Database.Models.Accounting.CurrencyConversion>> GetAllAsync();

        Task<IEnumerable<Database.Models.Accounting.CurrencyConversion>> GetAllWithCurrenciesMoreRecentAsync();

        Task<Database.Models.Accounting.CurrencyConversion> GetByIdAsync(int? idCurrencyConversion);

        Task<Database.Models.Accounting.CurrencyConversion> GetMoreRecentByIdAsync(int? idCurrencyConversion);
    }
}
