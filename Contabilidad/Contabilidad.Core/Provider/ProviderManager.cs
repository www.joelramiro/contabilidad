﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Provider
{
    public class ProviderManager : IProviderManager
    {
        private readonly IRepository<Database.Models.Accounting.Provider> providerRepository;

        public ProviderManager(IRepository<Database.Models.Accounting.Provider> providerRepository)
        {
            this.providerRepository = providerRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.Provider provider)
        {
            if (provider == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(provider)] = new string[] { "El objeto PROVEEDOR no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(provider);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.Provider provider)
        {
            this.providerRepository.Create(provider);
            await this.providerRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idProvider)
        {
            if (idProvider == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idProvider)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var provider = await this.providerRepository.All().FirstOrDefaultAsync(b => b.Id == idProvider.Value);

            if (provider == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(provider)] = new string[] { $"No se encontró una PROVEEDOR con el id {idProvider.Value}." },
                    });
            }

            this.providerRepository.Delete(provider);
            await this.providerRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.Provider provider)
        {
            this.providerRepository.Update(provider);
            await this.providerRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.Provider>> GetAllAsync()
        {
            var list = await this.providerRepository.All()
                .Include(p => p.People)
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.Provider> GetByIdAsync(int? idProvider)
        {
            if (idProvider == null)
            {
                return null;
            }

            return await this.providerRepository.All()
                .Include(p => p.People)
                .FirstOrDefaultAsync(p => p.Id == idProvider.Value);
        }
    }
}
