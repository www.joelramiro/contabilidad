﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Provider
{
    public interface IProviderManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.Provider provider);

        Task<OperationResult> EditAsync(Database.Models.Accounting.Provider provider);

        Task<OperationResult> DeleteAsync(int? idProvider);

        Task<IEnumerable<Database.Models.Accounting.Provider>> GetAllAsync();

        Task<Database.Models.Accounting.Provider> GetByIdAsync(int? idProvider);
    }
}
