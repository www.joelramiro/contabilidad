﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Product
{
    public class ProductManager : IProductManager
    {
        private readonly IRepository<Database.Models.Accounting.Product> productRepository;

        public ProductManager(IRepository<Database.Models.Accounting.Product> productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.Product product)
        {
            if (product == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(product)] = new string[] { "El objeto PRODUCTO no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(product);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.Product product)
        {
            var exist = await this.productRepository.All()
                .AnyAsync(p => p.Name == product.Name && p.IdProductCategory == product.IdProductCategory && p.IdModel == product.IdModel && p.IdBrand == product.IdBrand && p.IdMeasurementUnit == product.IdMeasurementUnit);
            
            if (exist)
            {
                return new OperationResult(
                       new Dictionary<string, IEnumerable<string>>()
                       {
                           [nameof(product.Name)] = new string[] { $"Ya existe un PRODUCTO con la combinación {product.Name}-{nameof(product.ProductCategory)}-{nameof(product.Model)}-{nameof(product.Brand)}-{nameof(product.MeasurementUnit)}." },
                       });
            }

            this.productRepository.Create(product);
            await this.productRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idProduct)
        {
            if (idProduct == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idProduct)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var product = await this.GetByIdAsync(idProduct.Value);

            if (product == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(product)] = new string[] { $"No se encontró un PRODUCTO con el id {idProduct.Value}." },
                    });
            }

            this.productRepository.Delete(product);
            await this.productRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.Product product)
        {
            var exist = await this.productRepository.All()
                .AnyAsync(p => p.Name == product.Name && p.Id != product.Id && p.IdProductCategory == product.IdProductCategory && p.IdModel == product.IdModel && p.IdBrand == product.IdBrand && p.IdMeasurementUnit == product.IdMeasurementUnit);

            if (exist)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(product.Name)] = new string[] { $"Ya existe un PRODUCTO con la combinación {product.Name}-{nameof(product.ProductCategory)}-{nameof(product.Model)}-{nameof(product.Brand)}-{nameof(product.MeasurementUnit)}." },
                    });
            }

            this.productRepository.Update(product);
            await this.productRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.Product>> GetAllAsync()
        {
            var list = await this.productRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<IEnumerable<Database.Models.Accounting.Product>> GetAllWithDetailsAsync()
        {
            var list = await this.productRepository.All()
                .Include(p => p.Brand)
                .Include(p => p.Model)
                .Include(p => p.ProductCategory)
                .Include(p => p.MeasurementUnit)
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.Product> GetByIdAsync(int? idProduct)
        {
            if (idProduct == null)
            {
                return null;
            }

            return await this.productRepository.All()
                .FirstOrDefaultAsync(p => p.Id == idProduct.Value);
        }
        public async Task<Database.Models.Accounting.Product> GetByIdandDetailsAsync(int? idProduct)
        {
            if (idProduct == null)
            {
                return null;
            }

            return await this.productRepository.All()
                .Include(p => p.Brand)
                .Include(p => p.Model)
                .Include(p => p.ProductCategory)
                .Include(p => p.MeasurementUnit)
                .FirstOrDefaultAsync(p => p.Id == idProduct.Value);
        }
    }
}
