﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Product
{
    public interface IProductManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.Product product);

        Task<OperationResult> EditAsync(Database.Models.Accounting.Product product);

        Task<OperationResult> DeleteAsync(int? idProduct);

        Task<IEnumerable<Database.Models.Accounting.Product>> GetAllAsync();

        Task<IEnumerable<Database.Models.Accounting.Product>> GetAllWithDetailsAsync();

        Task<Database.Models.Accounting.Product> GetByIdAsync(int? idProduct);

        Task<Database.Models.Accounting.Product> GetByIdandDetailsAsync(int? idProduct);
    }
}
