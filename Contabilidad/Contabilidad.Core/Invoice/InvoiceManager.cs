﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Invoice
{
    public class InvoiceManager : IInvoiceManager
    {
        private readonly IRepository<Database.Models.Accounting.Invoice> invoiceRepository;

        public InvoiceManager(IRepository<Database.Models.Accounting.Invoice> invoiceRepository)
        {
            this.invoiceRepository = invoiceRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.Invoice invoice)
        {
            throw new NotImplementedException();
        }

        public async Task<OperationResult> DeleteAsync(int? idInvoice)
        {
            if (idInvoice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idInvoice)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var invoice = await this.invoiceRepository.FirstOrDefaultAsync(i => i.Id == idInvoice.Value);
            if (invoice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(invoice)] = new string[] { $"No se encontró una FACTURA DE VENTA con el id {idInvoice.Value}." },
                    });
            }

            this.invoiceRepository.Delete(invoice);
            await this.invoiceRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.Invoice invoice)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Database.Models.Accounting.Invoice>> GetAllAsync()
        {
            var list = await this.invoiceRepository.All()
                .Include(i => i.Client)
                .ThenInclude(i => i.People)
                .Include(i => i.CashInvoice)
                .Include(i => i.CreditInvoice)
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.Invoice> GetByIdAsync(int? idInvoice)
        {
            if (invoiceRepository == null)
            {
                return null;
            }

            return await this.invoiceRepository.All()
                .Include(d => d.CashInvoice)
                .Include(d => d.CreditInvoice)
                .Include(d => d.BranchOffice)
                .Include(p => p.Client)
                .ThenInclude(pr => pr.People)
                .Include(p => p.Usser)
                .ThenInclude(pr => pr.People)
                .FirstOrDefaultAsync(p => p.Id == idInvoice.Value);
        }
    }
}
