﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Invoice
{
    public interface IInvoiceManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.Invoice invoice);

        Task<OperationResult> EditAsync(Database.Models.Accounting.Invoice invoice);

        Task<OperationResult> DeleteAsync(int? idInvoice);

        Task<IEnumerable<Database.Models.Accounting.Invoice>> GetAllAsync();

        Task<Database.Models.Accounting.Invoice> GetByIdAsync(int? idInvoice);
    }
}
