﻿using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Warehouse
{
    public class WarehouseMovementManager : IWarehouseMovementManager
    {
        private readonly IRepository<WarehouseMovement> warehouseMovementRepository;
        private readonly IRepository<Database.Models.Accounting.Warehouse> warehouseRepository;

        public WarehouseMovementManager(
            IRepository<WarehouseMovement> warehouseMovementRepository,
            IRepository<Database.Models.Accounting.Warehouse> warehouseRepository)
        {
            this.warehouseMovementRepository = warehouseMovementRepository;
            this.warehouseRepository = warehouseRepository;
        }

        public async Task<OperationResult> CreateAsync(WarehouseMovement warehouseMovement)
        {
            if (warehouseMovement == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(warehouseMovement)] = new string[] { "El objeto no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(warehouseMovement);
        }

        private async Task<OperationResult> InnerCreateAsync(WarehouseMovement warehouseMovement)
        {
            var warehouse = await this.warehouseRepository
                .All()
                .Include(w => w.Details)
                .ThenInclude(d => d.Product)
                .FirstOrDefaultAsync(w => w.Id == warehouseMovement.IdWarehouseOrigin);

            if (warehouse == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(warehouseMovement)] = new string[] { $"No se encontro el almacén de origen con el id [{warehouseMovement.IdWarehouseOrigin}]." },
                    });
            }

            var warehouseOriginProducts = warehouse.Details;
            var detailProducts = warehouseMovement.Details.GroupBy(d => d.IdProduct).Select(d => new { idProduct = d.Key, Quantity = d.Sum(dd => dd.Quantity) });

            var resultJoin = warehouseOriginProducts
                .Join(detailProducts, d => d.IdProduct, d => d.idProduct, (origin, news) => new { origin.Product, originQuantity = origin.Quantity, newsQuantity = news.Quantity });

            var messages = new StringBuilder();

            foreach (var item in resultJoin)
            {
                if (item.originQuantity - item.newsQuantity < 0)
                {
                    messages.Append($"Las cantidades del producto {item.Product.Name} sobrepasa a la cantidad disponible de la factura.");
                }
            }

            if (messages.Length != 0)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(warehouseMovement)] = new string[] { messages.ToString() },
                    });
            }

            var warehousesDestination = await this.warehouseRepository.All()
                .Include(w => w.Details)
                .ThenInclude(w => w.Product).ToListAsync();

            var destinationJoin = warehousesDestination
                .Join(warehouseMovement.Details, w => w.Id, wm => wm.IdWarehouseDestination, (wDes, detail) => new { wDes, detail.IdProduct, detail.Quantity });

            var destinationJoinGroup = destinationJoin.GroupBy(d => new { d.IdProduct, d.wDes });
            var finalOriginResult = destinationJoin.GroupBy(d => d.IdProduct);
            
            foreach (var item in finalOriginResult)
            {
                warehouse.Details.First(d => d.IdProduct == item.Key).Quantity -= item.Sum(d => d.Quantity);
            }

            this.warehouseRepository.Update(warehouse);

            foreach (var item in destinationJoinGroup)
            {
                var detail = item.Key.wDes.Details.FirstOrDefault(d => d.IdProduct == item.Key.IdProduct);

                if (detail != null)
                {
                    detail.Quantity += item.Sum(d => d.Quantity);
                }
                else
                {
                    item.Key.wDes.Details.Add(new WarehouseDetail
                    {
                        IdProduct = item.Key.IdProduct,
                        Quantity = item.Sum(d => d.Quantity),
                    });
                }
            }

            var wDestGroup = destinationJoin.Select(d => d.wDes).Distinct();

            foreach (var item in wDestGroup)
            {
                this.warehouseRepository.Update(item);
            }

            this.warehouseMovementRepository.Create(warehouseMovement);
            await this.warehouseMovementRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idWarehouseMovement)
        {
            if (idWarehouseMovement == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idWarehouseMovement)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var warehouseMovement = await this.warehouseMovementRepository.All().FirstOrDefaultAsync(b => b.Id == idWarehouseMovement.Value);

            if (warehouseMovement == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(warehouseMovement)] = new string[] { $"No se encontró un movimiento con el id {idWarehouseMovement.Value}." },
                    });
            }

            try
            {
                this.warehouseMovementRepository.Delete(warehouseMovement);
                await this.warehouseMovementRepository.SaveChangesAsync();
                return new OperationResult(true);

            }
            catch (Exception)
            {
                return new OperationResult(new[] { "Error al eliminar el movimiento de la base de datos" });
            }
        }

        public async Task<OperationResult> EditAsync(WarehouseMovement warehouseMovement)
        {
            this.warehouseMovementRepository.Update(warehouseMovement);
            await this.warehouseMovementRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<WarehouseMovement>> GetAllAsync()
        {
            var list = await this.warehouseMovementRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<WarehouseMovement> GetByIdAsync(int? idWarehouseMovement)
        {
            if (idWarehouseMovement == null)
            {
                return null;
            }

            return await this.warehouseMovementRepository.All()
                .Include(d => d.Details)
                .ThenInclude(d => d.Product)
                .ThenInclude(d => d.MeasurementUnit)
                .FirstOrDefaultAsync(p => p.Id == idWarehouseMovement.Value);
        }
    }
}
