﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Warehouse
{
    public interface IWarehouseManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.Warehouse warehouse);

        Task<OperationResult> EditAsync(Database.Models.Accounting.Warehouse warehouse);

        Task<OperationResult> DeleteAsync(int? idWarehouse);

        Task<IEnumerable<Database.Models.Accounting.Warehouse>> GetAllAsync();

        Task<Database.Models.Accounting.Warehouse> GetByIdAsync(int? idWarehouse);
    }
}
