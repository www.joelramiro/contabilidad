﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Warehouse
{
    public interface IWarehouseMovementManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.WarehouseMovement warehouseMovement);

        Task<OperationResult> EditAsync(Database.Models.Accounting.WarehouseMovement warehouseMovement);

        Task<OperationResult> DeleteAsync(int? idWarehouseMovement);

        Task<IEnumerable<Database.Models.Accounting.WarehouseMovement>> GetAllAsync();

        Task<Database.Models.Accounting.WarehouseMovement> GetByIdAsync(int? idWarehouseMovement);
    }
}
