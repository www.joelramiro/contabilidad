﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.Warehouse
{
    public class WarehouseManager : IWarehouseManager
    {
        private readonly IRepository<Database.Models.Accounting.Warehouse> warehouseRepository;

        public WarehouseManager(IRepository<Database.Models.Accounting.Warehouse> warehouseRepository)
        {
            this.warehouseRepository = warehouseRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.Warehouse warehouse)
        {
            if (warehouse == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(warehouse)] = new string[] { "El objeto ALMACÉN no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(warehouse);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.Warehouse warehouse)
        {
            var any = await this.warehouseRepository.All().AnyAsync(a => a.Code == warehouse.Code);
            if (any)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(warehouse)] = new string[] { "El objeto ALMACÉN no puede crear porque ya existe uno con el mismo código." },
                    });
            }
            this.warehouseRepository.Create(warehouse);
            await this.warehouseRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idWarehouse)
        {
            if (idWarehouse == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idWarehouse)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var warehouse = await this.warehouseRepository.All().FirstOrDefaultAsync(b => b.Id == idWarehouse.Value);

            if (warehouse == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(warehouse)] = new string[] { $"No se encontró una ALMACÉN con el id {idWarehouse.Value}." },
                    });
            }

            try
            {
                this.warehouseRepository.Delete(warehouse);
                await this.warehouseRepository.SaveChangesAsync();
                return new OperationResult(true);

            }
            catch (Exception)
            {
                return new OperationResult(new[] { "Error al eliminar el ALMACÉN de la base de datos" });
            }
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.Warehouse warehouse)
        {

            var any = await this.warehouseRepository.All().AnyAsync(a => a.Code == warehouse.Code && a.Id != warehouse.Id);
            if (any)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(warehouse)] = new string[] { "El objeto ALMACÉN no puede editar porque ya existe uno con el mismo código." },
                    });
            }

            this.warehouseRepository.Update(warehouse);
            await this.warehouseRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.Warehouse>> GetAllAsync()
        {
            var list = await this.warehouseRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.Warehouse> GetByIdAsync(int? idWarehouse)
        {
            if (idWarehouse == null)
            {
                return null;
            }

            return await this.warehouseRepository.All()
                .Include(d => d.Details)
                .ThenInclude(d => d.Product)
                .ThenInclude(d => d.MeasurementUnit)
                .FirstOrDefaultAsync(p => p.Id == idWarehouse.Value);
        }
    }
}
