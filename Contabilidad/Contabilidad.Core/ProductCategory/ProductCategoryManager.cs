﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.ProductCategory
{
    public class ProductCategoryManager : IProductCategoryManager
    {
        private readonly IRepository<Database.Models.Accounting.ProductCategory> productCategoryRepository;

        public ProductCategoryManager(IRepository<Database.Models.Accounting.ProductCategory> productCategoryRepository)
        {
            this.productCategoryRepository = productCategoryRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.ProductCategory productCategory)
        {
            if (productCategory == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productCategory)] = new string[] { "El objeto CATEGORIA DEL PRODUCTO no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(productCategory);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.ProductCategory productCategory)
        {
            var exist = await this.productCategoryRepository.All().AnyAsync(p => p.Name == productCategory.Name);
            if (exist)
            {
                return new OperationResult(
                       new Dictionary<string, IEnumerable<string>>()
                       {
                           [nameof(productCategory.Name)] = new string[] { $"Ya existe una CATEGORIA DEL PRODUCTO con el nombre {productCategory.Name}." },
                       });
            }

            this.productCategoryRepository.Create(productCategory);
            await this.productCategoryRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idProductCategory)
        {
            if (idProductCategory == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idProductCategory)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var productCategory = await this.productCategoryRepository.All().Include(b => b.Products).FirstOrDefaultAsync(b => b.Id == idProductCategory.Value);

            if (productCategory == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productCategory)] = new string[] { $"No se encontró una CATEGORIA DEL PRODUCTO con el id {idProductCategory.Value}." },
                    });
            }

            if (productCategory.Products.Any())
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productCategory)] = new string[] { $"No se puede eliminar la CATEGORIA DEL PRODUCTO debido a que esta siendo utilizada por uno(varios) PRODUCTOS" },
                    });
            }

            this.productCategoryRepository.Delete(productCategory);
            await this.productCategoryRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.ProductCategory productCategory)
        {
            var exist = await this.productCategoryRepository.All().AnyAsync(p => p.Name == productCategory.Name && p.Id != productCategory.Id);

            if (exist)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productCategory.Name)] = new string[] { $"Ya existe una CATEGORIA DEL PRODUCTO con el nombre {productCategory.Name}." },
                    });
            }

            this.productCategoryRepository.Update(productCategory);
            await this.productCategoryRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.ProductCategory>> GetAllAsync()
        {
            var list = await this.productCategoryRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.ProductCategory> GetByIdAsync(int? idProductCategory)
        {
            if (idProductCategory == null)
            {
                return null;
            }

            return await this.productCategoryRepository.All()
                .FirstOrDefaultAsync(p => p.Id == idProductCategory.Value);
        }
    }
}
