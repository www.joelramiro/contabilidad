﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.ProductCategory
{
    public interface IProductCategoryManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.ProductCategory productCategory);

        Task<OperationResult> EditAsync(Database.Models.Accounting.ProductCategory productCategory);

        Task<OperationResult> DeleteAsync(int? idProductCategory);

        Task<IEnumerable<Database.Models.Accounting.ProductCategory>> GetAllAsync();

        Task<Database.Models.Accounting.ProductCategory> GetByIdAsync(int? idProductCategory);
    }
}
