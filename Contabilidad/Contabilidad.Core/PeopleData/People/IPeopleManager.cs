﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.PeopleData.People
{
    public interface IPeopleManager
    {
        Task<OperationResult> CreateAsync(Database.Model.People.People people);

        Task<OperationResult> EditAsync(Database.Model.People.People people);

        Task<OperationResult> DeleteAsync(int? idPeople);

        Task<IEnumerable<Database.Model.People.People>> GetAllAsync();

        Task<Database.Model.People.People> GetByIdAsync(int? idPeople);
    }
}
