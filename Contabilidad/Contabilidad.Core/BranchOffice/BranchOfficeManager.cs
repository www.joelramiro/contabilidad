﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.BranchOffice
{
    public class BranchOfficeManager : IBranchOfficeManager
    {
        private readonly IRepository<Database.Models.Accounting.BranchOffice> branchOfficeRepository;

        public BranchOfficeManager(IRepository<Database.Models.Accounting.BranchOffice> branchOfficeRepository)
        {
            this.branchOfficeRepository = branchOfficeRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.BranchOffice branchOffice)
        {
            if (branchOffice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(branchOffice)] = new string[] { "El objeto SUCURSAL no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(branchOffice);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.BranchOffice branchOffice)
        {
            var any = await this.branchOfficeRepository.All().AnyAsync(a => a.Code == branchOffice.Code);
            if (any)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(branchOffice)] = new string[] { "El objeto SUCURSAL no puede crear porque ya existe una con el mismo código." },
                    });
            }
            this.branchOfficeRepository.Create(branchOffice);
            await this.branchOfficeRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idBranchOffice)
        {
            if (idBranchOffice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idBranchOffice)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var branchOffice = await this.branchOfficeRepository.All().FirstOrDefaultAsync(b => b.Id == idBranchOffice.Value);

            if (branchOffice == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(branchOffice)] = new string[] { $"No se encontró una SUCURSAL con el id {idBranchOffice.Value}." },
                    });
            }

            try
            {
                this.branchOfficeRepository.Delete(branchOffice);
                await this.branchOfficeRepository.SaveChangesAsync();
                return new OperationResult(true);

            }
            catch (Exception)
            {
                return new OperationResult(new[] { "Error al eliminar el SUCURSAL de la base de datos" });
            }
        }

        public async Task<OperationResult> EditAsync(Database.Models.Accounting.BranchOffice branchOffice)
        {

            var any = await this.branchOfficeRepository.All().AnyAsync(a => a.Code == branchOffice.Code && a.Id != branchOffice.Id);
            if (any)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(branchOffice)] = new string[] { "El objeto SUCURSAL no puede editar porque ya existe una con el mismo código." },
                    });
            }

            this.branchOfficeRepository.Update(branchOffice);
            await this.branchOfficeRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.BranchOffice>> GetAllAsync()
        {
            var list = await this.branchOfficeRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.BranchOffice> GetByIdAsync(int? idBranchOffice)
        {
            if (idBranchOffice == null)
            {
                return null;
            }

            return await this.branchOfficeRepository.All()
                .FirstOrDefaultAsync(p => p.Id == idBranchOffice.Value);
        }
    }
}
