﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.BranchOffice
{
    public interface IBranchOfficeManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.BranchOffice branchOffice);

        Task<OperationResult> EditAsync(Database.Models.Accounting.BranchOffice branchOffice);

        Task<OperationResult> DeleteAsync(int? idBranchOffice);

        Task<IEnumerable<Database.Models.Accounting.BranchOffice>> GetAllAsync();

        Task<Database.Models.Accounting.BranchOffice> GetByIdAsync(int? idBranchOffice);
    }
}
