﻿using Contabilidad.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.ProductModel
{
    public class ProductModelManager : IProductModelManager
    {
        private readonly IRepository<Database.Models.Accounting.ProductModel> productModelRepository;

        public ProductModelManager(IRepository<Database.Models.Accounting.ProductModel> productModelRepository)
        {
            this.productModelRepository = productModelRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Models.Accounting.ProductModel productModel)
        {
            if (productModel == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productModel)] = new string[] { "El objeto MODELO DEL PRODUCTO no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(productModel);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Models.Accounting.ProductModel productModel)
        {
            var exist = await this.productModelRepository.All().AnyAsync(p => p.Name == productModel.Name);
            if (exist)
            {
                return new OperationResult(
                       new Dictionary<string, IEnumerable<string>>()
                       {
                           [nameof(productModel.Name)] = new string[] { $"Ya existe un MODELO DEL PRODUCTO con el nombre {productModel.Name}." },
                       });
            }

            this.productModelRepository.Create(productModel);
            await this.productModelRepository.SaveChangesAsync();
            return new OperationResult(true);
        }


        public async Task<OperationResult> DeleteAsync(int? idProductModel)
        {
            if (idProductModel == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idProductModel)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var productModel = await this.productModelRepository.All().Include(b => b.Products).FirstOrDefaultAsync(b => b.Id == idProductModel.Value);

            if (productModel == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productModel)] = new string[] { $"No se encontró un MODELO DEL PRODUCTO con el id {idProductModel.Value}." },
                    });
            }

            if (productModel.Products.Any())
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productModel)] = new string[] { $"No se puede eliminar EL MODELO DEL PRODUCTO debido a que esta siendo utilizado por uno(varios) PRODUCTOS" },
                    });
            }

            this.productModelRepository.Delete(productModel);
            await this.productModelRepository.SaveChangesAsync();
            return new OperationResult(true);
        }


        public async Task<OperationResult> EditAsync(Database.Models.Accounting.ProductModel productModel)
        {
            var exist = await this.productModelRepository.All().AnyAsync(p => p.Name == productModel.Name && p.Id != productModel.Id);

            if (exist)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(productModel.Name)] = new string[] { $"Ya existe un MODELO DEL PRODUCTO con el nombre {productModel.Name}." },
                    });
            }

            this.productModelRepository.Update(productModel);
            await this.productModelRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Models.Accounting.ProductModel>> GetAllAsync()
        {
            var list = await this.productModelRepository.All()
                .ToListAsync();

            return list;
        }

        public async Task<Database.Models.Accounting.ProductModel> GetByIdAsync(int? idProductModel)
        {
            if (idProductModel == null)
            {
                return null;
            }

            return await this.productModelRepository.All()
                .FirstOrDefaultAsync(p => p.Id == idProductModel.Value);
        }
    }
}
