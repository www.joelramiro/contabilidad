﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contabilidad.Core.ProductModel
{
    public interface IProductModelManager
    {
        Task<OperationResult> CreateAsync(Database.Models.Accounting.ProductModel productModel);

        Task<OperationResult> EditAsync(Database.Models.Accounting.ProductModel productModel);

        Task<OperationResult> DeleteAsync(int? idProductModel);

        Task<IEnumerable<Database.Models.Accounting.ProductModel>> GetAllAsync();

        Task<Database.Models.Accounting.ProductModel> GetByIdAsync(int? idProductModel);
    }
}
