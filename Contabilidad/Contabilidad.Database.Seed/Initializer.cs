﻿
namespace Contabilidad.Database.Seed
{
    using System;
    using System.Threading.Tasks;
    using Contabilidad.Database.Context;
    using Microsoft.Extensions.DependencyInjection;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Models.Accounting;
    using System.Collections.Generic;

    public static class Initializer
    {
        public static async Task SeedAsync(IServiceProvider serviceProvider)
        {
            bool databaseCreated = false;
            bool databaseConfigurationCreated = false;
            try
            {
                databaseCreated = await serviceProvider.GetRequiredService<ContabilidadContext>().Database.EnsureCreatedAsync();
                databaseConfigurationCreated = await serviceProvider.GetRequiredService<ContabilidadSettingsContext>().Database.EnsureCreatedAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

            if (!databaseCreated || !databaseConfigurationCreated)
            {
                return;
            }

            var context = serviceProvider.GetRequiredService<ContabilidadContext>();

            await CreateSeed(context);
            /*try
            {
                await configurationContext.SaveChangesAsync();
            }
            finally
            {
                configurationContext.Database.CloseConnection();
            }*/
        }

        private static async Task CreateSeed(ContabilidadContext context)
        {
            try
            {
                // Sex
                var sexType = new SexType { Value = "Hombre" };
                context.SexType.Add(sexType);
                context.SexType.Add(new SexType { Value = "Mujer" });
                await context.SaveChangesAsync();

                // BloodType
                context.BloodType.Add(new BloodType { Type = "A+" });
                context.BloodType.Add(new BloodType { Type = "B+" });
                context.BloodType.Add(new BloodType { Type = "AB+" });
                context.BloodType.Add(new BloodType { Type = "O+" });
                context.BloodType.Add(new BloodType { Type = "A-" });
                context.BloodType.Add(new BloodType { Type = "B-" });
                context.BloodType.Add(new BloodType { Type = "AB-" });
                context.BloodType.Add(new BloodType { Type = "O-" });
                await context.SaveChangesAsync();

                var civilStatus = new CivilStatus { Description = "Soltero" };
                // CivilStatus
                context.CivilStatus.Add(civilStatus);
                context.CivilStatus.Add(new CivilStatus { Description = "Casado" });
                context.CivilStatus.Add(new CivilStatus { Description = "Union Libre" });
                await context.SaveChangesAsync();

                // Country
                var Honduras = new Country { Name = "Honduras" };
                context.Country.Add(Honduras);
                await context.SaveChangesAsync();

                var educationLevel = new EducationLevel { Level = "Primaria" };
                // EducationLevel
                context.EducationLevel.Add(educationLevel);
                context.EducationLevel.Add(new EducationLevel { Level = "Secundaria" });
                context.EducationLevel.Add(new EducationLevel { Level = "Profesional" });
                context.EducationLevel.Add(new EducationLevel { Level = "Pasante Universitario" });
                context.EducationLevel.Add(new EducationLevel { Level = "Universitario" });
                await context.SaveChangesAsync();

                // Nacionality
                context.Nacionality.Add(new Nacionality { IdCountry = Honduras.Id });
                await context.SaveChangesAsync();

                // PriorityLevel
                context.PriorityLevel.Add(new PriorityLevel { Level = "Bajo" });
                context.PriorityLevel.Add(new PriorityLevel { Level = "Medio" });
                context.PriorityLevel.Add(new PriorityLevel { Level = "Alto" });
                await context.SaveChangesAsync();

                // RelationTypes
                context.RelationType.Add(new RelationType { Name = "Abuelo" });
                context.RelationType.Add(new RelationType { Name = "Abuela" });
                context.RelationType.Add(new RelationType { Name = "Tio" });
                context.RelationType.Add(new RelationType { Name = "Tia" });
                context.RelationType.Add(new RelationType { Name = "Primo" });
                context.RelationType.Add(new RelationType { Name = "Prima" });
                context.RelationType.Add(new RelationType { Name = "Esposo" });
                context.RelationType.Add(new RelationType { Name = "Esposa" });
                context.RelationType.Add(new RelationType { Name = "Padre" });
                context.RelationType.Add(new RelationType { Name = "Madre" });
                context.RelationType.Add(new RelationType { Name = "Hermano" });
                context.RelationType.Add(new RelationType { Name = "Hermana" });
                context.RelationType.Add(new RelationType { Name = "Hijo" });
                context.RelationType.Add(new RelationType { Name = "Hija" });
                await context.SaveChangesAsync();


                var product = new Product
                {
                    Description = "Talla 20",
                    Name = "Zapato caballero",
                    Brand = new Brand
                    {
                        Name = "Nike",
                    },
                    MeasurementUnit = new MeasurementUnit
                    {
                        MeasurementName = "Caja individual",
                    },
                    Model = new ProductModel
                    {
                        Name = "ModelTest"
                    },
                    ProductCategory = new ProductCategory
                    {
                        Name = "Calzado",
                        Description = "general",
                    },
                };

                var product2 = new Product
                {
                    Description = "camisa amarilla",
                    Name = "Camisa mujer",
                    Brand = new Brand
                    {
                        Name = "Zara",
                    },
                    MeasurementUnit = new MeasurementUnit
                    {
                        MeasurementName = "pieza",
                    },
                    Model = new ProductModel
                    {
                        Name = "Desmangada"
                    },
                    ProductCategory = new ProductCategory
                    {
                        Name = "Vestuario",
                        Description = "general",
                    },
                };

                context.Product.Add(product);
                context.Product.Add(product2);

                await context.SaveChangesAsync();

                var currencyLps = new Models.Currency
                {
                    Name = "Lempira ",
                    Simbol = "L",
                    Description = "Base",
                };

                context.Currency.Add(currencyLps);

                context.Currency.Add(new Models.Currency
                {
                    Name = "Dolar ",
                    Simbol = "$",
                    Description = "Extranjero",
                });


                context.Currency.Add(new Models.Currency
                {
                    Name = "Euro",
                    Simbol = "€",
                    Description = "Extranjero",
                });

                await context.SaveChangesAsync();

                var provider = new Provider
                {
                    People = new People
                    {
                        Identity = "1602199700135",
                        FirstName = "Joel",
                        SecondName = "Ramiro",
                        MiddleName = "Caballero",
                        LastName = "Castellanos",
                        BirthDate = DateTime.Now,
                        SexType = sexType,
                        CivilStatus = civilStatus,
                        EducationLevel = educationLevel,
                    },
                    BusinessName = "Leitz",
                    Description = "Proveedor de prueba",
                };

                context.Provider.Add(provider);

                await context.SaveChangesAsync();

                context.Warehouse.Add(new Warehouse
                {
                    IsActive = true,
                    Code = "A1",
                    Description = "Almacén principal",
                    Location = new Location
                    {
                        Accuracy = 0,
                        Altitude = 0,
                        AltitudeAccuracy = 0,
                        Heading = 0,
                        Latitude = 0,
                        Longitude = 0,
                    },
                    Details = new List<WarehouseDetail>()
                {
                    new WarehouseDetail
                    {
                        IdProduct = product.Id,
                        Quantity = 10
                    },
                    new WarehouseDetail
                    {
                        IdProduct = product2.Id,
                        Quantity = 15
                    },
                },
                });

                context.Warehouse.Add(new Warehouse
                {
                    IsActive = true,
                    Code = "A2",
                    Description = "Almacén secundario",
                    Location = new Location
                    {
                        Accuracy = 0,
                        Altitude = 0,
                        AltitudeAccuracy = 0,
                        Heading = 0,
                        Latitude = 0,
                        Longitude = 0,
                    },
                });

                context.Warehouse.Add(new Warehouse
                {
                    IsActive = true,
                    Code = "B1",
                    Description = "Almacén Boulevar AB",
                    Location = new Location
                    {
                        Accuracy = 0,
                        Altitude = 0,
                        AltitudeAccuracy = 0,
                        Heading = 0,
                        Latitude = 0,
                        Longitude = 0,
                    },
                });
                await context.SaveChangesAsync();

                context.PurchaseInvoice.Add(new PurchaseInvoice
                {
                    Code = "EF01-01",
                    IsAssignedToWarehouse = false,
                    Comments = "Comentario de prueba",
                    CreatedDate = DateTime.Now,
                    IdCurrency = currencyLps.Id,
                    Disccount = 1000,
                    IdProvider = provider.Id,
                    //IdUsser = 0,  CODE HERE   CODE HERE   CODE HERE   CODE HERE
                    Location = new Location
                    {
                        Accuracy = 0,
                        Altitude = 0,
                        AltitudeAccuracy = 0,
                        Heading = 0,
                        Latitude = 0,
                        Longitude = 0,
                    },
                    InvoiceType = InvoiceType.Cash,
                    IsPaid = true,
                    Isv = 15,
                    Total = 6000,
                    SubTotal = 4900,
                    Details = new List<PurchaseInvoiceDetail>
                {
                    new PurchaseInvoiceDetail
                    {
                        IdProduct = product.Id,
                        Quantity = 3,
                        UnitPrice = 1000,
                        Total = 3000,
                    },
                    new PurchaseInvoiceDetail
                    {
                        IdProduct = product2.Id,
                        Quantity = 6,
                        UnitPrice = 500,
                        Total = 3000,
                    },
                },
                });

                await context.SaveChangesAsync();

                var branchOffice = context.BranchOffice.Add(new BranchOffice
                {
                    IsActive = true,
                    Code = "S1",
                    Description = "Sucursal principal",
                    Location = new Location
                    {
                        Accuracy = 0,
                        Altitude = 0,
                        AltitudeAccuracy = 0,
                        Heading = 0,
                        Latitude = 0,
                        Longitude = 0,
                    },
                });

                context.BranchOffice.Add(new BranchOffice
                {
                    IsActive = true,
                    Code = "S2",
                    Description = "Sucursal secundaria",
                    Location = new Location
                    {
                        Accuracy = 0,
                        Altitude = 0,
                        AltitudeAccuracy = 0,
                        Heading = 0,
                        Latitude = 0,
                        Longitude = 0,
                    },
                });
                await context.SaveChangesAsync();

                var client = context.Client.Add(new Client
                {
                    BusinessName = "Bodega Salomon",
                    Description = "Bodeha de prueba",
                    RTN = "1602-1997-00135-1",
                    IdPeople = 1,
                });
                await context.SaveChangesAsync();

                var invoice = context.Invoice.Add(new Invoice
                {
                    IdBranchOffice = branchOffice.Entity.Id,                  
                    Code = "char-001-205",
                    CreationDate = DateTime.Now,
                    InvoiceType = InvoiceType.Cash,
                    Total = 120000,
                    IdClient = client.Entity.Id,
                });

                await context.SaveChangesAsync();

                var CashInvoice = context.CashInvoice.Add(new CashInvoice
                {
                    IdInvoice = invoice.Entity.Id,
                    Details = new List<CashInvoiceDetail>()
                    {
                        new CashInvoiceDetail
                        {
                            IdProduct = product.Id,
                            DiscountPercent = 10,
                            Quantity = 10,
                            UnitPrice = 150,
                            Total = 1350,
                        },
                        new CashInvoiceDetail
                        {
                            IdProduct = product2.Id,
                            DiscountPercent = 10,
                            Quantity = 3,
                            UnitPrice = 1500,
                            Total = 4050,
                        },
                    },
                });

                await context.SaveChangesAsync();

              

                var CreditInvoice = context.CreditInvoice.Add(new CreditInvoice
                {
                    IdInvoice = invoice.Entity.Id,
                    Details = new List<CreditInvoiceDetail>()
                    {
                        new CreditInvoiceDetail
                        {
                            IdProduct = product.Id,
                            DiscountPercent = 10,
                            ExtraCostPercent = 10,
                            Quantity = 10,
                            UnitPrice = 150,
                            Total = 1350,
                        },
                        new CreditInvoiceDetail
                        {
                            IdProduct = product2.Id,
                            DiscountPercent = 10,
                            Quantity = 3,
                            UnitPrice = 1500,
                            Total = 4050,
                        },
                    },
                    IsPaid = false,
                    Prima = 1500,
                    Vouchers = new List<CreditInvoiceVoucher>()
                    { 
                        new CreditInvoiceVoucher
                        {
                            Code = "VOUCHER-01",
                            CreationDate  =DateTime.Now,
                            Total = 500,
                        },
                    },
                    Fee = new Fee 
                    {
                        Description = "Fee description",
                        ExtraPorcent = 2,
                        Months = 12,
                        MontlyPayment = 1500,
                        Percent = 2,
                    },
                });

                await context.SaveChangesAsync();
                var invoice2 = context.Invoice.Add(new Invoice
                {
                    IdBranchOffice = branchOffice.Entity.Id,
                    Code = "char-001-206",
                    CreationDate = DateTime.Now,
                    InvoiceType = InvoiceType.Credit,
                    Total = 120000,
                    IdClient = client.Entity.Id,
                    IdCreditInvoice = CreditInvoice.Entity.Id, 
                });

                await context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                var m = e.Message;
            }
        }
    }
}