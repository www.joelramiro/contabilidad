﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contabilidad.Database.Models.Accounting
{
    public class AssignPurchaseToWarehouse
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }
        
        [ForeignKey(nameof(Usser))]
        public int? IdUsser { get; set; }

        [ForeignKey(nameof(PurchaseInvoice))]
        public int IdPurchaseInvoice { get; set; }

        public Usser Usser { get; set; }

        public PurchaseInvoice PurchaseInvoice { get; set; }

        public ICollection<AssignPurchaseToWarehouseDetail> Detail { get; set; }
    }
}
