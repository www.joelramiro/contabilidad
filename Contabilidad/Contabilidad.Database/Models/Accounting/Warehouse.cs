﻿// <copyright file="Warehouse.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Warehouse
    {
        public int Id { get; set; }

        [ForeignKey(nameof(Location))]
        public int IdLocation { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }

        public bool IsActive { get; set; }

        public ICollection<WarehouseDetail> Details { get; set; }

        public Location Location { get; set; }
    }
}