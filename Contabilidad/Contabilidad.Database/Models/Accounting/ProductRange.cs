﻿// <copyright file="ProductRange.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    public class ProductRange
    {
        public int Id { get; set; }

        public int MinimQuantity { get; set; }

        public int MaxQuantity { get; set; }
    }
}