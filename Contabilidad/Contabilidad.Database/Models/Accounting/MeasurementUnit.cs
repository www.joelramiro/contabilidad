﻿// <copyright file="MeasurementUnit.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

using System.Collections.Generic;

namespace Contabilidad.Database.Models.Accounting
{
    public class MeasurementUnit
    {
        public int Id { get; set; }

        public string MeasurementName { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}