﻿// <copyright file="CashInvoice.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class CashInvoice
    {
        public int Id { get; set; }

        [ForeignKey(nameof(Invoice))]
        public int IdInvoice { get; set; }

        public Invoice Invoice { get; set; }

        public ICollection<CashInvoiceDetail> Details { get; set; }
    }
}