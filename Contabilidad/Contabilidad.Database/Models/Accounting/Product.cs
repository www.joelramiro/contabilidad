﻿// <copyright file="Product.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [ForeignKey(nameof(MeasurementUnit))]
        public int IdMeasurementUnit { get; set; }

        public byte[] Image { get; set; }

        [ForeignKey(nameof(ProductCategory))]
        public int IdProductCategory { get; set; }

        [ForeignKey(nameof(Brand))]
        public int IdBrand { get; set; }

        [ForeignKey(nameof(Model))]
        public int IdModel { get; set; }

        public MeasurementUnit MeasurementUnit { get; set; }

        public ProductCategory ProductCategory { get; set; }

        public Brand Brand { get; set; }

        public ProductModel Model { get; set; }
    }
}