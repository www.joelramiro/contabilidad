﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Contabilidad.Database.Models.Accounting
{
    public class WarehouseMovement
    {
        public int Id { get; set; }

        public DateTime MovementDate { get; set; }

        [ForeignKey(nameof(Usser))]
        public int? IdUsser { get; set; }

        public Usser Usser { get; set; }

        [ForeignKey(nameof(Warehouse))]
        public int IdWarehouseOrigin { get; set; }

        public Warehouse Warehouse { get; set; }

        public ICollection<WarehouseMovementDetail> Details { get; set; }
    }
}
