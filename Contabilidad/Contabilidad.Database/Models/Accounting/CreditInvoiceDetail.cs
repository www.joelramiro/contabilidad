﻿// <copyright file="CreditInvoiceDetail.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class CreditInvoiceDetail
    {
        public int Id { get; set; }

        [ForeignKey(nameof(CreditInvoice))]
        public int IdCreditInvoice { get; set; }

        [ForeignKey(nameof(Product))]
        public int IdProduct { get; set; }

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public double DiscountPercent { get; set; }

        public decimal Percent { get; set; }

        public double ExtraCostPercent { get; set; }

        public decimal CostPercent()
        {
            return this.UnitPrice * this.Quantity * (decimal)this.ExtraCostPercent / 100;
        }

        public decimal Total { get; set; }

        public CreditInvoice CreditInvoice { get; set; }

        public Product Product { get; set; }
    }
}