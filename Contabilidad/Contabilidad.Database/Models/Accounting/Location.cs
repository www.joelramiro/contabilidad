﻿// <copyright file="Location.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    public class Location
    {
        public int Id { get; set; }

        public double Accuracy { get; set; }

        public double? Altitude { get; set; }

        public double? AltitudeAccuracy { get; set; }

        public double? Heading { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}