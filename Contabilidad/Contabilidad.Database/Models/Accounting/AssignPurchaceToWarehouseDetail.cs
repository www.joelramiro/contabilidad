﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Contabilidad.Database.Models.Accounting
{
    public class AssignPurchaseToWarehouseDetail
    {
        public int Id { get; set; }

        [ForeignKey(nameof(AssignPurchaseToWarehouse))]
        public int IdAssignPurchaseToWarehouse { get; set; }

        [ForeignKey(nameof(Warehouse))]
        public int IdWarehouse { get; set; }

        [ForeignKey(nameof(Product))]
        public int IdProduct { get; set; }

        public int Quantity { get; set; }

        public decimal PurchaseUnitPrice { get; set; }

        public Warehouse Warehouse { get; set; }

        public Product Product { get; set; }

        public AssignPurchaseToWarehouse AssignPurchaseToWarehouse { get; set; }
    }
}