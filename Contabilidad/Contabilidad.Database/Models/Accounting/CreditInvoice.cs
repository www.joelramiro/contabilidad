﻿// <copyright file="CreditInvoice.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class CreditInvoice
    {
        public int Id { get; set; }

        [ForeignKey(nameof(Invoice))]
        public int IdInvoice { get; set; }

        [ForeignKey(nameof(Fee))]
        public int IdFee { get; set; }

        public decimal Prima { get; set; }

        public Invoice Invoice { get; set; }

        public Fee Fee { get; set; }

        public bool IsPaid { get; set; }

        public ICollection<CreditInvoiceDetail> Details { get; set; }

        public ICollection<CreditInvoiceVoucher> Vouchers { get; set; }
    }
}