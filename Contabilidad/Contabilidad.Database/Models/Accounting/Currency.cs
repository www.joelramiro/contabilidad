﻿using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contabilidad.Database.Models
{
    public class Currency
    {
        public int Id { get; set; }

        public string Simbol { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<PurchaseInvoice> PurchaseInvoices { get; set; }
        public ICollection<CurrencyConversion> CurrencyOriginConversions { get; set; }
        public ICollection<CurrencyConversion> CurrencyDestinationConversions { get; set; }
    }
}
