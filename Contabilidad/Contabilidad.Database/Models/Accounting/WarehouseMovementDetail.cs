﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Contabilidad.Database.Models.Accounting
{
    public class WarehouseMovementDetail
    {
        public int Id { get; set; }

        [ForeignKey(nameof(WarehouseMovement))]
        public int IdWarehouseMovement { get; set; }

        public WarehouseMovement WarehouseMovement { get; set; }

        [ForeignKey(nameof(Product))] 
        public int IdProduct { get; set; }

        [ForeignKey(nameof(WarehouseDestination))]
        public int? IdWarehouseDestination { get; set; }

        public Warehouse WarehouseDestination { get; set; }

        public Product Product { get; set; }

        public int Quantity { get; set; }
    }
}