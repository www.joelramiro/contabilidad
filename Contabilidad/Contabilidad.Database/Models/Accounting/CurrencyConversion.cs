﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Contabilidad.Database.Models.Accounting
{
    public class CurrencyConversion
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        [ForeignKey(nameof(CurrencyOrigin))]
        public int IdCurrencyOrigin { get; set; }
        
        [ForeignKey(nameof(CurrencyDestination))]
        public int IdCurrencyDestination { get; set; }

        public decimal ValueConversion { get; set; }

        [InverseProperty(nameof(Currency.CurrencyOriginConversions))]
        public virtual Currency CurrencyOrigin { get; set; }

        [InverseProperty(nameof(Currency.CurrencyDestinationConversions))]
        public virtual Currency CurrencyDestination { get; set; }
    }
}
