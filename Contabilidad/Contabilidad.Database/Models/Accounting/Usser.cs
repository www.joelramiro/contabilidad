﻿// <copyright file="Usser.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

using Contabilidad.Database.Model.People;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contabilidad.Database.Models.Accounting
{
    public class Usser
    {
        public int Id { get; set; }

        [ForeignKey(nameof(People))]
        public int IdPeople { get; set; }

        public People People { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
        public ICollection<CreditInvoiceVoucher> CreditInvoiceVouchers { get; set; }
    }
}