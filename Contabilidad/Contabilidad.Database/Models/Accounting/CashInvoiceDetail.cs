﻿// <copyright file="CashInvoiceDetail.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class CashInvoiceDetail
    {
        public int Id { get; set; }

        [ForeignKey(nameof(CashInvoice))]
        public int IdCashhInvoice { get; set; }

        [ForeignKey(nameof(Product))]
        public int IdProduct { get; set; }

        public decimal UnitPrice { get; set; }

        public int Quantity { get; set; }

        public double DiscountPercent { get; set; }

        public decimal Percent()
        {
            return (decimal)DiscountPercent * (UnitPrice * Quantity) / 100;
        }

        public decimal Total { get; set; }

        public CashInvoice CashInvoice { get; set; }

        public Product Product { get; set; }
    }
}