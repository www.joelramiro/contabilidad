﻿// <copyright file="InvoiceType.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    public enum InvoiceType
    {
        Cash = 0,
        Credit = 1
    }
}