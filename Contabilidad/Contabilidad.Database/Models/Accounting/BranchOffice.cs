﻿// <copyright file="BranchOffice.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class BranchOffice
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
        
        public bool IsActive { get; set; }

        [ForeignKey(nameof(Location))]
        public int IdLocation { get; set; }

        public Location Location { get; set; }
    }
}
