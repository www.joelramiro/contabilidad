﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Contabilidad.Database.Models.Accounting
{
    public class PurchaseInvoiceDetail
    {
        public int Id { get; set; }

        [ForeignKey(nameof(PurchaseInvoice))]
        public int IdPurchaseInvoice { get; set; }

        public PurchaseInvoice PurchaseInvoice { get; set; }

        [ForeignKey(nameof(Product))]
        public int IdProduct { get; set; }

        public Product Product { get; set; }

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Total { get; set; }
    }
}