﻿// <copyright file="ProductPrice.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ProductPrice
    {
        public int Id { get; set; }

        [ForeignKey(nameof(Product))]
        public int IdProduct { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal SalePrice { get; set; }

        public int? Years { get; set; }

        public int? Months { get; set; }

        public int? Days { get; set; }


        [ForeignKey(nameof(Currency))]
        public int IdCurrency { get; set; }

        public Currency Currency { get; set; }

        public Product Product { get; set; }

        public string WarrantyTime()
        {
            var zeroValue = 0.ToString("D2");
            var result = string.Concat(this.Years != null ? this.Years.Value.ToString("D2") : zeroValue,"/", this.Months != null ? this.Months.Value.ToString("D2") : zeroValue,"/", this.Days != null ? this.Days.Value.ToString("D2") : zeroValue);
            
            return result;
        }
    }
}