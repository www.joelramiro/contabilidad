﻿// <copyright file="Client.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

using Contabilidad.Database.Model.People;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contabilidad.Database.Models.Accounting
{
    public class Client
    {
        public int Id { get; set; }

        [ForeignKey(nameof(People))]
        public int IdPeople { get; set; }

        public People People { get; set; }

        public string Description { get; set; }

        public string BusinessName { get; set; }

        public string RTN { get; set; }
    }
}