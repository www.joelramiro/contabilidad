﻿// <copyright file="Invoice.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Invoice
    {
        public int Id { get; set; }

        [ForeignKey(nameof(Usser))]
        public int? IdUsser { get; set; }

        [ForeignKey(nameof(Client))]
        public int? IdClient { get; set; }

        public DateTime CreationDate { get; set; }

        public string Code { get; set; }

        [ForeignKey(nameof(BranchOffice))]
        public int IdBranchOffice { get; set; }

        public InvoiceType InvoiceType { get; set; }

        public decimal Total { get; set; }

        [ForeignKey(nameof(CreditInvoice))]
        public int? IdCreditInvoice { get; set; }

        [ForeignKey(nameof(CashInvoice))]
        public int? IdCashInvoice { get; set; }

        public CreditInvoice CreditInvoice { get; set; }

        public Usser Usser { get; set; }

        public Client Client { get; set; }

        public BranchOffice BranchOffice { get; set; }

        public CashInvoice CashInvoice { get; set; }
    }
}
