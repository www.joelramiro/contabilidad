﻿// <copyright file="ProductModel.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

using System.Collections.Generic;

namespace Contabilidad.Database.Models.Accounting
{
    public class ProductModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}