﻿using Contabilidad.Database.Model.People;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Contabilidad.Database.Models.Accounting
{
    public class Provider
    {
        public int Id { get; set; }

        [ForeignKey(nameof(People))]
        public int IdPeople { get; set; }

        public People People { get; set; }

        public string Description { get; set; }

        public string BusinessName { get; set; }
    }
}
