﻿// <copyright file="Fee.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Fee
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public int Months { get; set; }

        public double ExtraPorcent { get; set; }

        public decimal Percent { get; set; }

        public decimal MontlyPayment { get; set; }

        public ICollection<CreditInvoice> CreditInvoices { get; set; }
    }
}