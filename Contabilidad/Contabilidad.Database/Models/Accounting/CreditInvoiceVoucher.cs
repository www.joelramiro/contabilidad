﻿// <copyright file="CreditInvoiceVoucher.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public class CreditInvoiceVoucher
    {
        public int Id { get; set; }

        [ForeignKey(nameof(CreditInvoice))]
        public int IdCreditInvoice { get; set; }

        public DateTime CreationDate { get; set; }

        public int Fee { get; set; }

        public string Code { get; set; }

        [ForeignKey(nameof(Usser))]
        public int? IdUsser { get; set; }

        public decimal Total { get; set; }

        public CreditInvoice CreditInvoice { get; set; }

        public Usser Usser { get; set; }
    }
}