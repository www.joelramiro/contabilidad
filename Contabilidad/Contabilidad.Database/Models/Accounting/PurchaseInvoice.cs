﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Contabilidad.Database.Models.Accounting
{
    public class PurchaseInvoice
    {
        public int Id { get; set; }

        public string Code { get; set; }

        [ForeignKey(nameof(Provider))]
        public int IdProvider { get; set; }

        [ForeignKey(nameof(Usser))]
        public int? IdUsser { get; set; }

        [ForeignKey(nameof(Location))]
        public int? IdLocation { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal Total { get; set; }

        public decimal Isv { get; set; }

        public decimal Disccount { get; set; }

        public decimal SubTotal { get; set; }

        [ForeignKey(nameof(Currency))]
        public int IdCurrency { get; set; }

        public Currency Currency { get; set; }

        [ForeignKey(nameof(FileInBytes))]
        public int? IdVoucher { get; set; }

        public FileInBytes FileInBytes { get; set; }

        public InvoiceType InvoiceType { get; set; }

        public bool IsPaid { get; set; }

        public bool IsAssignedToWarehouse { get; set; }

        public string Comments { get; set; }

        public ICollection<PurchaseInvoiceDetail> Details { get; set; }

        public Provider Provider { get; set; }

        public Usser Usser { get; set; }

        public Location Location { get; set; }

        public DateTime? LastEditDate { get; set; }
    }
}
