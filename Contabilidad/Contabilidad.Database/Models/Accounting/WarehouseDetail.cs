﻿// <copyright file="WarehouseDetail.cs" company="JRCC.Company">
// Copyright (c) JRCC.Company. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Models.Accounting
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class WarehouseDetail
    {
        public int Id { get; set; }

        [ForeignKey(nameof(Warehouse))]
        public int IdWarehouse { get; set; }

        [ForeignKey(nameof(Product))]
        public int IdProduct { get; set; }

        public int Quantity { get; set; }

        public Warehouse Warehouse { get; set; }

        public Product Product { get; set; }
    }
}