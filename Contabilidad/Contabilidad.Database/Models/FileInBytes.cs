﻿using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contabilidad.Database.Models
{
    public class FileInBytes
    {
        public int Id { get; set; }

        public string OriginalName { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public byte[] FileinBytes { get; set; }

        public double LengthInBytes { get; set; }

        public ICollection<PurchaseInvoice> PurchaseInvoices { get; set; }
    }
}
