﻿// <copyright file="PriorityLevel.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    public class PriorityLevel
    {
        public int Id { get; set; }

        public string Level { get; set; }
    }
}
