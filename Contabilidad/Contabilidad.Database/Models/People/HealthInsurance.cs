﻿// <copyright file="HealthInsurance.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.Collections.Generic;

    public class HealthInsurance
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Order { get; set; }

        public ICollection<HealthInsuranceAddress> HealthInsuranceAddresses { get; set; }

        public ICollection<HealthInsuranceEmail> HealthInsuranceEmails { get; set; }

        public ICollection<HealthInsuranceTelephone> HealthInsuranceTelephones { get; set; }
    }
}
