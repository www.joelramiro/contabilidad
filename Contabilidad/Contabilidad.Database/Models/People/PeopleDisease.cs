﻿// <copyright file="PeopleDisease.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class PeopleDisease
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int IdPeople { get; set; }

        [ForeignKey(nameof(IdPeople))]
        public People People { get; set; }

        public int IdDisease { get; set; }

        [ForeignKey(nameof(IdDisease))]
        public Disease Disease { get; set; }
    }
}
