﻿// <copyright file="PeopleTelephone.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class PeopleTelephone
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public int IdPeople { get; set; }

        [ForeignKey(nameof(IdPeople))]
        public People People { get; set; }

        public int IdTelephone { get; set; }

        [ForeignKey(nameof(IdTelephone))]
        public Telephone Telephone { get; set; }

        public int Order { get; set; }
    }
}
