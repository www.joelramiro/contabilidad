﻿// <copyright file="People.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class People
    {
        public int Id { get; set; }

        public string Identity { get; set; }

        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public int IdEducationLevel { get; set; }

        [ForeignKey(nameof(IdEducationLevel))]
        public virtual EducationLevel EducationLevel { get; set; }

        public int IdSexType { get; set; }

        [ForeignKey(nameof(IdSexType))]
        public virtual SexType SexType { get; set; }

        public int IdCivilStatus { get; set; }

        [ForeignKey(nameof(IdCivilStatus))]
        public virtual CivilStatus CivilStatus { get; set; }

        public ICollection<PeopleAddress> PeopleAddresses { get; set; }

        public ICollection<PeopleAllergy> PeopleAllergies { get; set; }

        public ICollection<PeopleDisability> PeopleDisabilities { get; set; }

        public ICollection<PeopleDisease> PeopleDiseases { get; set; }

        public ICollection<PeopleEmail> PeopleEmails { get; set; }

        public ICollection<PeopleNacionality> PeopleNacionalities { get; set; }

        public ICollection<PeopleTelephone> PeopleTelephones { get; set; }

        public ICollection<Relation> Relations { get; set; }

        public ICollection<Models.Accounting.Provider> Providers { get; set; }


        public string FullName()
        {
            return $"{this.FirstName} {this.SecondName} {this.MiddleName} {this.LastName}";
        }
    }
}
