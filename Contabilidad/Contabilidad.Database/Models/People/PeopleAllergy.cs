﻿// <copyright file="PeopleAllergy.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class PeopleAllergy
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int IdPeople { get; set; }

        [ForeignKey(nameof(IdPeople))]
        public People People { get; set; }

        public int IdAllergy { get; set; }

        [ForeignKey(nameof(IdAllergy))]
        public Allergy Allergy { get; set; }
    }
}
