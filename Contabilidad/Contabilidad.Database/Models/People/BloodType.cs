﻿// <copyright file="BloodType.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    public class BloodType
    {
        public int Id { get; set; }

        public string Type { get; set; }
    }
}
