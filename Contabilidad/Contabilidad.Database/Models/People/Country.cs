﻿// <copyright file="Country.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    public class Country
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
