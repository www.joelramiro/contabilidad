﻿// <copyright file="HealthInsuranceAddress.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class HealthInsuranceAddress
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int IdHealthInsurance { get; set; }

        [ForeignKey(nameof(IdHealthInsurance))]
        public HealthInsurance HealthInsurance { get; set; }

        public int IdAddress { get; set; }

        [ForeignKey(nameof(IdAddress))]
        public Address Address { get; set; }

        public int Order { get; set; }
    }
}
