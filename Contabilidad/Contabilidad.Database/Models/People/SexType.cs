﻿// <copyright file="SexType.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    public class SexType
    {
        public int Id { get; set; }

        public string Value { get; set; }
    }
}
