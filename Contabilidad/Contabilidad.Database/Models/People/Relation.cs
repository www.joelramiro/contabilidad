﻿// <copyright file="Relation.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class Relation
    {
        public int Id { get; set; }

        public int IdPeople1 { get; set; }

        public int IdRelationType { get; set; }

        [ForeignKey(nameof(IdPeople1))]
        public People People1 { get; set; }

        public string People2 { get; set; }

        [ForeignKey(nameof(IdRelationType))]
        public RelationType RelationType { get; set; }

        public string Description { get; set; }
    }
}
