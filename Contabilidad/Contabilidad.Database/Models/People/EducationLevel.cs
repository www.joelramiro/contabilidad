﻿// <copyright file="EducationLevel.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    public class EducationLevel
    {
        public int Id { get; set; }

        public string Level { get; set; }
    }
}
