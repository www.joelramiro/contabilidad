﻿// <copyright file="Address.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    public class Address
    {
        public int Id { get; set; }

        public string Value { get; set; }
    }
}
