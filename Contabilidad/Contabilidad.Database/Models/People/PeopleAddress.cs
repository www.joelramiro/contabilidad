﻿// <copyright file="PeopleAddress.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class PeopleAddress
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int IdPeople { get; set; }

        [ForeignKey(nameof(IdPeople))]
        public People People { get; set; }

        public int IdAddress { get; set; }

        [ForeignKey(nameof(IdAddress))]
        public Address Address { get; set; }

        public int Order { get; set; }
    }
}
