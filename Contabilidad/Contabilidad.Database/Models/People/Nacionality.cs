﻿// <copyright file="Nacionality.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class Nacionality
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int IdCountry { get; set; }

        [ForeignKey(nameof(IdCountry))]
        public Country Country { get; set; }
    }
}
