﻿// <copyright file="PeopleDisability.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Model.People
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class PeopleDisability
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int IdPeople { get; set; }

        [ForeignKey(nameof(IdPeople))]
        public People People { get; set; }

        public int IdDisability { get; set; }

        [ForeignKey(nameof(IdDisability))]
        public Disability Disability { get; set; }
    }
}
