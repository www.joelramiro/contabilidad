﻿using System.Collections.Generic;

namespace Contabilidad.Database.Models.ContabilidadSettings
{
    public class ValuePair
    {
        public int Id { get; set; }

        public ValueType ValueType { get; set; }

        public int IdKeyPar { get; set; }

        public KeyPair KeyPair { get; set; }

        public string StringValue { get; set; }
        
        public int IntValue { get; set; }
        
        public double DoubleValue { get; set; }
        
        public decimal DecimalValue { get; set; }

        public char CharValue { get; set; }

        public bool BoolValue { get; set; }
    }
}