﻿namespace Contabilidad.Database.Models.ContabilidadSettings
{
    public enum ValueType
    {
        INT = 0,
        STRING = 1,
        DECIMAL = 2,
        DOUBLE = 3,
        BOOL = 4,
        CHAR = 5,
        LIST = 6
    }
}