﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contabilidad.Database.Models.ContabilidadSettings
{
    public class KeyPair
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public int ValuePairId { get; set; }

        public ValuePair Value { get; set; }        
    }
}
