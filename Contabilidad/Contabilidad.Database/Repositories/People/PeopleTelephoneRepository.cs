﻿// <copyright file="PeopleTelephoneRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class PeopleTelephoneRepository : ContabilidadDbContextRepositoryBase<PeopleTelephone>
    {
        public PeopleTelephoneRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleTelephone> All()
        {
            return this.Context.PeopleTelephone;
        }

        protected override PeopleTelephone MapNewValuesToOld(PeopleTelephone oldEntity, PeopleTelephone newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
