﻿// <copyright file="PeopleDisabilityRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class PeopleDisabilityRepository : ContabilidadDbContextRepositoryBase<PeopleDisability>
    {
        public PeopleDisabilityRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleDisability> All()
        {
            return this.Context.PeopleDisability;
        }

        protected override PeopleDisability MapNewValuesToOld(PeopleDisability oldEntity, PeopleDisability newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
