﻿// <copyright file="RelationTypeRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class RelationTypeRepository : ContabilidadDbContextRepositoryBase<RelationType>
    {
        public RelationTypeRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<RelationType> All()
        {
            return this.Context.RelationType;
        }

        protected override RelationType MapNewValuesToOld(RelationType oldEntity, RelationType newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
