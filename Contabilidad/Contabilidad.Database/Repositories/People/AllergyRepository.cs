﻿// <copyright file="AllergyRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class AllergyRepository : ContabilidadDbContextRepositoryBase<Allergy>
    {
        public AllergyRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Allergy> All()
        {
            return this.Context.Allergy;
        }

        protected override Allergy MapNewValuesToOld(Allergy oldEntity, Allergy newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
