﻿// <copyright file="HealthInsuranceAddressRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class HealthInsuranceAddressRepository : ContabilidadDbContextRepositoryBase<HealthInsuranceAddress>
    {
        public HealthInsuranceAddressRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<HealthInsuranceAddress> All()
        {
            return this.Context.HealthInsuranceAddress;
        }

        protected override HealthInsuranceAddress MapNewValuesToOld(HealthInsuranceAddress oldEntity, HealthInsuranceAddress newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
