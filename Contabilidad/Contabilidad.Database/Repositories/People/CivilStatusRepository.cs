﻿// <copyright file="CivilStatusRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class CivilStatusRepository : ContabilidadDbContextRepositoryBase<CivilStatus>
    {
        public CivilStatusRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<CivilStatus> All()
        {
            return this.Context.CivilStatus;
        }

        protected override CivilStatus MapNewValuesToOld(CivilStatus oldEntity, CivilStatus newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
