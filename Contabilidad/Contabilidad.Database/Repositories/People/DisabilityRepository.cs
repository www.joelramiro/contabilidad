﻿// <copyright file="DisabilityRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class DisabilityRepository : ContabilidadDbContextRepositoryBase<Disability>
    {
        public DisabilityRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Disability> All()
        {
            return this.Context.Disability;
        }

        protected override Disability MapNewValuesToOld(Disability oldEntity, Disability newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
