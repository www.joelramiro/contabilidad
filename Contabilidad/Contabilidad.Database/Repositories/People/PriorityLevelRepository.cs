﻿// <copyright file="PriorityLevelRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class PriorityLevelRepository : ContabilidadDbContextRepositoryBase<PriorityLevel>
    {
        public PriorityLevelRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PriorityLevel> All()
        {
            return this.Context.PriorityLevel;
        }

        protected override PriorityLevel MapNewValuesToOld(PriorityLevel oldEntity, PriorityLevel newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
