﻿// <copyright file="PeopleAllergyRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class PeopleAllergyRepository : ContabilidadDbContextRepositoryBase<PeopleAllergy>
    {
        public PeopleAllergyRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleAllergy> All()
        {
            return this.Context.PeopleAllergy;
        }

        protected override PeopleAllergy MapNewValuesToOld(PeopleAllergy oldEntity, PeopleAllergy newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
