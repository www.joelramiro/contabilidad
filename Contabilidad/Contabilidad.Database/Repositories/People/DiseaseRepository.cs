﻿// <copyright file="DiseaseRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;   

    public class DiseaseRepository : ContabilidadDbContextRepositoryBase<Disease>
    {
        public DiseaseRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Disease> All()
        {
            return this.Context.Disease;
        }

        protected override Disease MapNewValuesToOld(Disease oldEntity, Disease newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
