﻿// <copyright file="AddressRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repositories.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class AddressRepository : ContabilidadDbContextRepositoryBase<Address>
    {
        public AddressRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Address> All()
        {
            return this.Context.Address;
        }

        protected override Address MapNewValuesToOld(Address oldEntity, Address newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
