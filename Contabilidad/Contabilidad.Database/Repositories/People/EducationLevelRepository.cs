﻿// <copyright file="EducationLevelRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class EducationLevelRepository : ContabilidadDbContextRepositoryBase<EducationLevel>
    {
        public EducationLevelRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<EducationLevel> All()
        {
            return this.Context.EducationLevel;
        }

        protected override EducationLevel MapNewValuesToOld(EducationLevel oldEntity, EducationLevel newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
