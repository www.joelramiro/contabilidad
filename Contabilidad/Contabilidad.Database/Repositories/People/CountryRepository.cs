﻿// <copyright file="CountryRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class CountryRepository : ContabilidadDbContextRepositoryBase<Country>
    {
        public CountryRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Country> All()
        {
            return this.Context.Country;
        }

        protected override Country MapNewValuesToOld(Country oldEntity, Country newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
