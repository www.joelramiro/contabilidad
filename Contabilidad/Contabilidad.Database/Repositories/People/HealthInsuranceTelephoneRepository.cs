﻿// <copyright file="HealthInsuranceTelephoneRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class HealthInsuranceTelephoneRepository : ContabilidadDbContextRepositoryBase<HealthInsuranceTelephone>
    {
        public HealthInsuranceTelephoneRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<HealthInsuranceTelephone> All()
        {
            return this.Context.HealthInsuranceTelephone;
        }

        protected override HealthInsuranceTelephone MapNewValuesToOld(HealthInsuranceTelephone oldEntity, HealthInsuranceTelephone newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
