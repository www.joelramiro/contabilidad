﻿// <copyright file="EmailRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class EmailRepository : ContabilidadDbContextRepositoryBase<Email>
    {
        public EmailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Email> All()
        {
            return this.Context.Email;
        }

        protected override Email MapNewValuesToOld(Email oldEntity, Email newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
