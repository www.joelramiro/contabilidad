﻿// <copyright file="SexTypeRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class SexTypeRepository : ContabilidadDbContextRepositoryBase<SexType>
    {
        public SexTypeRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<SexType> All()
        {
            return this.Context.SexType;
        }

        protected override SexType MapNewValuesToOld(SexType oldEntity, SexType newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
