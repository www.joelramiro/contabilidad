﻿// <copyright file="PeopleDiseaseRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class PeopleDiseaseRepository : ContabilidadDbContextRepositoryBase<PeopleDisease>
    {
        public PeopleDiseaseRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleDisease> All()
        {
            return this.Context.PeopleDisease;
        }

        protected override PeopleDisease MapNewValuesToOld(PeopleDisease oldEntity, PeopleDisease newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
