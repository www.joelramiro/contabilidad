﻿// <copyright file="PeopleRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Repositories;

    public class PeopleRepository : ContabilidadDbContextRepositoryBase<Model.People.People>
    {
        public PeopleRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Model.People.People> All()
        {
            return this.Context.People;
        }

        protected override Model.People.People MapNewValuesToOld(Model.People.People oldEntity, Model.People.People newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
