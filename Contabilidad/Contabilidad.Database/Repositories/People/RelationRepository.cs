﻿// <copyright file="RelationRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class RelationRepository : ContabilidadDbContextRepositoryBase<Relation>
    {
        public RelationRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Relation> All()
        {
            return this.Context.Relation;
        }

        protected override Relation MapNewValuesToOld(Relation oldEntity, Relation newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
