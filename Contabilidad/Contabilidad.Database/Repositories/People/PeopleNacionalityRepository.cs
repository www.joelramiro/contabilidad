﻿// <copyright file="PeopleNacionalityRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class PeopleNacionalityRepository : ContabilidadDbContextRepositoryBase<PeopleNacionality>
    {
        public PeopleNacionalityRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleNacionality> All()
        {
            return this.Context.PeopleNacionality;
        }

        protected override PeopleNacionality MapNewValuesToOld(PeopleNacionality oldEntity, PeopleNacionality newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
