﻿// <copyright file="PeopleAddressRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class PeopleAddressRepository : ContabilidadDbContextRepositoryBase<PeopleAddress>
    {
        public PeopleAddressRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleAddress> All()
        {
            return this.Context.PeopleAddress;
        }

        protected override PeopleAddress MapNewValuesToOld(PeopleAddress oldEntity, PeopleAddress newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
