﻿// <copyright file="BloodTypeRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class BloodTypeRepository : ContabilidadDbContextRepositoryBase<BloodType>
    {
        public BloodTypeRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<BloodType> All()
        {
            return this.Context.BloodType;
        }

        protected override BloodType MapNewValuesToOld(BloodType oldEntity, BloodType newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
