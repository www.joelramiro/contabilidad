﻿// <copyright file="PeopleEmailRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class PeopleEmailRepository : ContabilidadDbContextRepositoryBase<PeopleEmail>
    {
        public PeopleEmailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleEmail> All()
        {
            return this.Context.PeopleEmail;
        }

        protected override PeopleEmail MapNewValuesToOld(PeopleEmail oldEntity, PeopleEmail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
