﻿// <copyright file="NacionalityRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class NacionalityRepository : ContabilidadDbContextRepositoryBase<Nacionality>
    {
        public NacionalityRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Nacionality> All()
        {
            return this.Context.Nacionality;
        }

        protected override Nacionality MapNewValuesToOld(Nacionality oldEntity, Nacionality newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
