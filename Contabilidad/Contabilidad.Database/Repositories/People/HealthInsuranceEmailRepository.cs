﻿// <copyright file="HealthInsuranceEmailRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;
    public class HealthInsuranceEmailRepository : ContabilidadDbContextRepositoryBase<HealthInsuranceEmail>
    {
        public HealthInsuranceEmailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<HealthInsuranceEmail> All()
        {
            return this.Context.HealthInsuranceEmail;
        }

        protected override HealthInsuranceEmail MapNewValuesToOld(HealthInsuranceEmail oldEntity, HealthInsuranceEmail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
