﻿// <copyright file="TelephoneRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace Contabilidad.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Repositories;

    public class TelephoneRepository : ContabilidadDbContextRepositoryBase<Telephone>
    {
        public TelephoneRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Telephone> All()
        {
            return this.Context.Telephone;
        }

        protected override Telephone MapNewValuesToOld(Telephone oldEntity, Telephone newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
