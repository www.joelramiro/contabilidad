﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class ProductRangeRepository : ContabilidadDbContextRepositoryBase<ProductRange>
    {
        public ProductRangeRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<ProductRange> All()
        {
            return this.Context.ProductRange;
        }

        protected override ProductRange MapNewValuesToOld(ProductRange oldEntity, ProductRange newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
