﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class InvoiceRepository : ContabilidadDbContextRepositoryBase<Invoice>
    {
        public InvoiceRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Invoice> All()
        {
            return this.Context.Invoice;
        }

        protected override Invoice MapNewValuesToOld(Invoice oldEntity, Invoice newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
