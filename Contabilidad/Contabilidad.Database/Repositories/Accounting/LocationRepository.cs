﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class LocationRepository : ContabilidadDbContextRepositoryBase<Location>
    {
        public LocationRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Location> All()
        {
            return this.Context.Location;
        }

        protected override Location MapNewValuesToOld(Location oldEntity, Location newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
