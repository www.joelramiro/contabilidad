﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class CreditInvoiceVoucherRepository : ContabilidadDbContextRepositoryBase<CreditInvoiceVoucher>
    {
        public CreditInvoiceVoucherRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<CreditInvoiceVoucher> All()
        {
            return this.Context.CreditInvoiceVoucher;
        }

        protected override CreditInvoiceVoucher MapNewValuesToOld(CreditInvoiceVoucher oldEntity, CreditInvoiceVoucher newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
