﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class WarehouseMovementRepository : ContabilidadDbContextRepositoryBase<WarehouseMovement>
    {
        public WarehouseMovementRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<WarehouseMovement> All()
        {
            return this.Context.WarehouseMovement;
        }

        protected override WarehouseMovement MapNewValuesToOld(WarehouseMovement oldEntity, WarehouseMovement newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
