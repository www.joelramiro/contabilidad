﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class ProductCategoryRepository : ContabilidadDbContextRepositoryBase<ProductCategory>
    {
        public ProductCategoryRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<ProductCategory> All()
        {
            return this.Context.ProductCategory;
        }

        protected override ProductCategory MapNewValuesToOld(ProductCategory oldEntity, ProductCategory newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
