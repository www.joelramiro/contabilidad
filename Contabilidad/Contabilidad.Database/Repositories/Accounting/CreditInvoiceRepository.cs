﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class CreditInvoiceRepository : ContabilidadDbContextRepositoryBase<CreditInvoice>
    {
        public CreditInvoiceRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<CreditInvoice> All()
        {
            return this.Context.CreditInvoice;
        }

        protected override CreditInvoice MapNewValuesToOld(CreditInvoice oldEntity, CreditInvoice newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
