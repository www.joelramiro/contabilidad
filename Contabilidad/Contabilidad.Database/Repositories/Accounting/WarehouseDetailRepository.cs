﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class WarehouseDetailRepository : ContabilidadDbContextRepositoryBase<WarehouseDetail>
    {
        public WarehouseDetailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<WarehouseDetail> All()
        {
            return this.Context.WarehouseDetail;
        }

        protected override WarehouseDetail MapNewValuesToOld(WarehouseDetail oldEntity, WarehouseDetail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
