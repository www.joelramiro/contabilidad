﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class WarehouseMovementDetailRepository : ContabilidadDbContextRepositoryBase<WarehouseMovementDetail>
    {
        public WarehouseMovementDetailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<WarehouseMovementDetail> All()
        {
            return this.Context.WarehouseMovementDetail;
        }

        protected override WarehouseMovementDetail MapNewValuesToOld(WarehouseMovementDetail oldEntity, WarehouseMovementDetail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
