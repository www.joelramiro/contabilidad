﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class ClientRepository : ContabilidadDbContextRepositoryBase<Client>
    {
        public ClientRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Client> All()
        {
            return this.Context.Client;
        }

        protected override Client MapNewValuesToOld(Client oldEntity, Client newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
