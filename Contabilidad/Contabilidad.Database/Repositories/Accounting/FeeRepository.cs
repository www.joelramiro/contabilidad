﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class FeeRepository : ContabilidadDbContextRepositoryBase<Fee>
    {
        public FeeRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Fee> All()
        {
            return this.Context.Fee;
        }

        protected override Fee MapNewValuesToOld(Fee oldEntity, Fee newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
