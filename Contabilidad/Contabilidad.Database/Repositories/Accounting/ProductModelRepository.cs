﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class ProductModelRepository : ContabilidadDbContextRepositoryBase<ProductModel>
    {
        public ProductModelRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<ProductModel> All()
        {
            return this.Context.ProductModel;
        }

        protected override ProductModel MapNewValuesToOld(ProductModel oldEntity, ProductModel newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
