﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class ProductPriceRepository : ContabilidadDbContextRepositoryBase<ProductPrice>
    {
        public ProductPriceRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<ProductPrice> All()
        {
            return this.Context.ProductPrice;
        }

        protected override ProductPrice MapNewValuesToOld(ProductPrice oldEntity, ProductPrice newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
