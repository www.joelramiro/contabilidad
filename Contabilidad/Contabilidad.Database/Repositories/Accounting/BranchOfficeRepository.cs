﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class BranchOfficeRepository : ContabilidadDbContextRepositoryBase<BranchOffice>
    {
        public BranchOfficeRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<BranchOffice> All()
        {
            return this.Context.BranchOffice;
        }

        protected override BranchOffice MapNewValuesToOld(BranchOffice oldEntity, BranchOffice newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
