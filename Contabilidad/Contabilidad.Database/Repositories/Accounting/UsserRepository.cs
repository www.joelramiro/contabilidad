﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class UsserRepository : ContabilidadDbContextRepositoryBase<Usser>
    {
        public UsserRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Usser> All()
        {
            return this.Context.Usser;
        }

        protected override Usser MapNewValuesToOld(Usser oldEntity, Usser newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
