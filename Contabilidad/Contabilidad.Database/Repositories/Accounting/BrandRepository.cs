﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class BrandRepository : ContabilidadDbContextRepositoryBase<Brand>
    {
        public BrandRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Brand> All()
        {
            return this.Context.Brand;
        }

        protected override Brand MapNewValuesToOld(Brand oldEntity, Brand newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
