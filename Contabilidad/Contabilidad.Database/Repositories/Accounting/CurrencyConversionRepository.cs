﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class CurrencyConversionRepository : ContabilidadDbContextRepositoryBase<CurrencyConversion>
    {
        public CurrencyConversionRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<CurrencyConversion> All()
        {
            return this.Context.CurrencyConversion;
        }

        protected override CurrencyConversion MapNewValuesToOld(CurrencyConversion oldEntity, CurrencyConversion newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
