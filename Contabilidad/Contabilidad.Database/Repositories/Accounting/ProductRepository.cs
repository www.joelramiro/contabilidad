﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class ProductRepository : ContabilidadDbContextRepositoryBase<Product>
    {
        public ProductRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Product> All()
        {
            return this.Context.Product;
        }

        protected override Product MapNewValuesToOld(Product oldEntity, Product newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
