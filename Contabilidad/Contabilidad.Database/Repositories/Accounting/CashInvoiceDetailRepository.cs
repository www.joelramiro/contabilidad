﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class CashInvoiceDetailRepository : ContabilidadDbContextRepositoryBase<CashInvoiceDetail>
    {
        public CashInvoiceDetailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<CashInvoiceDetail> All()
        {
            return this.Context.CashInvoiceDetail;
        }

        protected override CashInvoiceDetail MapNewValuesToOld(CashInvoiceDetail oldEntity, CashInvoiceDetail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
