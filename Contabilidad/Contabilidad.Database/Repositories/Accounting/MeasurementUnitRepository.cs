﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class MeasurementUnitRepository : ContabilidadDbContextRepositoryBase<MeasurementUnit>
    {
        public MeasurementUnitRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<MeasurementUnit> All()
        {
            return this.Context.MeasurementUnit;
        }

        protected override MeasurementUnit MapNewValuesToOld(MeasurementUnit oldEntity, MeasurementUnit newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
