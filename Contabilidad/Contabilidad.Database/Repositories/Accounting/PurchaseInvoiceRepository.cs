﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class PurchaseInvoiceRepository : ContabilidadDbContextRepositoryBase<PurchaseInvoice>
    {
        public PurchaseInvoiceRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PurchaseInvoice> All()
        {
            return this.Context.PurchaseInvoice;
        }

        protected override PurchaseInvoice MapNewValuesToOld(PurchaseInvoice oldEntity, PurchaseInvoice newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
