﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class CashInvoiceRepository : ContabilidadDbContextRepositoryBase<CashInvoice>
    {
        public CashInvoiceRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<CashInvoice> All()
        {
            return this.Context.CashInvoice;
        }

        protected override CashInvoice MapNewValuesToOld(CashInvoice oldEntity, CashInvoice newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
