﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class PurchaseInvoiceDetailRepository : ContabilidadDbContextRepositoryBase<PurchaseInvoiceDetail>
    {
        public PurchaseInvoiceDetailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<PurchaseInvoiceDetail> All()
        {
            return this.Context.PurchaseInvoiceDetail;
        }

        protected override PurchaseInvoiceDetail MapNewValuesToOld(PurchaseInvoiceDetail oldEntity, PurchaseInvoiceDetail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
