﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class ProviderRepository : ContabilidadDbContextRepositoryBase<Provider>
    {
        public ProviderRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Provider> All()
        {
            return this.Context.Provider;
        }

        protected override Provider MapNewValuesToOld(Provider oldEntity, Provider newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
