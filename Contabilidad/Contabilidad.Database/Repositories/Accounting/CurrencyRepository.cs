﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class CurrencyRepository : ContabilidadDbContextRepositoryBase<Currency>
    {
        public CurrencyRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Currency> All()
        {
            return this.Context.Currency;
        }

        protected override Currency MapNewValuesToOld(Currency oldEntity, Currency newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
