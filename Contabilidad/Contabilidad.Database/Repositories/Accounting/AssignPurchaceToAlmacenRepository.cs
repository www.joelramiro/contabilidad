﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class AssignPurchaseToWarehouseRepository : ContabilidadDbContextRepositoryBase<AssignPurchaseToWarehouse>
    {
        public AssignPurchaseToWarehouseRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<AssignPurchaseToWarehouse> All()
        {
            return this.Context.AssignPurchaseToWarehouse;
        }

        protected override AssignPurchaseToWarehouse MapNewValuesToOld(AssignPurchaseToWarehouse oldEntity, AssignPurchaseToWarehouse newEntity)
        {
            throw new NotImplementedException();
        }
    }
}