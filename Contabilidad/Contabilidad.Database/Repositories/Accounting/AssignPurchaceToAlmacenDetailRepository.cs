﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class AssignPurchaseToWarehouseDetailRepository : ContabilidadDbContextRepositoryBase<AssignPurchaseToWarehouseDetail>
    {
        public AssignPurchaseToWarehouseDetailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<AssignPurchaseToWarehouseDetail> All()
        {
            return this.Context.AssignPurchaseToWarehouseDetail;
        }

        protected override AssignPurchaseToWarehouseDetail MapNewValuesToOld(AssignPurchaseToWarehouseDetail oldEntity, AssignPurchaseToWarehouseDetail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}