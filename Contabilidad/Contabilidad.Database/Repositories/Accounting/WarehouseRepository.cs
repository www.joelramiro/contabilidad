﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class WarehouseRepository : ContabilidadDbContextRepositoryBase<Warehouse>
    {
        public WarehouseRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<Warehouse> All()
        {
            return this.Context.Warehouse;
        }

        protected override Warehouse MapNewValuesToOld(Warehouse oldEntity, Warehouse newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
