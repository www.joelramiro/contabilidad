﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class CreditInvoiceDetailRepository : ContabilidadDbContextRepositoryBase<CreditInvoiceDetail>
    {
        public CreditInvoiceDetailRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<CreditInvoiceDetail> All()
        {
            return this.Context.CreditInvoiceDetail;
        }

        protected override CreditInvoiceDetail MapNewValuesToOld(CreditInvoiceDetail oldEntity, CreditInvoiceDetail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
