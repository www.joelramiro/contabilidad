﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models;
using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.Accounting
{
    public class FileInBytesRepository : ContabilidadDbContextRepositoryBase<FileInBytes>
    {
        public FileInBytesRepository(ContabilidadContext context)
            : base(context)
        {
        }

        public override IQueryable<FileInBytes> All()
        {
            return this.Context.FileInBytes;
        }

        protected override FileInBytes MapNewValuesToOld(FileInBytes oldEntity, FileInBytes newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
