﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Models.ContabilidadSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.ContabilidadSettings
{
    public class KeyPairRepository : ContabilidadSettingsContextRepositoryBase<KeyPair>
    {
        public KeyPairRepository(ContabilidadSettingsContext context)
            : base(context)
        {
        }

        public override IQueryable<KeyPair> All()
        {
            return this.Context.KeyPair;
        }

        protected override KeyPair MapNewValuesToOld(KeyPair oldEntity, KeyPair newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
