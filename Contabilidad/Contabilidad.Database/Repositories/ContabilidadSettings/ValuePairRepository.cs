﻿using Contabilidad.Database.Context;
using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Models.ContabilidadSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Repositories.ContabilidadSettings
{
    public class ValuePairRepository : ContabilidadSettingsContextRepositoryBase<ValuePair>
    {
        public ValuePairRepository(ContabilidadSettingsContext context)
            : base(context)
        {
        }

        public override IQueryable<ValuePair> All()
        {
            return this.Context.ValuePair;
        }

        protected override ValuePair MapNewValuesToOld(ValuePair oldEntity, ValuePair newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
