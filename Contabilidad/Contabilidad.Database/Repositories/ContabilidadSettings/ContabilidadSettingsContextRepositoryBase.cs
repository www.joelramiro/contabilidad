﻿using Contabilidad.Database.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contabilidad.Database.Repositories.ContabilidadSettings
{
    public abstract class ContabilidadSettingsContextRepositoryBase<TEntity> : RepositoryBase<TEntity, ContabilidadSettingsContext>
        where TEntity : class
    {
        public ContabilidadSettingsContextRepositoryBase(ContabilidadSettingsContext context)
            : base(context)
        {
            this.Context = context;
        }
    }
}
