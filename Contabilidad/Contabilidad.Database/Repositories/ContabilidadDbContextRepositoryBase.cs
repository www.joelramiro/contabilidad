﻿using Contabilidad.Database.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contabilidad.Database.Repositories
{
    public abstract class ContabilidadDbContextRepositoryBase<TEntity> : RepositoryBase<TEntity, ContabilidadContext>
        where TEntity : class
    {
        public ContabilidadDbContextRepositoryBase(ContabilidadContext context)
            : base(context)
        {
            this.Context = context;
        }
    }
}
