﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contabilidad.Database.Context
{
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Models.Accounting;
    using System.Linq;
    using Contabilidad.Database.Models;

    public class ContabilidadContext : DbContext
    {

        public ContabilidadContext(
            DbContextOptions<ContabilidadContext> options)
            : base(options)
        {
        }

        public DbSet<CurrencyConversion> CurrencyConversion { get; set; }

        public DbSet<WarehouseMovement> WarehouseMovement { get; set; }

        public DbSet<WarehouseMovementDetail> WarehouseMovementDetail { get; set; }

        public DbSet<Currency> Currency { get; set; }

        public DbSet<AssignPurchaseToWarehouse> AssignPurchaseToWarehouse { get; set; }

        public DbSet<AssignPurchaseToWarehouseDetail> AssignPurchaseToWarehouseDetail { get; set; }

        public DbSet<FileInBytes> FileInBytes { get; set; }

        public DbSet<Address> Address { get; set; }

        public DbSet<Allergy> Allergy { get; set; }

        public DbSet<BloodType> BloodType { get; set; }

        public DbSet<CivilStatus> CivilStatus { get; set; }

        public DbSet<Country> Country { get; set; }

        public DbSet<Disability> Disability { get; set; }

        public DbSet<Disease> Disease { get; set; }

        public DbSet<EducationLevel> EducationLevel { get; set; }

        public DbSet<Email> Email { get; set; }

        public DbSet<HealthInsurance> HealthInsurance { get; set; }

        public DbSet<HealthInsuranceAddress> HealthInsuranceAddress { get; set; }

        public DbSet<HealthInsuranceEmail> HealthInsuranceEmail { get; set; }

        public DbSet<HealthInsuranceTelephone> HealthInsuranceTelephone { get; set; }

        public DbSet<Nacionality> Nacionality { get; set; }

        public DbSet<People> People { get; set; }

        public DbSet<PeopleAddress> PeopleAddress { get; set; }

        public DbSet<PeopleAllergy> PeopleAllergy { get; set; }

        public DbSet<PeopleDisability> PeopleDisability { get; set; }

        public DbSet<PeopleDisease> PeopleDisease { get; set; }

        public DbSet<PeopleEmail> PeopleEmail { get; set; }

        public DbSet<PeopleNacionality> PeopleNacionality { get; set; }

        public DbSet<PeopleTelephone> PeopleTelephone { get; set; }

        public DbSet<PriorityLevel> PriorityLevel { get; set; }

        public DbSet<Relation> Relation { get; set; }

        public DbSet<RelationType> RelationType { get; set; }

        public DbSet<SexType> SexType { get; set; }

        public DbSet<Telephone> Telephone { get; set; }

        public DbSet<Brand> Brand { get; set; }

        public DbSet<BranchOffice> BranchOffice { get; set; }

        public DbSet<CashInvoice> CashInvoice { get; set; }

        public DbSet<CashInvoiceDetail> CashInvoiceDetail { get; set; }

        public DbSet<Client> Client { get; set; }

        public DbSet<CreditInvoice> CreditInvoice { get; set; }

        public DbSet<CreditInvoiceDetail> CreditInvoiceDetail { get; set; }

        public DbSet<CreditInvoiceVoucher> CreditInvoiceVoucher { get; set; }

        public DbSet<Fee> Fee { get; set; }

        public DbSet<Invoice> Invoice { get; set; }

        public DbSet<Location> Location { get; set; }

        public DbSet<MeasurementUnit> MeasurementUnit { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<ProductCategory> ProductCategory { get; set; }

        public DbSet<ProductModel> ProductModel { get; set; }

        public DbSet<ProductRange> ProductRange { get; set; }

        public DbSet<Provider> Provider { get; set; }

        public DbSet<PurchaseInvoice> PurchaseInvoice { get; set; }

        public DbSet<PurchaseInvoiceDetail> PurchaseInvoiceDetail { get; set; }

        public DbSet<Usser> Usser { get; set; }

        public DbSet<Warehouse> Warehouse { get; set; }

        public DbSet<WarehouseDetail> WarehouseDetail { get; set; }

        public DbSet<ProductPrice> ProductPrice { get; set; }

        public override int SaveChanges()
        {
            return this.SaveChanges(true);
        }

        public Task<int> SaveChangesAsync()
        {
            return this.SaveChangesAsync(cancellationToken: default(CancellationToken));
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return this.SaveChangesAsync(acceptAllChangesOnSuccess: true, cancellationToken: cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Invoice>()
                .HasOne(rd => rd.Usser)
                .WithMany(sc => sc.Invoices)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<CreditInvoiceVoucher>()
                .HasOne(rd => rd.Usser)
                .WithMany(sc => sc.CreditInvoiceVouchers)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<CreditInvoiceVoucher>()
                .HasOne(rd => rd.CreditInvoice)
                .WithMany(sc => sc.Vouchers)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<CreditInvoice>()
                .HasOne(rd => rd.Fee)
                .WithMany(sc => sc.CreditInvoices)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Product>()
                .HasOne(rd => rd.Brand)
                .WithMany(sc => sc.Products)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Product>()
                .HasOne(rd => rd.Model)
                .WithMany(sc => sc.Products)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Product>()
                .HasOne(rd => rd.ProductCategory)
                .WithMany(sc => sc.Products)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Product>()
                .HasOne(rd => rd.MeasurementUnit)
                .WithMany(sc => sc.Products)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Provider>()
                .HasOne(rd => rd.People)
                .WithMany(sc => sc.Providers)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<PurchaseInvoice>()
                .HasOne(rd => rd.FileInBytes)
                .WithMany(sc => sc.PurchaseInvoices)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<PurchaseInvoice>()
                .HasOne(rd => rd.Currency)
                .WithMany(sc => sc.PurchaseInvoices)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Invoice>().Property(p => p.Total)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<ProductPrice>().Property(p => p.SalePrice)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<CashInvoiceDetail>().Property(p => p.Total)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<CashInvoiceDetail>().Property(p => p.UnitPrice)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<CreditInvoice>().Property(p => p.Prima)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<CreditInvoiceDetail>().Property(p => p.Percent)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<CreditInvoiceDetail>().Property(p => p.Total)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<CreditInvoiceDetail>().Property(p => p.UnitPrice)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<CreditInvoiceVoucher>().Property(p => p.Total)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<Fee>().Property(p => p.MontlyPayment)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<Fee>().Property(p => p.Percent)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<Invoice>().Property(p => p.Total)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<PurchaseInvoice>().Property(p => p.Total)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<PurchaseInvoiceDetail>().Property(p => p.Total)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<PurchaseInvoiceDetail>().Property(p => p.UnitPrice)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<CurrencyConversion>().Property(p => p.ValueConversion)
                            .HasColumnType("decimal(18,4)")
                            .IsRequired(true);

            builder.Entity<Warehouse>().HasIndex(p => new { p.Code });

            builder.Entity<CurrencyConversion>()
                .HasOne(m => m.CurrencyOrigin)
                .WithMany(d => d.CurrencyOriginConversions)
                .HasForeignKey(d => d.IdCurrencyOrigin)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<CurrencyConversion>()
                .HasOne(m => m.CurrencyDestination)
                .WithMany(d => d.CurrencyDestinationConversions)
                .HasForeignKey(d => d.IdCurrencyDestination)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
