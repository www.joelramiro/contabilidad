﻿using Contabilidad.Database.Models.ContabilidadSettings;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Contabilidad.Database.Context
{
    public class ContabilidadSettingsContext : DbContext
    {
        public ContabilidadSettingsContext(
            DbContextOptions<ContabilidadSettingsContext> options)
            : base(options)
        {
        }

        public DbSet<KeyPair> KeyPair { get; set; }
        public DbSet<ValuePair> ValuePair { get; set; }


        public override int SaveChanges()
        {
            return this.SaveChanges(true);
        }

        public Task<int> SaveChangesAsync()
        {
            return this.SaveChangesAsync(cancellationToken: default(CancellationToken));
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return this.SaveChangesAsync(acceptAllChangesOnSuccess: true, cancellationToken: cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
        }
    }
}