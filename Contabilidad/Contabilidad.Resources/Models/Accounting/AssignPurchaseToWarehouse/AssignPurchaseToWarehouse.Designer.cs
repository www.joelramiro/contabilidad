﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Contabilidad.Resources.Models.Accounting.AssignPurchaseToWarehouse {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AssignPurchaseToWarehouse {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AssignPurchaseToWarehouse() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Contabilidad.Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchase" +
                            "ToWarehouse", typeof(AssignPurchaseToWarehouse).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Asignar.
        /// </summary>
        public static string Assign {
            get {
                return ResourceManager.GetString("Assign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Código factura de compra.
        /// </summary>
        public static string Code {
            get {
                return ResourceManager.GetString("Code", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Comentarios.
        /// </summary>
        public static string Comments {
            get {
                return ResourceManager.GetString("Comments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Fecha de creación.
        /// </summary>
        public static string CreatedDate {
            get {
                return ResourceManager.GetString("CreatedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a ¿Desea asignar la factura?.
        /// </summary>
        public static string CreateMessage {
            get {
                return ResourceManager.GetString("CreateMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Currency.
        /// </summary>
        public static string Currency {
            get {
                return ResourceManager.GetString("Currency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Detalles.
        /// </summary>
        public static string Details {
            get {
                return ResourceManager.GetString("Details", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Detalles de la asignación.
        /// </summary>
        public static string DetailsTitle {
            get {
                return ResourceManager.GetString("DetailsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a ¿Desea editar la asignación?.
        /// </summary>
        public static string EditMessage {
            get {
                return ResourceManager.GetString("EditMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Id.
        /// </summary>
        public static string Id {
            get {
                return ResourceManager.GetString("Id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Id factura de compra.
        /// </summary>
        public static string IdInvoice {
            get {
                return ResourceManager.GetString("IdInvoice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Código factura.
        /// </summary>
        public static string InvoiceCode {
            get {
                return ResourceManager.GetString("InvoiceCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Pagado?.
        /// </summary>
        public static string IsPaid {
            get {
                return ResourceManager.GetString("IsPaid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a ¿Está seguro de querer eliminar la asignación?.
        /// </summary>
        public static string MessageDelete {
            get {
                return ResourceManager.GetString("MessageDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a asignación.
        /// </summary>
        public static string modelName {
            get {
                return ResourceManager.GetString("modelName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Nombre.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Provider.
        /// </summary>
        public static string Provider {
            get {
                return ResourceManager.GetString("Provider", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a SubTotal.
        /// </summary>
        public static string SubTotal {
            get {
                return ResourceManager.GetString("SubTotal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Eliminar asignación.
        /// </summary>
        public static string TitleDelete {
            get {
                return ResourceManager.GetString("TitleDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Asignaciones.
        /// </summary>
        public static string TitlePage {
            get {
                return ResourceManager.GetString("TitlePage", resourceCulture);
            }
        }
    }
}
