﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.ProductModel;
using Contabilidad.Site.Models.ProductModel;
using Microsoft.AspNetCore.Mvc;

namespace Contabilidad.Site.Controllers
{
    public class ProductModelController : Controller
    {
        private readonly IProductModelManager productModelManager;

        public ProductModelController(IProductModelManager productModelManager)
        {
            this.productModelManager = productModelManager;
        }

        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var productModels = await this.productModelManager.GetAllAsync();
            var model = productModels.OrderBy(b => b.Name).Select(b => new ProductModelListViewModel
            {
                Id = b.Id,
                Name = b.Name
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            return this.View();
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var productModel = await this.productModelManager.GetByIdAsync(id.Value);

            if (productModel == null)
            {
                return this.BadRequest();
            }

            var result = await this.productModelManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var productModel = await this.productModelManager.GetByIdAsync(id.Value);

            if (productModel == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProductModelEditViewModel
            {
                Id = productModel.Id,
                Name = productModel.Name,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProductModelEditViewModel productModelEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(productModelEditViewModel);
            }

            var productModel = await this.productModelManager.GetByIdAsync(productModelEditViewModel.Id);

            if (productModel == null)
            {
                return this.BadRequest();
            }

            productModel.Name = productModelEditViewModel.Name;

            var result = await this.productModelManager.EditAsync(productModel);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(productModelEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idProductModel)
        {
            if (idProductModel == null)
            {
                return this.BadRequest();
            }

            var productModel = await this.productModelManager.GetByIdAsync(idProductModel.Value);

            if (productModel == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProductModelDetailsViewModel
            {
                Id = productModel.Id,
                Name = productModel.Name,
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductModelCreateViewModel productModelCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(productModelCreateViewModel);
            }

            var result = await this.productModelManager.CreateAsync(new Database.Models.Accounting.ProductModel
            {
                Name = productModelCreateViewModel.Name,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(productModelCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
    }
}
