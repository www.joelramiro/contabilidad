﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.ProductPrice;
using Contabilidad.Database.Models;
using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.ProductPrice;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Contabilidad.Site.Controllers
{
    public class ProductPriceController : Controller
    {
        private readonly IProductPriceManager productPriceManager;
        private readonly IRepository<Product> productRepository;
        private readonly IRepository<Currency> currencyRepository;

        public ProductPriceController(
            IProductPriceManager productPriceManager,
            IRepository<Product> productRepository,
            IRepository<Currency> currencyRepository)
        {
            this.productPriceManager = productPriceManager;
            this.productRepository = productRepository;
            this.currencyRepository = currencyRepository;
        }

        public async Task<IActionResult> Index()
        {
            var result = await this.productPriceManager.GetAllWithProductAndCurrencyMoreRecentAsync();

            var model = result.Select(r => new ProductPriceListViewModel
            {
                Id = r.Id,
                Currency = r.Currency.Name,
                Product = r.Product.Name,
                SalePrice = r.SalePrice,
                WarrantyTime = r.WarrantyTime(),
            });

            return this.View(model);
        }

        public async Task<IActionResult> Details(int? idProductPrice)
        {
            if (idProductPrice == null)
            {
                return this.BadRequest();
            }

            var productPrice = await this.productPriceManager.GetByIdAsync(idProductPrice.Value);

            if (productPrice == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProductPriceDetailViewModel
            {
                Id = productPrice.Id,
                Currency = productPrice.Currency.Name,
                Product = productPrice.Product.Name,
                SalePrice = productPrice.SalePrice,
                WarrantyTime = productPrice.WarrantyTime(),
            });
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var productPrice = await this.productPriceManager.GetByIdAsync(id.Value);

            if (productPrice == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProductPriceEditViewModel
            {
                Id = productPrice.Id,
                IdCurrency = productPrice.IdCurrency,
                IdProduct = productPrice.IdProduct,
                Currencies = await this.GetCurrencies(),
                Products = await this.GetProducts(),
                SalePrice = productPrice.SalePrice,
                Years = productPrice.Years,
                Months = productPrice.Months,
                Days = productPrice.Days,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProductPriceEditViewModel productPriceEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                productPriceEditViewModel.Products = await this.GetProducts();
                productPriceEditViewModel.Currencies = await this.GetCurrencies();
                return this.View(productPriceEditViewModel);
            }

            var result = await this.productPriceManager.CreateAsync(new ProductPrice
            {
                CreatedDate = DateTime.Now,
                IdCurrency = productPriceEditViewModel.IdCurrency,
                IdProduct = productPriceEditViewModel.IdProduct,
                SalePrice = productPriceEditViewModel.SalePrice,
                Years = productPriceEditViewModel.Years,
                Months = productPriceEditViewModel.Months,
                Days = productPriceEditViewModel.Days,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                productPriceEditViewModel.Products = await this.GetProducts();
                productPriceEditViewModel.Currencies = await this.GetCurrencies();
                return this.View(productPriceEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var productPrice = await this.productPriceManager.GetByIdAsync(id.Value);

            if (productPrice == null)
            {
                return this.BadRequest();
            }

            var result = await this.productPriceManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Create()
        {
            var model = new ProductPriceCreateViewModel
            {
                Products = await this.GetProducts(),
                Currencies = await this.GetCurrencies(),
            };

            return this.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductPriceCreateViewModel productPriceCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                productPriceCreateViewModel.Products = await this.GetProducts();
                productPriceCreateViewModel.Currencies = await this.GetCurrencies();
                return this.View(productPriceCreateViewModel);
            }

            var result = await this.productPriceManager.CreateAsync(new ProductPrice
            {
                CreatedDate = DateTime.Now,
                IdCurrency = productPriceCreateViewModel.IdCurrency,
                IdProduct = productPriceCreateViewModel.IdProduct,
                SalePrice = productPriceCreateViewModel.SalePrice,
                Years = productPriceCreateViewModel.Years,
                Months = productPriceCreateViewModel.Months,
                Days = productPriceCreateViewModel.Days,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                productPriceCreateViewModel.Products = await this.GetProducts();
                productPriceCreateViewModel.Currencies = await this.GetCurrencies();
                return this.View(productPriceCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }

        private async Task<IEnumerable<SelectListViewModel>> GetProducts()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.ProductPrice.ProductPrice.ProductSelected
                }
            }
            .Union(await this.productRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }

        private async Task<IEnumerable<SelectListViewModel>> GetCurrencies()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.ProductPrice.ProductPrice.CurrencySelected
                }
            }
            .Union(await this.currencyRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }
    }
}
