﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Contabilidad.Core.File;
using Contabilidad.Core.Product;
using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Contabilidad.Resources.Models;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.Product;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Contabilidad.Site.Controllers
{
    public class ProductController : Controller
    {
        private readonly IRepository<Brand> brandRepository;
        private readonly IRepository<ProductModel> productModelRepository;
        private readonly IRepository<ProductCategory> productCategoryRepository;
        private readonly IRepository<MeasurementUnit> measurementUnitRepository;
        private readonly IProductManager productManager;
        private readonly IFileManager fileManager;

        public ProductController(
            IRepository<Brand> brandRepository,
            IRepository<ProductModel> productModelRepository,
            IRepository<ProductCategory> productCategoryRepository,
            IRepository<MeasurementUnit> measurementUnitRepository,
            IProductManager productManager,
            IFileManager fileManager)
        {
            this.brandRepository = brandRepository;
            this.productModelRepository = productModelRepository;
            this.productCategoryRepository = productCategoryRepository;
            this.measurementUnitRepository = measurementUnitRepository;
            this.productManager = productManager;
            this.fileManager = fileManager;
        }
        public async Task<IActionResult> Index()
        {
            var products = await this.productManager.GetAllWithDetailsAsync();
            var model = products.OrderBy(b => b.Name).Select(b => new ProductListViewModel
            {
                Id = b.Id,
                Name = b.Name,
                Description = b.Description,
                Brand = b.Brand?.Name,
                MeasurementUnit = b.MeasurementUnit?.MeasurementName,
                ProductCategory = b.ProductCategory?.Name,
                Model = b.Model?.Name,
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            return this.View(new ProductCreateViewModel
            {
                Brands = await this.GetBrands(),
                Models = await this.GetModels(),
                ProductCategories = await this.GetCategories(),
                MeasurementUnits = await this.GetMeasurementUnits(),
            });
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var product = await this.productManager.GetByIdAsync(id.Value);

            if (product == null)
            {
                return this.BadRequest();
            }

            var result = await this.productManager.DeleteAsync(id.Value);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.RedirectToAction(nameof(Index));
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var product = await this.productManager.GetByIdAsync(id.Value);

            if (product == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProductEditViewModel
            {
                Id = product.Id,
                Name = product.Name,
                IdBrand = product.IdBrand,
                IdMeasurementUnit = product.IdMeasurementUnit,
                IdModel = product.IdModel,
                IdProductCategory = product.IdProductCategory,
                Description = product.Description,
                Brands = await this.GetBrands(),
                MeasurementUnits = await this.GetMeasurementUnits(),
                Models = await this.GetModels(),
                ProductCategories = await this.GetCategories(),
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProductEditViewModel productEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                productEditViewModel.Brands = await this.GetBrands();
                productEditViewModel.Models = await this.GetModels();
                productEditViewModel.ProductCategories = await this.GetCategories();
                productEditViewModel.MeasurementUnits = await this.GetMeasurementUnits();

                return this.View(productEditViewModel);
            }

            var product = await this.productManager.GetByIdAsync(productEditViewModel.Id);

            if (product == null)
            {
                return this.BadRequest();
            }

            product.Name = productEditViewModel.Name;
            product.Description = productEditViewModel.Description;
            product.Image = await this.fileManager.GetByteArrayFromFormFile(productEditViewModel.Image);
            product.IdBrand = productEditViewModel.IdBrand.Value;
            product.IdModel = productEditViewModel.IdModel.Value;
            product.IdProductCategory = productEditViewModel.IdProductCategory.Value;
            product.IdMeasurementUnit = productEditViewModel.IdMeasurementUnit.Value;


            var result = await this.productManager.EditAsync(product);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                productEditViewModel.Brands = await this.GetBrands();
                productEditViewModel.Models = await this.GetModels();
                productEditViewModel.ProductCategories = await this.GetCategories();
                productEditViewModel.MeasurementUnits = await this.GetMeasurementUnits();

                return this.View(productEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idProduct)
        {
            if (idProduct == null)
            {
                return this.BadRequest();
            }

            var product = await this.productManager.GetByIdandDetailsAsync(idProduct.Value);

            if (product == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProductDetailsViewModel
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Brand = product.Brand?.Name,
                Image = product.Image,
                MeasurementUnit = product.MeasurementUnit?.MeasurementName,
                Model = product.Model?.Name,
                ProductCategory = product.ProductCategory?.Name,
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductCreateViewModel productCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                productCreateViewModel.Brands = await this.GetBrands();
                productCreateViewModel.Models = await this.GetModels();
                productCreateViewModel.ProductCategories = await this.GetCategories();
                productCreateViewModel.MeasurementUnits = await this.GetMeasurementUnits();

                return this.View(productCreateViewModel);
            }

            var result = await this.productManager.CreateAsync(new Product
            {
                Name = productCreateViewModel.Name,
                Description = productCreateViewModel.Description,
                Image = await this.fileManager.GetByteArrayFromFormFile(productCreateViewModel.Image),
                IdBrand = productCreateViewModel.IdBrand.Value,
                IdMeasurementUnit = productCreateViewModel.IdMeasurementUnit.Value,
                IdModel = productCreateViewModel.IdModel.Value,
                IdProductCategory = productCreateViewModel.IdProductCategory.Value,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                productCreateViewModel.Brands = await this.GetBrands();
                productCreateViewModel.Models = await this.GetModels();
                productCreateViewModel.ProductCategories = await this.GetCategories();
                productCreateViewModel.MeasurementUnits = await this.GetMeasurementUnits();
                return this.View(productCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }

        private async Task<IEnumerable<SelectListViewModel>> GetBrands()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Product.Product.BrandSelect
                }
            }
            .Union(await this.brandRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }
        private async Task<IEnumerable<SelectListViewModel>> GetModels()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Product.Product.ModelSelect
                }
            }
            .Union(await this.productModelRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }
        private async Task<IEnumerable<SelectListViewModel>> GetCategories()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Product.Product.CategorySelect
                }
            }
            .Union(await this.productCategoryRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }
        private async Task<IEnumerable<SelectListViewModel>> GetMeasurementUnits()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Product.Product.MeasurementUnitSelect
                }
            }
            .Union(await this.measurementUnitRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.MeasurementName,
            }).ToListAsync());
        }
    }
}
