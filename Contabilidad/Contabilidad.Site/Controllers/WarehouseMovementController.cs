﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contabilidad.Core.AssignPurchaseToWarehouse;
using Contabilidad.Core.Product;
using Contabilidad.Core.Warehouse;
using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.WarehouseMovement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Contabilidad.Site.Controllers
{
    public class WarehouseMovementController : Controller
    {
        private readonly IWarehouseManager warehouseManager;
        private readonly IWarehouseMovementManager warehouseMovementManager;
        private readonly IRepository<Warehouse> warehouseRepository;
        private readonly IProductManager productManager;

        public WarehouseMovementController(
            IWarehouseManager warehouseManager,
            IWarehouseMovementManager warehouseMovementManager,
            IRepository<Warehouse> warehouseRepository,
            IProductManager productManager)
        {
            this.warehouseManager = warehouseManager;
            this.warehouseMovementManager = warehouseMovementManager;
            this.warehouseRepository = warehouseRepository;
            this.productManager = productManager;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MovementToWareohuses(WarehouseMovementCreateViewModel warehouseMovementCreateViewModel)
        {
            warehouseMovementCreateViewModel.Details = warehouseMovementCreateViewModel.Details.Where(d => d.IsDeleted == null || !d.IsDeleted.Value);

            if (!this.ModelState.IsValid)
            {
                warehouseMovementCreateViewModel.Index = warehouseMovementCreateViewModel.Details.Count();
                return this.View(warehouseMovementCreateViewModel);
            }


            if (warehouseMovementCreateViewModel.Details == null)
            {
                return this.RedirectToAction("Details", "Warehouse", new { idWarehouse = warehouseMovementCreateViewModel.IdWarehouseOrigin });
            }

            var result = await this.warehouseMovementManager.CreateAsync(new Database.Models.Accounting.WarehouseMovement
            {
                IdWarehouseOrigin = warehouseMovementCreateViewModel.IdWarehouseOrigin,
                //IdUsser = CODE HERE   CODE HERE   CODE HERE   CODE HERE
                MovementDate = DateTime.Now,
                Details = warehouseMovementCreateViewModel.Details.Select(d => new Database.Models.Accounting.WarehouseMovementDetail
                {
                    IdProduct = d.IdProduct.Value,
                    IdWarehouseDestination = d.IdWarehouseDestination,
                    Quantity = d.Quantity,
                }).ToList(),
            });


            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                warehouseMovementCreateViewModel.Index = warehouseMovementCreateViewModel.Details.Count();
                return this.View(warehouseMovementCreateViewModel);
            }
            return this.RedirectToAction("Details", "Warehouse", new { idWarehouse = warehouseMovementCreateViewModel.IdWarehouseOrigin });
        }

        public async Task<ActionResult> AddDetail(int? index, int? idWarehouseOrigin)
        {
            if (idWarehouseOrigin == null || index == null)
            {
                return this.BadRequest();
            }

            return PartialView("MovementToWareohusesDetailCreate", new WarehouseMovementDetailCreateViewModel
            {
                Index = index.Value,
                IsDeleted = false,
                Products = await this.GetProducts(idWarehouseOrigin.Value),
                WarehousesDestination = await this.GetWarehouses(idWarehouseOrigin.Value),
            });
        }

        public async Task<IActionResult> MovementToWareohuses(int? idWarehouse)
        {
            if (idWarehouse == null)
            {
                return this.BadRequest();
            }

            var warehouse = await this.warehouseManager.GetByIdAsync(idWarehouse.Value);

            if (warehouse == null)
            {
                return this.BadRequest();
            }

            var model = new WarehouseMovementCreateViewModel()
            {
                Code = warehouse.Code,
                IdWarehouseOrigin = idWarehouse.Value,
                Index = 0,
                Details = new List<WarehouseMovementDetailCreateViewModel>(),
            };

            return this.View(model);
        }

        private async Task<IEnumerable<SelectListViewModel>> GetWarehouses(int? idWarehouse)
        {
            if (idWarehouse == null)
            {
                return new List<SelectListViewModel>();
            }

            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Warehouse.WarehouseMovementDetail.WarehouseDestinationSelected
                }
            }
            .Union(await this.warehouseRepository.Filter(w => w.Id != idWarehouse.Value).Where(w => w.IsActive).Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Code,
            }).ToListAsync());
        }


        private async Task<IEnumerable<SelectListViewModel>> GetProducts(int? idWarehouse)
        {
            if (idWarehouse == null)
            {
                return new List<SelectListViewModel>();
            }

            var warehouse = await this.warehouseManager.GetByIdAsync(idWarehouse.Value);

            if (warehouse == null)
            {
                return new List<SelectListViewModel>();
            }
            var products = await this.productManager.GetAllAsync();
            var productsJoin = warehouse.Details.Where(d => d.Quantity > 0).Select(d => new { d.IdProduct, d.Quantity })
                .Join(products, d => d.IdProduct, d => d.Id, (warehouseProduct, product) => new { product, warehouseProduct.Quantity });

            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Warehouse.WarehouseMovementDetail.ProductSelected
                }
            }
            .Union(productsJoin.Select(b => new SelectListViewModel
            {
                Id = b.product.Id,
                Value = b.product.Name + " (" + b.Quantity + ")",
            }));
        }
    }
}
