﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.ProductCategory;
using Contabilidad.Site.Models.ProductCategory;
using Microsoft.AspNetCore.Mvc;

namespace Contabilidad.Site.Controllers
{
    public class ProductCategoryController : Controller
    {
        private readonly IProductCategoryManager productCategoryManager;

        public ProductCategoryController(IProductCategoryManager productCategoryManager)
        {
            this.productCategoryManager = productCategoryManager;
        }

        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var productCategorys = await this.productCategoryManager.GetAllAsync();
            var model = productCategorys.OrderBy(b => b.Name).Select(b => new ProductCategoryListViewModel
            {
                Id = b.Id,
                Name = b.Name,
                Description = b.Description,
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            return this.View();
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var productCategory = await this.productCategoryManager.GetByIdAsync(id.Value);

            if (productCategory == null)
            {
                return this.BadRequest();
            }

            var result = await this.productCategoryManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var productCategory = await this.productCategoryManager.GetByIdAsync(id.Value);

            if (productCategory == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProductCategoryEditViewModel
            {
                Id = productCategory.Id,
                Name = productCategory.Name,
                Description = productCategory.Description,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProductCategoryEditViewModel productCategoryEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(productCategoryEditViewModel);
            }

            var productCategory = await this.productCategoryManager.GetByIdAsync(productCategoryEditViewModel.Id);

            if (productCategory == null)
            {
                return this.BadRequest();
            }

            productCategory.Name = productCategoryEditViewModel.Name;
            productCategory.Description = productCategoryEditViewModel.Description;

            var result = await this.productCategoryManager.EditAsync(productCategory);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(productCategoryEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idProductCategory)
        {
            if (idProductCategory == null)
            {
                return this.BadRequest();
            }

            var productCategory = await this.productCategoryManager.GetByIdAsync(idProductCategory.Value);

            if (productCategory == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProductCategoryDetailsViewModel
            {
                Id = productCategory.Id,
                Name = productCategory.Name,
                Description = productCategory.Description,
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductCategoryCreateViewModel productCategoryCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(productCategoryCreateViewModel);
            }

            var result = await this.productCategoryManager.CreateAsync(new Database.Models.Accounting.ProductCategory
            {
                Name = productCategoryCreateViewModel.Name,
                Description = productCategoryCreateViewModel.Description,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(productCategoryCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
    }
}
