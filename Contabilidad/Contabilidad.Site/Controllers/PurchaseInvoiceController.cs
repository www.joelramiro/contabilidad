﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.AssignPurchaseToWarehouse;
using Contabilidad.Core.File;
using Contabilidad.Core.Location;
using Contabilidad.Core.PurchaseInvoice;
using Contabilidad.Database.Models;
using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.PurchaseInvoice;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Contabilidad.Site.Controllers
{
    public class PurchaseInvoiceController : Controller
    {
        private readonly IPurchaseInvoiceManager purchaseInvoiceManager;
        private readonly IRepository<Currency> currencyRepository;
        private readonly IRepository<Provider> providerRepository;
        private readonly IRepository<Product> productRepository;
        private readonly IRepository<Warehouse> warehouseRepository;
        private readonly IFileManager fileManager;
        private readonly ILocationManager locationManager;
        private readonly IAssignPurchaseToWarehouseManager assignPurchaseToWarehouseManager;

        public PurchaseInvoiceController(
            IPurchaseInvoiceManager purchaseInvoiceManager,
            IRepository<Currency> currencyRepository,
            IRepository<Provider> providerRepository,
            IRepository<Product> productRepository,
            IRepository<Warehouse> warehouseRepository,
            IFileManager fileManager,
            ILocationManager locationManager,
            IAssignPurchaseToWarehouseManager assignPurchaseToWarehouseManager)
        {
            this.purchaseInvoiceManager = purchaseInvoiceManager;
            this.currencyRepository = currencyRepository;
            this.providerRepository = providerRepository;
            this.productRepository = productRepository;
            this.warehouseRepository = warehouseRepository;
            this.fileManager = fileManager;
            this.locationManager = locationManager;
            this.assignPurchaseToWarehouseManager = assignPurchaseToWarehouseManager;
        }
        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var purchaseInvoices = await this.purchaseInvoiceManager.GetAllAsync();
            var model = purchaseInvoices.Select(b => new PurchaseInvoiceListViewModel
            {
                Code = b.Code,
                Id = b.Id,
                Comments = b.Comments,
                Currency = b.Currency?.Name,
                Disccount = b.Disccount,
                InvoiceType = b.InvoiceType,
                IsPaid = b.IsPaid,
                Total = b.Total,
                Isv = b.Isv,
                SubTotal = b.SubTotal,
                Provider = b.Provider?.People?.FullName(),
                IsAssigned = b.IsAssignedToWarehouse,
            });

            return View(model);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var purchaseInvoice = await this.purchaseInvoiceManager.GetByIdAsync(id.Value);

            if (purchaseInvoice == null)
            {
                return this.BadRequest();
            }

            var model = new PurchaseInvoiceDetailsViewModel 
            {
                Id = purchaseInvoice.Id,
                Code = purchaseInvoice.Code,
                Comments = purchaseInvoice.Comments,
                Currency = purchaseInvoice.Currency.Name,
                Disccount = purchaseInvoice.Disccount,
                InvoiceType = purchaseInvoice.InvoiceType,
                IsPaid = purchaseInvoice.IsPaid,
                Isv = purchaseInvoice.Isv,
                Provider = purchaseInvoice.Provider.Description,
                Total = purchaseInvoice.Total,
                SubTotal = purchaseInvoice.SubTotal,
                Details = purchaseInvoice.Details.Select(d => new PurchaseInvoiceDetailsDetailsViewModel 
                {
                    Id = d.Id,
                    Product = d.Product?.Name,
                    Quantity = d.Quantity,
                    Total = d.Total,
                    UnitPrice = d.UnitPrice,
                }),
            };
            

            return this.View(model);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var purchaseInvoice = await this.purchaseInvoiceManager.GetByIdAsync(id.Value);

            if (purchaseInvoice == null)
            {
                return this.BadRequest();
            }

            var result = await this.purchaseInvoiceManager.DeleteAsync(id.Value);
            var stringBuilder = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        stringBuilder.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = stringBuilder.ToString() });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Create()
        {
            return this.View(new PurchaseInvoiceCreateViewModel
            {
                Details = new List<PurchaseInvoiceDetailCreateViewModel>(),
                Index = 0,
                Providers = await this.GetProviders(),
                Currencies = await this.GetCurrencies(),
                Warehouses = await this.GetWarehouses(),
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PurchaseInvoiceEditViewModel purchaseInvoiceEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                purchaseInvoiceEditViewModel.Details = purchaseInvoiceEditViewModel.Details != null ? purchaseInvoiceEditViewModel.Details.Where(d => !d.IsDeleted.GetValueOrDefault()) : new List<PurchaseInvoiceDetailCreateViewModel>();
                purchaseInvoiceEditViewModel.Currencies = await this.GetCurrencies();
                purchaseInvoiceEditViewModel.Providers = await this.GetProviders();
                purchaseInvoiceEditViewModel.Index = purchaseInvoiceEditViewModel.Details.Count();
                purchaseInvoiceEditViewModel.Warehouses = await this.GetWarehouses();
                return this.View(purchaseInvoiceEditViewModel);
            }
            var purchase = await this.purchaseInvoiceManager.GetByIdAsync(purchaseInvoiceEditViewModel.IdPurchaseInvoice);

            if (purchase == null)
            {
                return this.BadRequest();
            }

            var total = purchaseInvoiceEditViewModel.Details == null ? 0 : purchaseInvoiceEditViewModel.Details.Where(d => !d.IsDeleted.GetValueOrDefault()).Sum(d => d.UnitPrice * d.Quantity);

            purchaseInvoiceEditViewModel.Details = purchaseInvoiceEditViewModel.Details.GroupBy(p => new { p.IdProduct, p.UnitPrice })
                .Select(p => new PurchaseInvoiceDetailCreateViewModel
                {
                    IdProduct = p.Key.IdProduct.Value,
                    UnitPrice = p.Key.UnitPrice,
                    Quantity = p.Sum(d => d.Quantity),
                }).ToList();

            purchase.Code = purchaseInvoiceEditViewModel.Code;
            purchase.Comments = purchaseInvoiceEditViewModel.Comments;
            purchase.LastEditDate = DateTime.Now;
            purchase.Disccount = purchaseInvoiceEditViewModel.Disccount;
            //purchase.//IdUsser = CODE HERE   CODE HERE   CODE HERE   CODE HERE   CODE HERE
            purchase.IdProvider = purchaseInvoiceEditViewModel.IdProvider.Value;
            purchase.IdCurrency = purchaseInvoiceEditViewModel.IdCurrency.Value;
            purchase.FileInBytes = (await this.fileManager.GsetByFormFileAsync(purchaseInvoiceEditViewModel.Voucher)).Result;
            purchase.Location = (await this.locationManager.GetLocationAsync());
            purchase.InvoiceType = purchaseInvoiceEditViewModel.InvoiceType;
            purchase.Total = total;
            purchase.Isv = purchaseInvoiceEditViewModel.Isv;
            purchase.SubTotal = total - purchaseInvoiceEditViewModel.Disccount + (total * purchaseInvoiceEditViewModel.Isv / 100);
            purchase.IsPaid = purchaseInvoiceEditViewModel.IsPaid;
            purchase.Details = purchaseInvoiceEditViewModel.Details == null ? null : purchaseInvoiceEditViewModel.Details.Where(d => !d.IsDeleted.GetValueOrDefault()).Select(d => new PurchaseInvoiceDetail
            {
                IdProduct = d.IdProduct.Value,
                UnitPrice = d.UnitPrice,
                Quantity = d.Quantity,
                Total = d.UnitPrice * d.Quantity,
            }).ToList();

            var result = await this.purchaseInvoiceManager.EditAsync(purchase);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                purchaseInvoiceEditViewModel.Warehouses = await this.GetWarehouses();
                purchaseInvoiceEditViewModel.Currencies = await this.GetCurrencies();
                purchaseInvoiceEditViewModel.Providers = await this.GetProviders();
                purchaseInvoiceEditViewModel.Details = purchaseInvoiceEditViewModel.Details != null ? purchaseInvoiceEditViewModel.Details.Where(d => !d.IsDeleted.GetValueOrDefault()) : new List<PurchaseInvoiceDetailCreateViewModel>();
                purchaseInvoiceEditViewModel.Index = purchaseInvoiceEditViewModel.Details.Count();
                return this.View(purchaseInvoiceEditViewModel);
            }
            if (purchaseInvoiceEditViewModel.WarehouseSelected != null)
            {
                var assigned = await this.assignPurchaseToWarehouseManager.AssignPurchaseInvoiceToWarehouse(purchase, purchaseInvoiceEditViewModel.WarehouseSelected.Value);
            }

            return this.RedirectToAction(nameof(Index));

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PurchaseInvoiceCreateViewModel purchaseInvoiceCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                purchaseInvoiceCreateViewModel.Details = purchaseInvoiceCreateViewModel.Details != null ? purchaseInvoiceCreateViewModel.Details.Where(d => !d.IsDeleted.GetValueOrDefault()) : new List<PurchaseInvoiceDetailCreateViewModel>();
                purchaseInvoiceCreateViewModel.Currencies = await this.GetCurrencies();
                purchaseInvoiceCreateViewModel.Providers = await this.GetProviders();
                purchaseInvoiceCreateViewModel.Index = purchaseInvoiceCreateViewModel.Details.Count();
                purchaseInvoiceCreateViewModel.Warehouses = await this.GetWarehouses();
                return this.View(purchaseInvoiceCreateViewModel);
            }
            var total = purchaseInvoiceCreateViewModel.Details == null ? 0 : purchaseInvoiceCreateViewModel.Details.Where(d => !d.IsDeleted.GetValueOrDefault()).Sum(d => d.UnitPrice * d.Quantity);

            purchaseInvoiceCreateViewModel.Details = purchaseInvoiceCreateViewModel.Details.GroupBy(p => new { p.IdProduct, p.UnitPrice })
                .Select(p => new PurchaseInvoiceDetailCreateViewModel
                {
                    IdProduct = p.Key.IdProduct.Value,
                    UnitPrice = p.Key.UnitPrice,
                    Quantity = p.Sum(d => d.Quantity),
                }).ToList();

            var purchase = new PurchaseInvoice
            {
                Code = purchaseInvoiceCreateViewModel.Code,
                Comments = purchaseInvoiceCreateViewModel.Comments,
                CreatedDate = DateTime.Now,
                Disccount = purchaseInvoiceCreateViewModel.Disccount,
                //IdUsser = CODE HERE   CODE HERE   CODE HERE   CODE HERE   CODE HERE
                IdProvider = purchaseInvoiceCreateViewModel.IdProvider.Value,
                IdCurrency = purchaseInvoiceCreateViewModel.IdCurrency.Value,
                FileInBytes = (await this.fileManager.GsetByFormFileAsync(purchaseInvoiceCreateViewModel.Voucher)).Result,
                Location = (await this.locationManager.GetLocationAsync()),
                InvoiceType = purchaseInvoiceCreateViewModel.InvoiceType,
                Total = total,
                Isv = purchaseInvoiceCreateViewModel.Isv,
                SubTotal = total - purchaseInvoiceCreateViewModel.Disccount + (total * purchaseInvoiceCreateViewModel.Isv / 100),
                IsPaid = purchaseInvoiceCreateViewModel.IsPaid,
                Details = purchaseInvoiceCreateViewModel.Details == null ? null : purchaseInvoiceCreateViewModel.Details.Where(d => !d.IsDeleted.GetValueOrDefault()).Select(d => new PurchaseInvoiceDetail
                {
                    IdProduct = d.IdProduct.Value,
                    UnitPrice = d.UnitPrice,
                    Quantity = d.Quantity,
                    Total = d.UnitPrice * d.Quantity,
                }).ToList(),
            };
            var result = await this.purchaseInvoiceManager.CreateAsync(purchase);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                purchaseInvoiceCreateViewModel.Warehouses = await this.GetWarehouses();
                purchaseInvoiceCreateViewModel.Currencies = await this.GetCurrencies();
                purchaseInvoiceCreateViewModel.Providers = await this.GetProviders();
                purchaseInvoiceCreateViewModel.Details = purchaseInvoiceCreateViewModel.Details != null ? purchaseInvoiceCreateViewModel.Details.Where(d => !d.IsDeleted.GetValueOrDefault()) : new List<PurchaseInvoiceDetailCreateViewModel>();
                purchaseInvoiceCreateViewModel.Index = purchaseInvoiceCreateViewModel.Details.Count();
                return this.View(purchaseInvoiceCreateViewModel);
            }
            if (purchaseInvoiceCreateViewModel.WarehouseSelected != null)
            {
                var assigned = await this.assignPurchaseToWarehouseManager.AssignPurchaseInvoiceToWarehouse(purchase, purchaseInvoiceCreateViewModel.WarehouseSelected.Value);
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var purchaseInvoice = await this.purchaseInvoiceManager.GetByIdAsync(id.Value);

            if (purchaseInvoice == null)
            {
                return this.BadRequest();
            }

            if (purchaseInvoice.IsAssignedToWarehouse)
            {
                return this.BadRequest();
            }

            var model = new PurchaseInvoiceEditViewModel
            {
                IdPurchaseInvoice = purchaseInvoice.Id,
                Code = purchaseInvoice.Code,
                Comments = purchaseInvoice.Comments,
                Disccount = purchaseInvoice.Disccount,
                IdCurrency = purchaseInvoice.IdCurrency,
                IdProvider = purchaseInvoice.IdProvider,
                InvoiceType = purchaseInvoice.InvoiceType,
                IsPaid = purchaseInvoice.IsPaid,
                Isv = purchaseInvoice.Isv,
                SubTotal = purchaseInvoice.SubTotal,
                Total = purchaseInvoice.Total,
                Providers = await this.GetProviders(),
                Currencies = await this.GetCurrencies(),
                Warehouses = await this.GetWarehouses(),
                Index = purchaseInvoice.Details.Count(),
                Details = purchaseInvoice.Details != null ? purchaseInvoice.Details.Select((d, index) =>
                    new PurchaseInvoiceDetailCreateViewModel
                    {
                        Id = d.Id,
                        IdProduct = d.IdProduct,
                        IsDeleted = false,
                        Quantity = d.Quantity,
                        UnitPrice = d.UnitPrice,
                        Total = d.Total,
                        Index = index,
                    }) : new List<PurchaseInvoiceDetailCreateViewModel>(),
            };

            return this.View(model);
        }

        public async Task<ActionResult> AddDetail(int? index)
        {
            return PartialView("PurchaseInvoiceDetailCreate", new PurchaseInvoiceDetailCreateViewModel
            {
                Index = index.Value,
                IsDeleted = false,
                Products = await this.GetProducts(),
            });
        }
        public async Task<ActionResult> UnionDetail(PurchaseInvoiceDetailCreateViewModel detail, int? index)
        {
            detail.Index = index.Value;
            detail.IsDeleted = false;
            detail.Products = await this.GetProducts();
            return PartialView("PurchaseInvoiceDetailCreate", detail);
        }

        private async Task<IEnumerable<SelectListViewModel>> GetProducts()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail.ProductSelect,
                }
            }
            .Union(await this.productRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }

        private async Task<IEnumerable<SelectListViewModel>> GetProviders()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice.ProviderSelect
                }
            }
            .Union(await this.providerRepository.All().Include(p => p.People).Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.People == null ? string.Empty : b.People.FullName(),
            }).ToListAsync());
        }

        private async Task<IEnumerable<SelectListViewModel>> GetWarehouses()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice.WarehouseSelected
                }
            }
            .Union(await this.warehouseRepository.All().Where(w => w.IsActive).Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Code,
            }).ToListAsync());
        }

        private async Task<IEnumerable<SelectListViewModel>> GetCurrencies()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice.CurrencySelect
                }
            }
            .Union(await this.currencyRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }
    }
}
