﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.Client;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Contabilidad.Site.Controllers
{
    public class ClientController : Controller
    {
        private readonly IClientManager clientManager;
        private readonly IRepository<Database.Model.People.People> peopleRepository;

        public ClientController(
            IClientManager clientManager,
            IRepository<Database.Model.People.People> peopleRepository)
        {
            this.clientManager = clientManager;
            this.peopleRepository = peopleRepository;
        }
        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var clients = await this.clientManager.GetAllAsync();
            var model = clients.OrderBy(b => b.People).Select(b => new ClientListViewModel
            {
                Id = b.Id,
                People = b.People?.FullName(),
                BusinessName = b.BusinessName,
                Description = b.Description,
                RTN = b.RTN,
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            return this.View(new ClientCreateViewModel 
            {
                Peoples = await  this.GetPeoples(),
            });
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var client = await this.clientManager.GetByIdAsync(id.Value);

            if (client == null)
            {
                return this.BadRequest();
            }

            var result = await this.clientManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var client = await this.clientManager.GetByIdAsync(id.Value);

            if (client == null)
            {
                return this.BadRequest();
            }

            return this.View(new ClientEditViewModel
            {
                Id = client.Id,
                Peoples = await this.GetPeoples(),
                BusinessName = client.BusinessName,
                Description = client.Description,
                IdPeople = client.IdPeople,
                RTN = client.RTN,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ClientEditViewModel clientEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                clientEditViewModel.Peoples = await this.GetPeoples();
                return this.View(clientEditViewModel);
            }

            var client = await this.clientManager.GetByIdAsync(clientEditViewModel.Id);

            if (client == null)
            {
                return this.BadRequest();
            }

            client.BusinessName = clientEditViewModel.BusinessName;
            client.Description = clientEditViewModel.Description;
            client.IdPeople = clientEditViewModel.IdPeople.Value;
            client.RTN = clientEditViewModel.RTN;

            var result = await this.clientManager.EditAsync(client);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                clientEditViewModel.Peoples = await this.GetPeoples();
                return this.View(clientEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idClient)
        {
            if (idClient == null)
            {
                return this.BadRequest();
            }

            var client = await this.clientManager.GetByIdAsync(idClient.Value);

            if (client == null)
            {
                return this.BadRequest();
            }

            return this.View(new ClientDetailsViewModel
            {
                Id = client.Id,
                People = client.People?.FullName(),
                BusinessName = client.BusinessName,
                Description = client.Description,
                RTN = client.RTN,
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClientCreateViewModel clientCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                clientCreateViewModel.Peoples = await this.GetPeoples();
                return this.View(clientCreateViewModel);
            }

            var result = await this.clientManager.CreateAsync(new Database.Models.Accounting.Client
            {
                BusinessName = clientCreateViewModel.BusinessName,
                IdPeople = clientCreateViewModel.IdPeople.Value,
                Description = clientCreateViewModel.Description,
                RTN = clientCreateViewModel.RTN,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                clientCreateViewModel.Peoples = await this.GetPeoples();
                return this.View(clientCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        private async Task<IEnumerable<SelectListViewModel>> GetPeoples()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Client.Client.PeopleSelect,
                }
            }
            .Union(await this.peopleRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.FullName(),
            }).ToListAsync());
        }

    }
}
