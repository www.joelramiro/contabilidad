﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.Brand;
using Contabilidad.Site.Models.Brand;
using Microsoft.AspNetCore.Mvc;

namespace Contabilidad.Site.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandManager brandManager;

        public BrandController(IBrandManager brandManager)
        {
            this.brandManager = brandManager;
        }
        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var brands = await this.brandManager.GetAllAsync();
            var model = brands.OrderBy(b => b.Name).Select(b => new BrandListViewModel
            {
                Id = b.Id,
                Name = b.Name
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            return this.View();
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var brand = await this.brandManager.GetByIdAsync(id.Value);

            if (brand == null)
            {
                return this.BadRequest();
            }

            var result = await this.brandManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var brand = await this.brandManager.GetByIdAsync(id.Value);

            if (brand == null)
            {
                return this.BadRequest();
            }

            return this.View(new BrandEditViewModel
            {
                Id = brand.Id,
                Name = brand.Name,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(BrandEditViewModel brandEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(brandEditViewModel);
            }

            var brand = await this.brandManager.GetByIdAsync(brandEditViewModel.Id);

            if (brand == null)
            {
                return this.BadRequest();
            }

            brand.Name = brandEditViewModel.Name;

            var result = await this.brandManager.EditAsync(brand);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(brandEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idBrand)
        {
            if (idBrand == null)
            {
                return this.BadRequest();
            }

            var brand = await this.brandManager.GetByIdAsync(idBrand.Value);

            if (brand == null)
            {
                return this.BadRequest();
            }

            return this.View(new BrandDetailsViewModel
            {
                Id = brand.Id,
                Name = brand.Name,
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BrandCreateViewModel brandCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(brandCreateViewModel);
            }

            var result = await this.brandManager.CreateAsync(new Database.Models.Accounting.Brand
            {
                Name = brandCreateViewModel.Name,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(brandCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
    }
}
