﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contabilidad.Core.AssignPurchaseToWarehouse;
using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.AssignPurchaseToWarehouse;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Schema;

namespace Contabilidad.Site.Controllers
{
    public class AssignPurchaseToWarehouseController : Controller
    {
        private readonly IAssignPurchaseToWarehouseManager assignPurchaseToWarehouseManager;
        private readonly IRepository<PurchaseInvoice> purchaseInvoiceRepository;
        private readonly IRepository<AssignPurchaseToWarehouse> assignPurchaseToWarehouseRepository;
        private readonly IRepository<Warehouse> warehouseRepository;

        public AssignPurchaseToWarehouseController(
            IAssignPurchaseToWarehouseManager assignPurchaseToWarehouseManager,
            IRepository<PurchaseInvoice> purchaseInvoiceRepository,
            IRepository<AssignPurchaseToWarehouse> assignPurchaseToWarehouseRepository,
            IRepository<Warehouse> warehouseRepository)
        {
            this.assignPurchaseToWarehouseManager = assignPurchaseToWarehouseManager;
            this.purchaseInvoiceRepository = purchaseInvoiceRepository;
            this.assignPurchaseToWarehouseRepository = assignPurchaseToWarehouseRepository;
            this.warehouseRepository = warehouseRepository;
        }

        public async Task<IActionResult> Index()
        {
            var model = await this.purchaseInvoiceRepository.All()
                .Where(p => !p.IsAssignedToWarehouse)
                .Include(p => p.Provider)
                .Include(p => p.Currency)
                .Select(p => new AssignPurchaseToWarehouseListViewModel
                {
                    Code = p.Code,
                    Comments = p.Comments,
                    CreatedDate = p.CreatedDate,
                    Currency = p.Currency.Name,
                    IdInvoice = p.Id,
                    IsPaid = p.IsPaid,
                    Provider = p.Provider.Description,
                    SubTotal = p.SubTotal,
                }).ToListAsync();
            return View(model);
        }


        public async Task<IActionResult> AssignToWarehouse(int? idInvoice)
        {
            if (idInvoice == null)
            {
                return this.BadRequest();
            }

            var invoice = await this.purchaseInvoiceRepository.All()
                .FirstOrDefaultAsync(i => i.Id == idInvoice.Value);

            if (invoice == null)
            {
                return this.BadRequest();
            }

            var model = new AssignPurchaseToWarehouseCreateViewModel
            {
                InvoiceCode = invoice.Code,
                Index = 0,
                IdInvoice = idInvoice.Value,
                Details = new List<AssignPurchaseToWarehouseDetailCreateViewModel>(),
            };

            return this.View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AssignToWarehouse(AssignPurchaseToWarehouseCreateViewModel assignPurchaseToWarehouseCreateViewModel)
        {
            assignPurchaseToWarehouseCreateViewModel.Details = assignPurchaseToWarehouseCreateViewModel.Details.Where(d => d.IsDeleted == null || !d.IsDeleted.Value);
            if (!this.ModelState.IsValid)
            {
                assignPurchaseToWarehouseCreateViewModel.Index = assignPurchaseToWarehouseCreateViewModel.Details.Count();
                return this.View(assignPurchaseToWarehouseCreateViewModel);
            }


            if (assignPurchaseToWarehouseCreateViewModel.Details == null)
            {
                return this.RedirectToAction(nameof(Index));
            }

            var invoice = await this.purchaseInvoiceRepository.All()
                .Include(d => d.Details).FirstOrDefaultAsync(d => d.Id == assignPurchaseToWarehouseCreateViewModel.IdInvoice);

            assignPurchaseToWarehouseCreateViewModel.Details = assignPurchaseToWarehouseCreateViewModel.Details == null ? new List<AssignPurchaseToWarehouseDetailCreateViewModel>() : assignPurchaseToWarehouseCreateViewModel.Details;

            var detailJoin = assignPurchaseToWarehouseCreateViewModel.Details.Join(invoice.Details, d => d.IdProduct, d => d.IdProduct, (d1, d2) => new { d = d1, price = d2.UnitPrice });

            var result = await this.assignPurchaseToWarehouseManager.CreateAsync(new AssignPurchaseToWarehouse
            {
                CreatedDate = DateTime.Now,
                IdPurchaseInvoice = assignPurchaseToWarehouseCreateViewModel.IdInvoice,
                //IdUsser CODE HERE CODE HERE   CODE HERE   CODE HERE,
                Detail = detailJoin.Select(d => new AssignPurchaseToWarehouseDetail
                {
                    IdProduct = d.d.IdProduct.Value,
                    Quantity = d.d.Quantity.Value,
                    IdWarehouse = d.d.IdWarehouse.Value,
                    PurchaseUnitPrice = d.price,
                }).ToList(),
            });
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                assignPurchaseToWarehouseCreateViewModel.Index = assignPurchaseToWarehouseCreateViewModel.Details.Count();
                return this.View(assignPurchaseToWarehouseCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> AddDetail(int? index, int? idPurchaseInvoice)
        {
            if (idPurchaseInvoice == null)
            {
                return this.BadRequest();
            }

            return PartialView("AssignPurchaseToWarehouseDetailCreate", new AssignPurchaseToWarehouseDetailCreateViewModel
            {
                Index = index.Value,
                IsDeleted = false,
                Products = await this.GetProducts(idPurchaseInvoice.Value),
                Warehouses = await this.GetWarehouses(),
            });
        }

        private async Task<IEnumerable<SelectListViewModel>> GetProducts(int idPurchaseInvoice)
        {
            var productsInvoice = await this.purchaseInvoiceRepository.All()
                .Include(p => p.Details)
                .ThenInclude(p => p.Product).FirstOrDefaultAsync(i => i.Id == idPurchaseInvoice);

            if (productsInvoice == null)
            {
                return new List<SelectListViewModel>();
            }

            var preAssigned = await this.assignPurchaseToWarehouseRepository.All()
                .Include(d => d.Detail)
                .ThenInclude(d => d.Product)
                .Where(a => a.IdPurchaseInvoice == idPurchaseInvoice)
                .Select(d => new { header = d, detail = d.Detail })
                .SelectMany(d => d.detail, (h, d) => new { d })
                .ToListAsync();

            var preasignedGroup = preAssigned.GroupBy(d => d.d.Product)
                .Select(p => new
                {
                    Product = p.Key,
                    Quantity = p.Sum(d => d.d.Quantity),
                });

            var pending = productsInvoice.Details
                .GroupJoin(preAssigned, d => d.IdProduct, d => d.d.IdProduct, (principal, preAssigned) => new { principal, preAssigned })
                .SelectMany(d => d.preAssigned.DefaultIfEmpty(), (principal, preAsigned) => new { principal.principal, preAsigned })
                .Select(d => new { Product = d.principal.Product, principalQuantity = d.principal.Quantity, d.preAsigned })
                .GroupBy(d => d.Product)
                .Select(d => new
                {
                    Product = d.Key,
                    principal = d.FirstOrDefault().principalQuantity,
                    secundary = d.Sum(dd => dd.preAsigned == null ? 0 : dd.preAsigned.d.Quantity)
                })
                .Where(p => p.secundary < p.principal)
                .ToList();



            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail.ProductSelect,
                }
}
            .Union(pending.Select(b => new SelectListViewModel
            {
                Id = b.Product.Id,
                Value = b.Product.Name + " (" + (b.principal - b.secundary) + ")",
            }).ToList());
        }

        private async Task<IEnumerable<SelectListViewModel>> GetWarehouses()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail.WarehouseSelect
                }
            }
            .Union(await this.warehouseRepository.All().Where(w => w.IsActive).Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Code,
            }).ToListAsync());
        }
    }
}
