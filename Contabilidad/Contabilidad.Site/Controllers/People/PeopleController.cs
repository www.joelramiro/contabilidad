﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contabilidad.Core.PeopleData.People;
using Contabilidad.Database.Model.People;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.PeopleData.People;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Contabilidad.Site.Controllers.People
{
    public class PeopleController : Controller
    {
        private readonly IPeopleManager peopleManager;
        private readonly IRepository<CivilStatus> civilStatusRepository;
        private readonly IRepository<SexType> sexTypeRepository;
        private readonly IRepository<EducationLevel> educationLevelRepository;

        public PeopleController(
            IPeopleManager peopleManager,
            IRepository<CivilStatus> civilStatusRepository,
            IRepository<SexType> sexTypeRepository,
            IRepository<EducationLevel> educationLevelRepository)
        {
            this.peopleManager = peopleManager;
            this.civilStatusRepository = civilStatusRepository;
            this.sexTypeRepository = sexTypeRepository;
            this.educationLevelRepository = educationLevelRepository;
        }

        public async Task<IActionResult> Index()
        {
            var list = (await this.peopleManager.GetAllAsync()).Select(p => new PeopleListViewModel
            {
                Id = p.Id,
                FullName = $"{p.FirstName} {p.SecondName} {p.MiddleName} {p.LastName}",
                BirthDate = p.BirthDate,
                CivilStatus = p.CivilStatus.Description,
                EducationLevel = p.EducationLevel.Level,
                Identity = p.Identity,
                SexType = p.SexType.Value,
            }).ToList();

            return this.View(list);
        }

        public async Task<IActionResult> Create()
        {
            return this.View(new PeopleCreateViewModel
            {
                CivilStatuses = await this.GetCivilStatuses(),
                SexTypes = await this.GetSexTypes(),
                EducationLevels = await this.GetEducationLevels(),
                BirthDate = DateTime.Now,
            });
        }

        public async Task<IActionResult> Details(int? idPeople)
        {
            if (idPeople == null)
            {
                return this.BadRequest();
            }

            var people = await this.peopleManager.GetByIdAsync(idPeople.Value);

            if (people == null)
            {
                return this.BadRequest();
            }

            return this.View(new PeopleDetailsViewModel
            {
                Id = people.Id,
                SexType = people.SexType.Value,
                BirthDate = people.BirthDate,
                CivilStatus = people.CivilStatus.Description,
                EducationLevel = people.EducationLevel.Level,
                Identity = people.Identity,
                FullName = $"{people.FirstName} {people.SecondName} {people.MiddleName} {people.LastName}",
            });
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var people = await this.peopleManager.GetByIdAsync(id.Value);

            if (people == null)
            {
                return this.BadRequest();
            }

            var result = await this.peopleManager.DeleteAsync(id.Value);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.RedirectToAction("Index");
            }

            return this.RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var people = await this.peopleManager.GetByIdAsync(id.Value);

            if (people == null)
            {
                return this.BadRequest();
            }

            return this.View(new PeopleEditViewModel
            {
                SexTypeId = people.IdSexType,
                CivilStatusId = people.IdCivilStatus,
                EducationLevelId = people.IdEducationLevel,
                CivilStatuses = await this.GetCivilStatuses(),
                SexTypes = await this.GetSexTypes(),
                EducationLevels = await this.GetEducationLevels(),
                Id = people.Id,
                BirthDate = people.BirthDate,
                FirstName = people.FirstName,
                SecondName = people.SecondName,
                MiddleName = people.MiddleName,
                LastName = people.LastName,
                Identity = people.Identity,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PeopleEditViewModel peopleEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                peopleEditViewModel.CivilStatuses = await this.GetCivilStatuses();
                peopleEditViewModel.SexTypes = await this.GetSexTypes();
                peopleEditViewModel.EducationLevels = await this.GetEducationLevels();
                return this.View(peopleEditViewModel);
            }

            var people = await this.peopleManager.GetByIdAsync(peopleEditViewModel.Id);

            if (people == null)
            {
                return this.BadRequest();
            }

            people.IdEducationLevel = peopleEditViewModel.EducationLevelId.Value;
            people.BirthDate = peopleEditViewModel.BirthDate;
            people.IdCivilStatus = peopleEditViewModel.CivilStatusId.Value;
            people.FirstName = peopleEditViewModel.FirstName;
            people.SecondName = peopleEditViewModel.SecondName;
            people.MiddleName = peopleEditViewModel.MiddleName;
            people.LastName = peopleEditViewModel.LastName;
            people.Identity = peopleEditViewModel.Identity;
            people.IdSexType = peopleEditViewModel.SexTypeId.Value;

            var result = await this.peopleManager.EditAsync(people);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                peopleEditViewModel.CivilStatuses = await this.GetCivilStatuses();
                peopleEditViewModel.SexTypes = await this.GetSexTypes();
                peopleEditViewModel.EducationLevels = await this.GetEducationLevels();
                return this.View(peopleEditViewModel);
            }

            return this.RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PeopleCreateViewModel peopleCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                peopleCreateViewModel.CivilStatuses = await this.GetCivilStatuses();
                peopleCreateViewModel.SexTypes = await this.GetSexTypes();
                peopleCreateViewModel.EducationLevels = await this.GetEducationLevels();
                return this.View(peopleCreateViewModel);
            }

            var result = await this.peopleManager.CreateAsync(
                new Database.Model.People.People
                {
                    IdEducationLevel = peopleCreateViewModel.EducationLevelId.Value,
                    BirthDate = peopleCreateViewModel.BirthDate,
                    IdCivilStatus = peopleCreateViewModel.CivilStatusId.Value,
                    FirstName = peopleCreateViewModel.FirstName,
                    SecondName = peopleCreateViewModel.SecondName,
                    MiddleName = peopleCreateViewModel.MiddleName,
                    LastName = peopleCreateViewModel.LastName,
                    Identity = peopleCreateViewModel.Identity,
                    IdSexType = peopleCreateViewModel.SexTypeId.Value,
                });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                peopleCreateViewModel.CivilStatuses = await this.GetCivilStatuses();
                peopleCreateViewModel.SexTypes = await this.GetSexTypes();
                peopleCreateViewModel.EducationLevels = await this.GetEducationLevels();
                return this.View(peopleCreateViewModel);
            }

            return this.RedirectToAction("Index");
        }
        private async Task<IEnumerable<SelectListViewModel>> GetCivilStatuses()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.People.People.People.CivilStatusSelect,
                }
            }
            .Union(await this.civilStatusRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Description,
            }).ToListAsync());
        }
        private async Task<IEnumerable<SelectListViewModel>> GetSexTypes()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.People.People.People.SexTypeSelect,
                }
            }
            .Union(await this.sexTypeRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Value,
            }).ToListAsync());
        }
        private async Task<IEnumerable<SelectListViewModel>> GetEducationLevels()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.People.People.People.EducationLevelSelect,
                }
            }
            .Union(await this.educationLevelRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Level,
            }).ToListAsync());
        }
    }
}