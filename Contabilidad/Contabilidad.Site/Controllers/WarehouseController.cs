﻿using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.Warehouse;
using Contabilidad.Site.Models.Warehouse;
using Microsoft.AspNetCore.Mvc;

namespace Contabilidad.Site.Controllers
{
    public class WarehouseController : Controller
    {
        private readonly IWarehouseManager warehouseManager;

        public WarehouseController(
            IWarehouseManager warehouseManager)
        {
            this.warehouseManager = warehouseManager;
        }
        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var warehouses = await this.warehouseManager.GetAllAsync();
            var model = warehouses.OrderBy(b => b.Code).Select(b => new WarehouseListViewModel
            {
                Id = b.Id,
                Description = b.Description,
                Code = b.Code,
                IsActive = b.IsActive,
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            return this.View();
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var warehouse = await this.warehouseManager.GetByIdAsync(id.Value);

            if (warehouse == null)
            {
                return this.BadRequest();
            }

            var result = await this.warehouseManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var warehouse = await this.warehouseManager.GetByIdAsync(id.Value);

            if (warehouse == null)
            {
                return this.BadRequest();
            }

            return this.View(new WarehouseEditViewModel
            {
                Id = warehouse.Id,
                Description = warehouse.Description,
                Code = warehouse.Code,
                IsActive = warehouse.IsActive,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(WarehouseEditViewModel warehouseEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(warehouseEditViewModel);
            }

            var warehouse = await this.warehouseManager.GetByIdAsync(warehouseEditViewModel.Id);

            if (warehouse == null)
            {
                return this.BadRequest();
            }

            warehouse.Code = warehouseEditViewModel.Code;
            warehouse.Description = warehouseEditViewModel.Description;
            warehouse.IsActive = warehouseEditViewModel.IsActive;

            var result = await this.warehouseManager.EditAsync(warehouse);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(warehouseEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idWarehouse)
        {
            if (idWarehouse == null)
            {
                return this.BadRequest();
            }

            var warehouse = await this.warehouseManager.GetByIdAsync(idWarehouse.Value);

            if (warehouse == null)
            {
                return this.BadRequest();
            }
            return this.View(new WarehouseDetailsViewModel
            {
                Id = warehouse.Id,
                Code = warehouse.Code,
                Description = warehouse.Description,
                IsActive = warehouse.IsActive,
                Stocks = warehouse.Details.Where(d => d.Quantity > 0).Select(d => new StockViewModel
                {
                    Product = d.Product.Name,
                    Quantity = d.Quantity,
                    MeasurementUnit = d.Product.MeasurementUnit.MeasurementName,
                }),
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(WarehouseCreateViewModel warehouseCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(warehouseCreateViewModel);
            }

            var result = await this.warehouseManager.CreateAsync(new Database.Models.Accounting.Warehouse
            {
                Code = warehouseCreateViewModel.Code,
                Description = warehouseCreateViewModel.Description,
                IsActive = warehouseCreateViewModel.IsActive,
                Location = new Database.Models.Accounting.Location
                {
                    Accuracy = 0,
                    Altitude = 0,
                    AltitudeAccuracy = 0,
                    Heading = 0,
                    Latitude = 0,
                    Longitude = 0,
                },
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(warehouseCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
    }
}
