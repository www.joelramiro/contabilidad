﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.MeasurementUnit;
using Contabilidad.Site.Models.MeasurementUnit;
using Microsoft.AspNetCore.Mvc;

namespace Contabilidad.Site.Controllers
{
    public class MeasurementUnitController : Controller
    {
        private readonly IMeasurementUnitManager measurementUnitManager;

        public MeasurementUnitController(IMeasurementUnitManager measurementUnitManager)
        {
            this.measurementUnitManager = measurementUnitManager;
        }

        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var measurementUnits = await this.measurementUnitManager.GetAllAsync();
            var model = measurementUnits.OrderBy(b => b.MeasurementName).Select(b => new MeasurementUnitListViewModel
            {
                Id = b.Id,
                Name = b.MeasurementName
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            return this.View();
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var measurementUnit = await this.measurementUnitManager.GetByIdAsync(id.Value);

            if (measurementUnit == null)
            {
                return this.BadRequest();
            }

            var result = await this.measurementUnitManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var measurementUnit = await this.measurementUnitManager.GetByIdAsync(id.Value);

            if (measurementUnit == null)
            {
                return this.BadRequest();
            }

            return this.View(new MeasurementUnitEditViewModel
            {
                Id = measurementUnit.Id,
                Name = measurementUnit.MeasurementName,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(MeasurementUnitEditViewModel measurementUnitEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(measurementUnitEditViewModel);
            }

            var measurementUnit = await this.measurementUnitManager.GetByIdAsync(measurementUnitEditViewModel.Id);

            if (measurementUnit == null)
            {
                return this.BadRequest();
            }

            measurementUnit.MeasurementName = measurementUnitEditViewModel.Name;

            var result = await this.measurementUnitManager.EditAsync(measurementUnit);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(measurementUnitEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idMeasurementUnit)
        {
            if (idMeasurementUnit == null)
            {
                return this.BadRequest();
            }

            var measurementUnit = await this.measurementUnitManager.GetByIdAsync(idMeasurementUnit.Value);

            if (measurementUnit == null)
            {
                return this.BadRequest();
            }

            return this.View(new MeasurementUnitDetailsViewModel
            {
                Id = measurementUnit.Id,
                Name = measurementUnit.MeasurementName,
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MeasurementUnitCreateViewModel measurementUnitCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(measurementUnitCreateViewModel);
            }

            var result = await this.measurementUnitManager.CreateAsync(new Database.Models.Accounting.MeasurementUnit
            {
                MeasurementName = measurementUnitCreateViewModel.Name,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                return this.View(measurementUnitCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
    }
}
