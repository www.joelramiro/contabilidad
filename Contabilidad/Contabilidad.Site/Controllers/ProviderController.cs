﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.Provider;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.Provider;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Contabilidad.Site.Controllers
{
    public class ProviderController : Controller
    {
        private readonly IProviderManager providerManager;
        private readonly IRepository<Database.Model.People.People> peopleRepository;

        public ProviderController(
            IProviderManager providerManager,
            IRepository<Database.Model.People.People> peopleRepository)
        {
            this.providerManager = providerManager;
            this.peopleRepository = peopleRepository;
        }
        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var providers = await this.providerManager.GetAllAsync();
            var model = providers.OrderBy(b => b.People).Select(b => new ProviderListViewModel
            {
                Id = b.Id,
                People = b.People?.FullName(),
                BusinessName = b.BusinessName,
                Description = b.Description,
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            return this.View(new ProviderCreateViewModel 
            {
                Peoples = await  this.GetPeoples(),
            });
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var provider = await this.providerManager.GetByIdAsync(id.Value);

            if (provider == null)
            {
                return this.BadRequest();
            }

            var result = await this.providerManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var provider = await this.providerManager.GetByIdAsync(id.Value);

            if (provider == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProviderEditViewModel
            {
                Id = provider.Id,
                Peoples = await this.GetPeoples(),
                BusinessName = provider.BusinessName,
                Description = provider.Description,
                IdPeople = provider.IdPeople,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProviderEditViewModel providerEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                providerEditViewModel.Peoples = await this.GetPeoples();
                return this.View(providerEditViewModel);
            }

            var provider = await this.providerManager.GetByIdAsync(providerEditViewModel.Id);

            if (provider == null)
            {
                return this.BadRequest();
            }

            provider.BusinessName = providerEditViewModel.BusinessName;
            provider.Description = providerEditViewModel.Description;
            provider.IdPeople = providerEditViewModel.IdPeople.Value;

            var result = await this.providerManager.EditAsync(provider);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                providerEditViewModel.Peoples = await this.GetPeoples();
                return this.View(providerEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idProvider)
        {
            if (idProvider == null)
            {
                return this.BadRequest();
            }

            var provider = await this.providerManager.GetByIdAsync(idProvider.Value);

            if (provider == null)
            {
                return this.BadRequest();
            }

            return this.View(new ProviderDetailsViewModel
            {
                Id = provider.Id,
                People = provider.People?.FullName(),
                BusinessName = provider.BusinessName,
                Description = provider.Description,
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProviderCreateViewModel providerCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                providerCreateViewModel.Peoples = await this.GetPeoples();
                return this.View(providerCreateViewModel);
            }

            var result = await this.providerManager.CreateAsync(new Database.Models.Accounting.Provider
            {
                BusinessName = providerCreateViewModel.BusinessName,
                IdPeople = providerCreateViewModel.IdPeople.Value,
                Description = providerCreateViewModel.Description,
            });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                providerCreateViewModel.Peoples = await this.GetPeoples();
                return this.View(providerCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        private async Task<IEnumerable<SelectListViewModel>> GetPeoples()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Provider.Provider.PeopleSelect,
                }
            }
            .Union(await this.peopleRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.FullName(),
            }).ToListAsync());
        }

    }
}
