﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contabilidad.Core.CurrencyConversion;
using Contabilidad.Database.Models;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.CurrencyConversion;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Contabilidad.Site.Controllers
{
    public class CurrencyConversionController : Controller
    {
        private readonly ICurrencyConversionManager currencyConversionManager;
        private readonly IRepository<Currency> currencyRepository;

        public CurrencyConversionController(ICurrencyConversionManager currencyConversionManager,
            IRepository<Currency> currencyRepository)
        {
            this.currencyConversionManager = currencyConversionManager;
            this.currencyRepository = currencyRepository;
        }
        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var currencyConversions = await this.currencyConversionManager.GetAllWithCurrenciesMoreRecentAsync();
            var model = currencyConversions.OrderBy(b => b.CurrencyOrigin.Name).Select(b => new CurrencyConversionListViewModel
            {
                Id = b.Id,
                CreatedDate = b.CreatedDate,
                CurrencyDestination = b.CurrencyDestination.Name,
                CurrencyOrigin = b.CurrencyOrigin.Name,
                ValueConversion = b.ValueConversion,
            });

            return View(model);
        }
        public async Task<IActionResult> Create()
        {
            var currencies = await this.GetCurrenciesAsync();
            var model = new CurrencyConversionCreateViewModel 
            {
                CurrenciesOrigin = currencies,
                CurrenciesDestination = currencies,
            };

            return this.View(model);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return this.BadRequest();
            }

            var currencyConversion = await this.currencyConversionManager.GetByIdAsync(id.Value);

            if (currencyConversion == null)
            {
                return this.BadRequest();
            }

            var result = await this.currencyConversionManager.DeleteAsync(id.Value);
            var errorMessages = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        errorMessages.Append(item);
                    }
                }

                return this.RedirectToAction(nameof(Index), new { errorMessage = errorMessages });
            }

            return this.RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var currencyConversion = await this.currencyConversionManager.GetByIdAsync(id.Value);

            if (currencyConversion == null)
            {
                return this.BadRequest();
            }

            var currencies = await this.GetCurrenciesAsync();
            return this.View(new CurrencyConversionEditViewModel
            {
                Id = currencyConversion.Id,
                CurrenciesOrigin = currencies,
                CreatedDate = currencyConversion.CreatedDate,
                CurrenciesDestination = currencies,
                IdCurrencyDestination = currencyConversion.IdCurrencyDestination,
                IdCurrencyOrigin = currencyConversion.IdCurrencyOrigin,
                ValueConversion = currencyConversion.ValueConversion,
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CurrencyConversionEditViewModel currencyConversionEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                var currencies = await this.GetCurrenciesAsync();
                currencyConversionEditViewModel.CurrenciesOrigin = currencies;
                currencyConversionEditViewModel.CurrenciesDestination = currencies;
                return this.View(currencyConversionEditViewModel);
            }
                      

            var result = await this.currencyConversionManager.EditAsync(new Database.Models.Accounting.CurrencyConversion 
            {
                CreatedDate = DateTime.Now,
                IdCurrencyDestination = currencyConversionEditViewModel.IdCurrencyDestination.Value,
                IdCurrencyOrigin = currencyConversionEditViewModel.IdCurrencyOrigin.Value,
                ValueConversion = currencyConversionEditViewModel.ValueConversion,
            }, currencyConversionEditViewModel.ReverseConversion);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                var currencies = await this.GetCurrenciesAsync();
                currencyConversionEditViewModel.CurrenciesOrigin = currencies;
                currencyConversionEditViewModel.CurrenciesDestination = currencies;
                return this.View(currencyConversionEditViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? idCurrencyConversion)
        {
            if (idCurrencyConversion == null)
            {
                return this.BadRequest();
            }

            var currencyConversion = await this.currencyConversionManager.GetMoreRecentByIdAsync(idCurrencyConversion.Value);

            if (currencyConversion == null)
            {
                return this.BadRequest();
            }

            return this.View(new CurrencyConversionDetailViewModel
            {
                Id = currencyConversion.Id,
                CreatedDate = currencyConversion.CreatedDate,
                CurrencyDestination = currencyConversion.CurrencyDestination.Name,
                CurrencyOrigin = currencyConversion.CurrencyOrigin.Name,
                ValueConversion = currencyConversion.ValueConversion,
            });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CurrencyConversionCreateViewModel currencyConversionCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                var currencies = await this.GetCurrenciesAsync();
                currencyConversionCreateViewModel.CurrenciesOrigin = currencies;
                currencyConversionCreateViewModel.CurrenciesDestination = currencies;
                return this.View(currencyConversionCreateViewModel);
            }

            var result = await this.currencyConversionManager.CreateAsync(new Database.Models.Accounting.CurrencyConversion
            {
                IdCurrencyOrigin = currencyConversionCreateViewModel.IdCurrencyOrigin.Value,
                IdCurrencyDestination = currencyConversionCreateViewModel.IdCurrencyDestination.Value,
                ValueConversion = currencyConversionCreateViewModel.ValueConversion,
                CreatedDate = DateTime.Now,
            }, currencyConversionCreateViewModel.ReverseConversion);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(string.Empty, item);
                    }
                }

                var currencies = await this.GetCurrenciesAsync();
                currencyConversionCreateViewModel.CurrenciesOrigin = currencies;
                currencyConversionCreateViewModel.CurrenciesDestination = currencies;
                return this.View(currencyConversionCreateViewModel);
            }

            return this.RedirectToAction(nameof(Index));
        }

        private async Task<IEnumerable<SelectListViewModel>> GetCurrenciesAsync()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.ProductPrice.ProductPrice.CurrencySelected
                }
            }
            .Union(await this.currencyRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }
    }
}
