﻿using Contabilidad.Core.Invoice;
using Contabilidad.Site.Models.Invoice;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Controllers
{
    public class InvoiceController : Controller
    {
        private readonly IInvoiceManager invoiceManager;

        public InvoiceController(IInvoiceManager invoiceManager)
        {
            this.invoiceManager = invoiceManager;
        }

        public async Task<IActionResult> Index(string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                this.ModelState.AddModelError(string.Empty, errorMessage);
            }

            var invoices = await this.invoiceManager.GetAllAsync();

            var model = invoices.Select(i => new InvoiceListViewModel
            {
                Id = i.Id,
                Client = i.Client?.People?.FullName(),
                Code = i.Code,
                CreationDate = i.CreationDate,
                InvoiceType = i.InvoiceType,
                Total = i.Total,
                IsPaid = i.InvoiceType == Database.Models.Accounting.InvoiceType.Cash ? true : i.CreditInvoice.IsPaid,
            });

            return View(model);
        }

        public async Task<IActionResult> CreateCashInvoice()
        {
            return this.View(new CashInvoiceCreateViewModel
            {
                Index = 0,
            });
        }
        public async Task<IActionResult> CreateCreditInvoice()
        {
            return this.View(new CreditInvoiceCreateViewModel
            {
                Index = 0,
            });
        }
    }
}
