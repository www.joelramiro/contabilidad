// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Contabilidad.Site
{
    using Contabilidad.Core.AssignPurchaseToWarehouse;
    using Contabilidad.Core.BranchOffice;
    using Contabilidad.Core.Brand;
    using Contabilidad.Core.Client;
    using Contabilidad.Core.CurrencyConversion;
    using Contabilidad.Core.File;
    using Contabilidad.Core.Invoice;
    using Contabilidad.Core.Location;
    using Contabilidad.Core.MeasurementUnit;
    using Contabilidad.Core.PeopleData.People;
    using Contabilidad.Core.Product;
    using Contabilidad.Core.ProductCategory;
    using Contabilidad.Core.ProductModel;
    using Contabilidad.Core.ProductPrice;
    using Contabilidad.Core.Provider;
    using Contabilidad.Core.PurchaseInvoice;
    using Contabilidad.Core.Warehouse;
    using Contabilidad.Database.Context;
    using Contabilidad.Database.Model.People;
    using Contabilidad.Database.Models;
    using Contabilidad.Database.Models.ContabilidadSettings;
    using Contabilidad.Database.Repositories;
    using Contabilidad.Database.Repositories.Accounting;
    using Contabilidad.Database.Repositories.ContabilidadSettings;
    using Contabilidad.Database.Repository.Database.People;
    using Hangfire;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    //using Swashbuckle.AspNetCore.Swagger; devolveer

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCloudscribeNavigation(this.Configuration.GetSection("NavigationOptions"));
            services.AddControllersWithViews();

            services.AddLocalization();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<ContabilidadContext>(options =>
            {
                options.UseSqlServer(this.Configuration.GetConnectionString("ContabilidadConnection"));
                options.EnableSensitiveDataLogging(true);
                options.EnableDetailedErrors(true);
            });
            services.AddDbContext<ContabilidadSettingsContext>(options =>
            {
                options.UseSqlServer(this.Configuration.GetConnectionString("ContabilidadSettingsConnection"));
                options.EnableSensitiveDataLogging(true);
                options.EnableDetailedErrors(true);
            });

            this.AddRepositories(services);
            this.AddManagers(services);

            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new Info { Title = "Communication Channel Gateway", Version = "v1" });
            //});  devolveer

            services.AddHangfire(configuration => configuration
               .UseSimpleAssemblyNameTypeSerializer()
               .UseRecommendedSerializerSettings()
               .UseSqlServerStorage(this.Configuration.GetConnectionString("HangfireConnection")));

            // Add the processing server as IHostedService
            services.AddHangfireServer();
        }

        private void AddManagers(IServiceCollection services)
        {
            //services.AddScoped<IPeopleManager, PeopleManager>();
            services.AddScoped<IBrandManager, BrandManager>();
            services.AddScoped<IProductModelManager, ProductModelManager>();
            services.AddScoped<IProductCategoryManager, ProductCategoryManager>();
            services.AddScoped<IMeasurementUnitManager, MeasurementUnitManager>();
            services.AddScoped<IProductManager, ProductManager>();
            services.AddScoped<IPeopleManager, PeopleManager>();
            services.AddScoped<IProviderManager, ProviderManager>();
            services.AddScoped<IPurchaseInvoiceManager, PurchaseInvoiceManager>();
            services.AddScoped<IFileManager, FileManager>();
            services.AddScoped<ILocationManager, LocationManager>();
            services.AddScoped<IAssignPurchaseToWarehouseManager, AssignPurchaseToWarehouseManager>();
            services.AddScoped<IAssignPurchaseToWarehouseManager, AssignPurchaseToWarehouseManager>();
            services.AddScoped<IWarehouseManager, WarehouseManager>();
            services.AddScoped<IWarehouseMovementManager, WarehouseMovementManager>();
            services.AddScoped<IBranchOfficeManager, BranchOfficeManager>();
            services.AddScoped<IProductPriceManager, ProductPriceManager>();
            services.AddScoped<ICurrencyConversionManager, CurrencyConversionManager>();
            services.AddScoped<IClientManager, ClientManager>();
            services.AddScoped<IInvoiceManager, InvoiceManager>();
        }

        private void AddRepositories(IServiceCollection services)
        {

            services.AddScoped<IRepository<KeyPair>, KeyPairRepository>();
            services.AddScoped<IRepository<ValuePair>, ValuePairRepository>();


            services.AddScoped<IRepository<Currency>, CurrencyRepository>();
            services.AddScoped<IRepository<FileInBytes>, FileInBytesRepository>();

            services.AddScoped<IRepository<People>, PeopleRepository>();
            services.AddScoped<IRepository<CivilStatus>, CivilStatusRepository>();
            services.AddScoped<IRepository<SexType>, SexTypeRepository>();
            services.AddScoped<IRepository<EducationLevel>, EducationLevelRepository>();
            //services.AddScoped<IRepository<Address>, AddressRepository>();

            services.AddScoped<IRepository<Database.Models.Accounting.Brand>, BrandRepository>();
            services.AddScoped < IRepository < Database.Models.Accounting.BranchOffice>, BranchOfficeRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.ProductPrice>, ProductPriceRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.CashInvoice>, CashInvoiceRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.CashInvoiceDetail>, CashInvoiceDetailRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.Client>, ClientRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.CreditInvoice>, CreditInvoiceRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.CreditInvoiceDetail>, CreditInvoiceDetailRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.CreditInvoiceVoucher>, CreditInvoiceVoucherRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.Fee>, FeeRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.Invoice>, InvoiceRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.Location>, LocationRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.MeasurementUnit>, MeasurementUnitRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.Product>, ProductRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.ProductCategory>, ProductCategoryRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.ProductModel>, ProductModelRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.ProductRange>, ProductRangeRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.Provider>, ProviderRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.PurchaseInvoice>, PurchaseInvoiceRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.PurchaseInvoiceDetail>, PurchaseInvoiceDetailRepository > ();
            services.AddScoped < IRepository < Database.Models.Accounting.Usser>, UsserRepository> ();
            services.AddScoped < IRepository < Database.Models.Accounting.Warehouse>, WarehouseRepository> ();
            services.AddScoped<IRepository<Database.Models.Accounting.WarehouseDetail>, WarehouseDetailRepository>();
            services.AddScoped<IRepository<Database.Models.Accounting.AssignPurchaseToWarehouse>, AssignPurchaseToWarehouseRepository>();
            services.AddScoped<IRepository<Database.Models.Accounting.AssignPurchaseToWarehouseDetail>, AssignPurchaseToWarehouseDetailRepository>();
            services.AddScoped < IRepository<Database.Models.Accounting.WarehouseMovement>, WarehouseMovementRepository>();
            services.AddScoped<IRepository<Database.Models.Accounting.WarehouseMovementDetail>, WarehouseMovementDetailRepository>();
            services.AddScoped<IRepository<Database.Models.Accounting.CurrencyConversion>, CurrencyConversionRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Communication Channel Gateway V1");
            //}); devolveer

            app.UseHangfireDashboard();

            app.UseStaticFiles();
            app.UseRouting();
            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
