﻿using Contabilidad.Core.Product;
using Contabilidad.Core.Warehouse;
using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.WarehouseMovement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Components
{
    public class WarehouseMovementDetailCreateComponent : ViewComponent
    {
        private readonly IRepository<Warehouse> warehouseRepository;
        private readonly IWarehouseManager warehouseManager;
        private readonly IProductManager productManager;

        public WarehouseMovementDetailCreateComponent(
            IRepository<Warehouse> warehouseRepository,
            IWarehouseManager warehouseManager,
            IProductManager productManager)
        {
            this.warehouseRepository = warehouseRepository;
            this.warehouseManager = warehouseManager;
            this.productManager = productManager;
        }


        public async Task<IViewComponentResult> InvokeAsync(WarehouseMovementDetailCreateViewModel detail, int idWarehouseOrigin)
        {
            detail.IsDeleted = false;
            detail.Products = await this.GetProducts(idWarehouseOrigin);
            detail.WarehousesDestination = await this.GetWarehouses(idWarehouseOrigin);
            return View(detail);
        }

        private async Task<IEnumerable<SelectListViewModel>> GetWarehouses(int? idWarehouse)
        {
            if (idWarehouse == null)
            {
                return new List<SelectListViewModel>();
            }

            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Warehouse.WarehouseMovementDetail.WarehouseDestinationSelected
                }
            }
            .Union(await this.warehouseRepository.Filter(w => w.Id != idWarehouse.Value).Where(w => w.IsActive).Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Code,
            }).ToListAsync());
        }


        private async Task<IEnumerable<SelectListViewModel>> GetProducts(int? idWarehouse)
        {
            if (idWarehouse == null)
            {
                return new List<SelectListViewModel>();
            }

            var warehouse = await this.warehouseManager.GetByIdAsync(idWarehouse.Value);

            if (warehouse == null)
            {
                return new List<SelectListViewModel>();
            }
            var products = await this.productManager.GetAllAsync();
            var productsJoin = warehouse.Details.Where(d => d.Quantity > 0).Select(d => new { d.IdProduct, d.Quantity })
                .Join(products, d => d.IdProduct, d => d.Id, (warehouseProduct, product) => new { product, warehouseProduct.Quantity });

            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.Warehouse.WarehouseMovementDetail.ProductSelected
                }
            }
            .Union(productsJoin.Select(b => new SelectListViewModel
            {
                Id = b.product.Id,
                Value = b.product.Name + " (" + b.Quantity + ")",
            }));
        }
    }
}
