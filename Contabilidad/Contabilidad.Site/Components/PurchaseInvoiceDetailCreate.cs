﻿using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.PurchaseInvoice;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Components
{
    public class PurchaseInvoiceDetailCreate : ViewComponent
    {
        private readonly IRepository<Product> productRepository;

        public PurchaseInvoiceDetailCreate(IRepository<Product> productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync(PurchaseInvoiceDetailCreateViewModel detail)
        {
            detail.IsDeleted = false;
            detail.Products = await this.GetProducts();
            return View(detail);
        }

        private async Task<IEnumerable<SelectListViewModel>> GetProducts()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail.ProductSelect,
                }
            }
            .Union(await this.productRepository.All().Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Name,
            }).ToListAsync());
        }
    }
}
