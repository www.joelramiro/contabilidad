﻿using Contabilidad.Database.Models.Accounting;
using Contabilidad.Database.Repositories;
using Contabilidad.Site.Models;
using Contabilidad.Site.Models.AssignPurchaseToWarehouse;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Components
{
    public class AssignPurchaseToWarehouseDetailCreateComponent : ViewComponent
    {
        private readonly IRepository<Warehouse> warehouseRepository;
        private readonly IRepository<PurchaseInvoice> purchaseInvoiceRepository;
        private readonly IRepository<AssignPurchaseToWarehouse> assignPurchaseToWarehouseRepository;

        public AssignPurchaseToWarehouseDetailCreateComponent(
            IRepository<Warehouse> warehouseRepository,
            IRepository<PurchaseInvoice> purchaseInvoiceRepository,
            IRepository<AssignPurchaseToWarehouse> assignPurchaseToWarehouseRepository)
        {
            this.warehouseRepository = warehouseRepository;
            this.purchaseInvoiceRepository = purchaseInvoiceRepository;
            this.assignPurchaseToWarehouseRepository = assignPurchaseToWarehouseRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync(AssignPurchaseToWarehouseDetailCreateViewModel detail, int idPurchaseInvoice)
        {
            detail.IsDeleted = false;
            detail.Products = await this.GetProducts(idPurchaseInvoice);
            detail.Warehouses = await this.GetWarehouses();
            return View(detail);
        }


        private async Task<IEnumerable<SelectListViewModel>> GetProducts(int idPurchaseInvoice)
        {
            var productsInvoice = await this.purchaseInvoiceRepository.All()
                .Include(p => p.Details)
                .ThenInclude(p => p.Product).FirstOrDefaultAsync(i => i.Id == idPurchaseInvoice);

            if (productsInvoice == null)
            {
                return new List<SelectListViewModel>();
            }

            var preAssigned = await this.assignPurchaseToWarehouseRepository.All()
                .Include(d => d.Detail)
                .ThenInclude(d => d.Product)
                .Where(a => a.IdPurchaseInvoice == idPurchaseInvoice)
                .Select(d => new { header = d, detail = d.Detail })
                .SelectMany(d => d.detail, (h, d) => new { d })
                .ToListAsync();

            var preasignedGroup = preAssigned.GroupBy(d => d.d.Product)
                .Select(p => new
                {
                    Product = p.Key,
                    Quantity = p.Sum(d => d.d.Quantity),
                });

            var pending = productsInvoice.Details
                .GroupJoin(preAssigned, d => d.IdProduct, d => d.d.IdProduct, (principal, preAssigned) => new { principal, preAssigned })
                .SelectMany(d => d.preAssigned.DefaultIfEmpty(), (principal, preAsigned) => new { principal.principal, preAsigned })
                .Select(d => new { Product = d.principal.Product, principalQuantity = d.principal.Quantity, d.preAsigned })
                .GroupBy(d => d.Product)
                .Select(d => new
                {
                    Product = d.Key,
                    principal = d.FirstOrDefault().principalQuantity,
                    secundary = d.Sum(dd => dd.preAsigned == null ? 0 : dd.preAsigned.d.Quantity)
                })
                .Where(p => p.secundary < p.principal)
                .ToList();



            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail.ProductSelect,
                }
}
            .Union(pending.Select(b => new SelectListViewModel
            {
                Id = b.Product.Id,
                Value = b.Product.Name + " (" + (b.principal - b.secundary) + ")",
            }).ToList());
        }
        private async Task<IEnumerable<SelectListViewModel>> GetWarehouses()
        {
            return new List<SelectListViewModel>() {
                new SelectListViewModel
                {
                    Value = Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail.WarehouseSelect
                }
            }
            .Union(await this.warehouseRepository.All().Where(w => w.IsActive).Select(b => new SelectListViewModel
            {
                Id = b.Id,
                Value = b.Code,
            }).ToListAsync());
        }
    }
}
