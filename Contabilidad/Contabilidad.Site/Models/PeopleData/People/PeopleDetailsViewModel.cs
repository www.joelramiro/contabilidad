﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.PeopleData.People
{
    public class PeopleDetailsViewModel
    {
        public int Id { get; set; }

        [Display(Name = nameof(Identity), ResourceType = typeof(Resources.Models.People.People.People))]
        public string Identity { get; set; }

        [Display(Name = nameof(FullName), ResourceType = typeof(Resources.Models.People.People.People))]
        public string FullName { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Display(Name = nameof(BirthDate), ResourceType = typeof(Resources.Models.People.People.People))]
        public DateTime BirthDate { get; set; }

        [Display(Name = nameof(EducationLevel), ResourceType = typeof(Resources.Models.People.People.People))]
        public string EducationLevel { get; set; }

        [Display(Name = nameof(SexType), ResourceType = typeof(Resources.Models.People.People.People))]
        public string SexType { get; set; }

        [Display(Name = nameof(CivilStatus), ResourceType = typeof(Resources.Models.People.People.People))]
        public string CivilStatus { get; set; }
    }
}
