﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.PeopleData.People
{
    public class PeopleEditViewModel
    {
        public int Id { get; set; }

        [Display(Name = nameof(Identity), ResourceType = typeof(Resources.Models.People.People.People))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string Identity { get; set; }

        [Display(Name = nameof(FirstName), ResourceType = typeof(Resources.Models.People.People.People))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string FirstName { get; set; }

        [Display(Name = nameof(SecondName), ResourceType = typeof(Resources.Models.People.People.People))]
        public string SecondName { get; set; }

        [Display(Name = nameof(MiddleName), ResourceType = typeof(Resources.Models.People.People.People))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string MiddleName { get; set; }

        [Display(Name = nameof(LastName), ResourceType = typeof(Resources.Models.People.People.People))]
        public string LastName { get; set; }

        [Display(Name = nameof(BirthDate), ResourceType = typeof(Resources.Models.People.People.People))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime BirthDate { get; set; }

        [Display(Name = nameof(Database.Model.People.People.EducationLevel), ResourceType = typeof(Resources.Models.People.People.People))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? EducationLevelId { get; set; }

        [Display(Name = nameof(Database.Model.People.People.SexType), ResourceType = typeof(Resources.Models.People.People.People))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? SexTypeId { get; set; }

        [Display(Name = nameof(Database.Model.People.People.CivilStatus), ResourceType = typeof(Resources.Models.People.People.People))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? CivilStatusId { get; set; }

        public IEnumerable<SelectListViewModel> CivilStatuses { get; set; }

        public IEnumerable<SelectListViewModel> SexTypes { get; set; }

        public IEnumerable<SelectListViewModel> EducationLevels { get; set; }

        public IEnumerable<SelectListViewModel> Telephones { get; set; }
    }
}
