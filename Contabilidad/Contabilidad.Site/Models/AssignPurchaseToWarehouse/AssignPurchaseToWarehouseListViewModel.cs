﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.AssignPurchaseToWarehouse
{
    public class AssignPurchaseToWarehouseListViewModel
    {
        [Display(Name = nameof(IdInvoice), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public int IdInvoice { get; set; }

        [Display(Name = nameof(Code), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public string Code { get; set; }

        [Display(Name = nameof(CreatedDate), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = nameof(Comments), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public string Comments { get; set; }

        [Display(Name = nameof(SubTotal), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        [DisplayFormat(DataFormatString = "{0:N}")]
        public decimal SubTotal { get; set; }

        [Display(Name = nameof(Provider), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public string Provider { get; set; }

        [Display(Name = nameof(Currency), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public string Currency { get; set; }

        [Display(Name = nameof(IsPaid), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public bool IsPaid { get; set; }
    }
}
