﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contabilidad.Site.Models.AssignPurchaseToWarehouse
{
    public class AssignPurchaseToWarehouseDetailCreateViewModel
    {
        public int? Index { get; set; }

        public int? Id { get; set; }

        [Display(Name = nameof(IdProduct), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail))]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdProduct { get; set; }

        [Display(Name = nameof(Quantity), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail))]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? Quantity { get; set; }

        public bool? IsDeleted { get; set; }

        [Display(Name = nameof(IdWarehouse), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail))]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdWarehouse { get; set; }

        [Display(Name = nameof(Warehouses), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail))]
        public IEnumerable<SelectListViewModel>? Warehouses { get; set; }

        [Display(Name = nameof(Products), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouseDetail.AssignPurchaseToWarehouseDetail))]
        public IEnumerable<SelectListViewModel>? Products { get; set; }
    }
}