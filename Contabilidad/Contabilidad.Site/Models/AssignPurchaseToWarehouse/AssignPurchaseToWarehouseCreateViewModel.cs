﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.AssignPurchaseToWarehouse
{
    public class AssignPurchaseToWarehouseCreateViewModel
    {
        public int Index { get; set; }

        [Display(Name = nameof(IdInvoice), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public int IdInvoice { get; set; }

        [Display(Name = nameof(Details), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public IEnumerable<AssignPurchaseToWarehouseDetailCreateViewModel> Details { get; set; }
        
        [Display(Name = nameof(InvoiceCode), ResourceType = typeof(Resources.Models.Accounting.AssignPurchaseToWarehouse.AssignPurchaseToWarehouse))]
        public string InvoiceCode { get; set; }
    }
}
