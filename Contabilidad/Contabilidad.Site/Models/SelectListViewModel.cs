﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contabilidad.Site.Models
{
    public class SelectListViewModel
    {
        public int? Id { get; set; }

        public string Value { get; set; }
    }
}
