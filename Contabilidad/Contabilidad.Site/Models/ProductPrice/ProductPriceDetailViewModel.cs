﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.ProductPrice
{
    public class ProductPriceDetailViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        public int Id { get; set; }


        [Display(Name = nameof(SalePrice), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        public decimal SalePrice { get; set; }

        [Display(Name = nameof(WarrantyTime), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        public string WarrantyTime { get; set; }


        [Display(Name = nameof(Product), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        public string Product { get; set; }


        [Display(Name = nameof(Currency), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        public string Currency { get; set; }
    }
}
