﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.ProductPrice
{
    public class ProductPriceCreateViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        public int Id { get; set; }


        [Display(Name = nameof(SalePrice), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public decimal SalePrice { get; set; }


        [Display(Name = nameof(Years), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        [Range(0,30, ErrorMessageResourceName = "RangeLimit", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? Years { get; set; }

        [Display(Name = nameof(Months), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        [Range(0, 12, ErrorMessageResourceName = "RangeLimit", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? Months { get; set; }

        [Display(Name = nameof(Days), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        [Range(0, 31, ErrorMessageResourceName = "RangeLimit", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? Days { get; set; }


        [Display(Name = nameof(IdProduct), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int IdProduct { get; set; }


        [Display(Name = nameof(IdCurrency), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int IdCurrency { get; set; }


        [Display(Name = nameof(Products), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        public IEnumerable<SelectListViewModel> Products { get; set; }


        [Display(Name = nameof(Currencies), ResourceType = typeof(Resources.Models.Accounting.ProductPrice.ProductPrice))]
        public IEnumerable<SelectListViewModel> Currencies { get; set; }
    }
}
