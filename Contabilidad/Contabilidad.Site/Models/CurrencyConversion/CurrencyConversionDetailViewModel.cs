﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.CurrencyConversion
{
    public class CurrencyConversionDetailViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        public int Id { get; set; }


        [Display(Name = nameof(CreatedDate), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedDate { get; set; }


        [Display(Name = nameof(CurrencyOrigin), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        public string CurrencyOrigin { get; set; }


        [Display(Name = nameof(CurrencyDestination), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        public string CurrencyDestination { get; set; }


        [Display(Name = nameof(ValueConversion), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        [DisplayFormat(DataFormatString = "{0:N5}")]
        public decimal ValueConversion { get; set; }
    }
}
