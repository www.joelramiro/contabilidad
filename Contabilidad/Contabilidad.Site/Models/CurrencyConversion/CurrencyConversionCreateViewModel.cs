﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.CurrencyConversion
{
    public class CurrencyConversionCreateViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        public int Id { get; set; }


        [Display(Name = nameof(CreatedDate), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public DateTime CreatedDate { get; set; }


        [Display(Name = nameof(IdCurrencyOrigin), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdCurrencyOrigin { get; set; }


        [Display(Name = nameof(IdCurrencyDestination), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdCurrencyDestination { get; set; }


        [Display(Name = nameof(ValueConversion), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        [Range(0.0, double.MaxValue, ErrorMessageResourceName = "MinValue", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public decimal ValueConversion { get; set; }


        [Display(Name = nameof(ReverseConversion), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public bool ReverseConversion { get; set; }


        [Display(Name = nameof(CurrenciesOrigin), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        public IEnumerable<SelectListViewModel> CurrenciesOrigin { get; set; }


        [Display(Name = nameof(CurrenciesDestination), ResourceType = typeof(Resources.Models.Accounting.CurrencyConversion.CurrencyConversion))]
        public IEnumerable<SelectListViewModel> CurrenciesDestination { get; set; }
    }
}
