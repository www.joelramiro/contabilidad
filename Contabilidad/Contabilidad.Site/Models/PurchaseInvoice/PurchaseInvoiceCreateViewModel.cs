﻿using Contabilidad.Database.Models.Accounting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.PurchaseInvoice
{
    public class PurchaseInvoiceCreateViewModel
    {
        [Display(Name = nameof(Total), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public decimal Total { get; set; }

        [Display(Name = nameof(Code), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string Code { get; set; }

        [Display(Name = nameof(Isv), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public decimal Isv { get; set; }

        [Display(Name = nameof(Disccount), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public decimal Disccount { get; set; }

        [Display(Name = nameof(SubTotal), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public decimal SubTotal { get; set; }


        [Display(Name = nameof(Database.Models.Accounting.PurchaseInvoice.Provider), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdProvider { get; set; }

        [Display(Name = nameof(Database.Models.Accounting.PurchaseInvoice.Currency), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdCurrency { get; set; }

        [Display(Name = nameof(WarehouseSelected), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public int? WarehouseSelected { get; set; }

        [Display(Name = nameof(Voucher), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public IFormFile Voucher { get; set; }

        [Display(Name = nameof(IsPaid), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public bool IsPaid { get; set; }

        [Display(Name = nameof(Comments), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public string Comments { get; set; }

        [Display(Name = nameof(InvoiceType), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public InvoiceType InvoiceType { get; set; }

        [Display(Name = nameof(Providers), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public IEnumerable<SelectListViewModel> Providers { get; set; }

        [Display(Name = nameof(Currencies), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public IEnumerable<SelectListViewModel> Currencies { get; set; }

        [Display(Name = nameof(Details), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public IEnumerable<PurchaseInvoiceDetailCreateViewModel> Details { get; set; }

        [Display(Name = nameof(Warehouses), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public IEnumerable<SelectListViewModel> Warehouses { get; set; }
        public int Index { get; set; }
    }
}
    