﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.PurchaseInvoice
{
    public class PurchaseInvoiceDetailsDetailsViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        public int Id { get; set; }


        [Display(Name = nameof(Product), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        public string Product { get; set; }


        [Display(Name = nameof(Quantity), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        public int Quantity { get; set; }


        [Display(Name = nameof(UnitPrice), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        public decimal UnitPrice { get; set; }


        [Display(Name = nameof(Total), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        public decimal Total { get; set; }
    }
}
