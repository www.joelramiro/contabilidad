﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contabilidad.Site.Models.PurchaseInvoice
{
    public class PurchaseInvoiceDetailCreateViewModel
    {
        public int Index { get; set; }

        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        public int Id { get; set; }

        [Display(Name = nameof(Database.Models.Accounting.PurchaseInvoiceDetail.Product), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdProduct { get; set; }

        [Display(Name = nameof(Quantity), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int Quantity { get; set; }

        [Display(Name = nameof(UnitPrice), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public decimal UnitPrice { get; set; }

        [Display(Name = nameof(Total), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoiceDetail.PurchaseInvoiceDetail))]
        public decimal Total { get; set; }

        public bool? IsDeleted { get; set; }

        public IEnumerable<SelectListViewModel> Products { get; set; }
    }
}