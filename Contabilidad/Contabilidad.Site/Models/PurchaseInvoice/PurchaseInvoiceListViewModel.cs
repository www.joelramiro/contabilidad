﻿using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.PurchaseInvoice
{
    public class PurchaseInvoiceListViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public int Id { get; set; }

        [Display(Name = nameof(Code), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public string Code { get; set; }

        [Display(Name = nameof(Provider), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public string Provider { get; set; }

        [Display(Name = nameof(Currency), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public string Currency { get; set; }

        [Display(Name = nameof(InvoiceType), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public InvoiceType InvoiceType { get; set; }

        [Display(Name = nameof(Comments), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public string Comments { get; set; }

        [Display(Name = nameof(IsPaid), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public bool IsPaid { get; set; }

        [Display(Name = nameof(IsAssigned), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public bool IsAssigned { get; set; }

        [Display(Name = nameof(Disccount), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public decimal Disccount { get; set; }

        [Display(Name = nameof(Isv), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public decimal Isv { get; set; }

        [Display(Name = nameof(SubTotal), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public decimal SubTotal { get; set; }

        [Display(Name = nameof(Total), ResourceType = typeof(Resources.Models.Accounting.PurchaseInvoice.PurchaseInvoice))]
        public decimal Total { get; set; }
    }
}
