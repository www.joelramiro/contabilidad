﻿using Contabilidad.Database.Models.Accounting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.Invoice
{
    public class InvoiceDetailViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public int Id { get; set; }

        [Display(Name = nameof(CreationDate), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public DateTime CreationDate { get; set; }

        [Display(Name = nameof(Client), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public string Client { get; set; }

        [Display(Name = nameof(Code), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public string Code { get; set; }

        [Display(Name = nameof(InvoiceType), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public InvoiceType InvoiceType { get; set; }

        [Display(Name = nameof(IsPaid), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public bool IsPaid { get; set; }

        [Display(Name = nameof(Total), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public decimal Total { get; set; }
    }
}
