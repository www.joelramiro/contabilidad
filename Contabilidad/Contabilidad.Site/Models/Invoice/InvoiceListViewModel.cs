﻿// <copyright file="InvoiceListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using Contabilidad.Database.Models.Accounting;
using System;
using System.ComponentModel.DataAnnotations;

namespace Contabilidad.Site.Models.Invoice
{
    public class InvoiceListViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public int Id { get; set; }

        [Display(Name = nameof(CreationDate), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public DateTime CreationDate { get; set; }

        [Display(Name = nameof(Client), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public string Client { get; set; }

        [Display(Name = nameof(Code), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public string Code { get; set; }

        [Display(Name = nameof(InvoiceType), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public InvoiceType InvoiceType { get; set; }

        [Display(Name = nameof(IsPaid), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public bool IsPaid { get; set; }

        [Display(Name = nameof(Total), ResourceType = typeof(Resources.Models.Accounting.Invoice.Invoice))]
        public decimal Total { get; set; }
    }   
}
