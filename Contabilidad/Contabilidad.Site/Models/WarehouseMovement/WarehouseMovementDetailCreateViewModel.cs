﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.WarehouseMovement
{
    public class WarehouseMovementDetailCreateViewModel
    {

        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovementDetail))]
        public int Id { get; set; }

        [Display(Name = nameof(IdProduct), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovementDetail))]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdProduct { get; set; }


        [Display(Name = nameof(IdWarehouseDestination), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovementDetail))]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdWarehouseDestination { get; set; }


        [Display(Name = nameof(Quantity), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovementDetail))]
        //[Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int Quantity { get; set; }

        [Display(Name = nameof(WarehousesDestination), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovementDetail))]
        public IEnumerable<SelectListViewModel> WarehousesDestination { get; set; }

        [Display(Name = nameof(Products), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovementDetail))]
        public IEnumerable<SelectListViewModel> Products { get; set; }
        
        public bool? IsDeleted { get; set; }
        public int? Index { get; set; }
    }
}
