﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.WarehouseMovement
{
    public class WarehouseMovementCreateViewModel
    {
        [Display(Name = nameof(Code), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovement))]
        public string Code { get; set; }

        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovement))]
        public int Id { get; set; }


        [Display(Name = nameof(IdWarehouseOrigin), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovement))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int IdWarehouseOrigin { get; set; }


        [Display(Name = nameof(MovementDate), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovement))]
        public DateTime MovementDate { get; set; }

        [Display(Name = nameof(IdUsser), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovement))]
        public int? IdUsser { get; set; }

        [Display(Name = nameof(Details), ResourceType = typeof(Resources.Models.Accounting.Warehouse.WarehouseMovement))]

        public IEnumerable<WarehouseMovementDetailCreateViewModel> Details { get; set; }
        public int Index { get; set; }
    }
}
