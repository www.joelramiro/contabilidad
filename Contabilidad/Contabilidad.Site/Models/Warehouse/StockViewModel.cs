﻿using System.ComponentModel.DataAnnotations;

namespace Contabilidad.Site.Models.Warehouse
{
    public class StockViewModel
    {
        [Display(Name = nameof(Product), ResourceType = typeof(Resources.Models.Accounting.Warehouse.Stock))]
        public string Product { get; set; }

        [Display(Name = nameof(Quantity), ResourceType = typeof(Resources.Models.Accounting.Warehouse.Stock))]
        public int Quantity { get; set; }

        [Display(Name = nameof(MeasurementUnit), ResourceType = typeof(Resources.Models.Accounting.Warehouse.Stock))]
        public string MeasurementUnit { get; set; }
    }
}