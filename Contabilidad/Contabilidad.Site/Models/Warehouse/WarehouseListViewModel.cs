﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.Warehouse
{
    public class WarehouseListViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Warehouse.Warehouse))]
        public int Id { get; set; }

        [Display(Name = nameof(Description), ResourceType = typeof(Resources.Models.Accounting.Warehouse.Warehouse))]
        public string Description { get; set; }

        [Display(Name = nameof(Code), ResourceType = typeof(Resources.Models.Accounting.Warehouse.Warehouse))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string Code { get; set; }

        [Display(Name = nameof(IsActive), ResourceType = typeof(Resources.Models.Accounting.Warehouse.Warehouse))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public bool IsActive { get; set; }
    }
}
