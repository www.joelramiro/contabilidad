﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.Provider
{
    public class ProviderListViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Provider.Provider))]
        public int Id { get; set; }

        [Display(Name = nameof(BusinessName), ResourceType = typeof(Resources.Models.Accounting.Provider.Provider))]
        public string BusinessName { get; set; }

        [Display(Name = nameof(Description), ResourceType = typeof(Resources.Models.Accounting.Provider.Provider))]
        public string Description { get; set; }


        [Display(Name = nameof(People), ResourceType = typeof(Resources.Models.Accounting.Provider.Provider))]
        public string People { get; set; }
    }
}
