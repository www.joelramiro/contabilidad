﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.MeasurementUnit
{
    public class MeasurementUnitEditViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.MeasurementUnit.MeasurementUnit))]
        public int Id { get; set; }

        [Display(Name = nameof(Name), ResourceType = typeof(Resources.Models.Accounting.MeasurementUnit.MeasurementUnit))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string Name { get; set; }
    }
}
