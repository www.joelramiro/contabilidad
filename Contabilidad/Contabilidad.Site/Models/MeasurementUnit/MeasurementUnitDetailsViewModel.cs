﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.MeasurementUnit
{
    public class MeasurementUnitDetailsViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.MeasurementUnit.MeasurementUnit))]
        public int Id { get; set; }

        [Display(Name = nameof(Name), ResourceType = typeof(Resources.Models.Accounting.MeasurementUnit.MeasurementUnit))]
        public string Name { get; set; }
    }
}
