﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.MeasurementUnit
{
    public class MeasurementUnitCreateViewModel
    {

        [Display(Name = nameof(Name), ResourceType = typeof(Resources.Models.Accounting.MeasurementUnit.MeasurementUnit))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string Name { get; set; }
    }
}
