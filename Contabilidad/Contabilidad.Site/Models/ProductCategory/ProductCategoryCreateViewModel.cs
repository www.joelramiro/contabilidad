﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.ProductCategory
{
    public class ProductCategoryCreateViewModel
    {

        [Display(Name = nameof(Name), ResourceType = typeof(Resources.Models.Accounting.ProductCategory.ProductCategory))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string Name { get; set; }

        [Display(Name = nameof(Description), ResourceType = typeof(Resources.Models.Accounting.ProductCategory.ProductCategory))]
        public string Description { get; set; }
    }
}
