﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.Client
{
    public class ClientEditViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Client.Client))]
        public int Id { get; set; }

        [Display(Name = nameof(BusinessName), ResourceType = typeof(Resources.Models.Accounting.Client.Client))]
        public string BusinessName { get; set; }


        [Display(Name = nameof(RTN), ResourceType = typeof(Resources.Models.Accounting.Client.Client))]
        public string RTN { get; set; }


        [Display(Name = nameof(Description), ResourceType = typeof(Resources.Models.Accounting.Client.Client))]
        public string Description { get; set; }

        [Display(Name = nameof(Database.Models.Accounting.Client.People), ResourceType = typeof(Resources.Models.Accounting.Client.Client))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdPeople { get; set; }

        [Display(Name = nameof(Peoples), ResourceType = typeof(Resources.Models.Accounting.Client.Client))]
        public IEnumerable<SelectListViewModel> Peoples { get; set; }
    }
}