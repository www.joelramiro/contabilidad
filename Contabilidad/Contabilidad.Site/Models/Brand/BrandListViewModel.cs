﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.Brand
{
    public class BrandListViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Brand.Brand))]
        public int Id { get; set; }

        [Display(Name = nameof(Name), ResourceType = typeof(Resources.Models.Accounting.Brand.Brand))]
        public string Name { get; set; }
    }
}
