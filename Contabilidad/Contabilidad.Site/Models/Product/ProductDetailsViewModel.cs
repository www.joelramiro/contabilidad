﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.Product
{
    public class ProductDetailsViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public int Id { get; set; }

        [Display(Name = nameof(Name), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public string Name { get; set; }

        [Display(Name = nameof(Description), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public string Description { get; set; }

        [Display(Name = nameof(MeasurementUnit), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public string MeasurementUnit { get; set; }

        [Display(Name = nameof(Image), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public byte[] Image { get; set; }

        [Display(Name = nameof(ProductCategory), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public string ProductCategory { get; set; }

        [Display(Name = nameof(Brand), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public string Brand { get; set; }

        [Display(Name = nameof(Model), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public string Model { get; set; }
    }
}
    