﻿using Contabilidad.Resources.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.Product
{
    public class ProductCreateViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public int Id { get; set; }

        [Display(Name = nameof(Name), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string Name { get; set; }

        [Display(Name = nameof(Description), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public string Description { get; set; }

        [Display(Name = nameof(Database.Models.Accounting.Product.MeasurementUnit), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdMeasurementUnit { get; set; }

        [Display(Name = nameof(Image), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public IFormFile Image { get; set; }

        [Display(Name = nameof(Database.Models.Accounting.Product.ProductCategory), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdProductCategory { get; set; }

        [Display(Name = nameof(Database.Models.Accounting.Product.Brand), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdBrand { get; set; }

        [Display(Name = nameof(Database.Models.Accounting.Product.Model), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public int? IdModel { get; set; }

        [Display(Name = nameof(MeasurementUnits), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public IEnumerable<SelectListViewModel> MeasurementUnits { get; set; }

        [Display(Name = nameof(ProductCategories), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public IEnumerable<SelectListViewModel> ProductCategories { get; set; }

        [Display(Name = nameof(Brands), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public IEnumerable<SelectListViewModel> Brands { get; set; }

        [Display(Name = nameof(Models), ResourceType = typeof(Resources.Models.Accounting.Product.Product))]
        public IEnumerable<SelectListViewModel> Models { get; set; }
    }
}
