﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Site.Models.BranchOffice
{
    public class BranchOfficeEditViewModel
    {
        [Display(Name = nameof(Id), ResourceType = typeof(Resources.Models.Accounting.BranchOffice.BranchOffice))]
        public int Id { get; set; }


        [Display(Name = nameof(Code), ResourceType = typeof(Resources.Models.Accounting.BranchOffice.BranchOffice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public string Code { get; set; }


        [Display(Name = nameof(IsActive), ResourceType = typeof(Resources.Models.Accounting.BranchOffice.BranchOffice))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.CommonResources))]
        public bool IsActive { get; set; }


        [Display(Name = nameof(Description), ResourceType = typeof(Resources.Models.Accounting.BranchOffice.BranchOffice))]
        public string Description { get; set; }
    }
}
